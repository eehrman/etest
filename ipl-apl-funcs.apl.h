#ifndef IPL_APL_H_
#define IPL_APL_H_

typedef unsigned short int u16;
#define bool	_Bool
#define true	1
#define false	0

#define NUM_MRKS 16
#define NUM_VRS 24
#define NUM_VMRS 48

/* Vector Register IDs (16 bits) */
enum gvrc_vr16 {
	GVRC_VR16_0	= SB_0,
	GVRC_VR16_1	= SB_1,
	GVRC_VR16_2	= SB_2,
	GVRC_VR16_3	= SB_3,
	GVRC_VR16_4	= SB_4,
	GVRC_VR16_5	= SB_5,
	GVRC_VR16_6	= SB_6,
	GVRC_VR16_7	= SB_7,
	GVRC_VR16_8	= SB_8,
	GVRC_VR16_9	= SB_9,
	GVRC_VR16_10	= SB_10,
	GVRC_VR16_11	= SB_11,
	GVRC_VR16_12	= SB_12,
	GVRC_VR16_13	= SB_13,
	GVRC_VR16_14	= SB_14,
	GVRC_VR16_15	= SB_15,
	GVRC_VR16_16	= SB_16,
	GVRC_VR16_17	= SB_17,
	GVRC_VR16_18	= SB_18,
	GVRC_VR16_19	= SB_19,
	GVRC_VR16_20	= SB_20,
	GVRC_VR16_FLAGS_BACKUP	= SB_21,
	GVRC_VR16_M1	= SB_22,
	GVRC_VR16_FLAGS	= SB_23,

	GVRC_VR16_FIRST	= GVRC_VR16_0,
	GVRC_VR16_LAST	= GVRC_VR16_20
};

/* Vector Register IDs (32 bits)
enum gvml_2vr16 {
	GVML_2VR16_0_1		= SB_0,
	GVML_2VR16_1_2		= SB_1,
	GVML_2VR16_2_3		= SB_2,
	GVML_2VR16_3_4		= SB_3,
	GVML_2VR16_4_5		= SB_4,
	GVML_2VR16_5_6		= SB_5,
	GVML_2VR16_6_7		= SB_6,
	GVML_2VR16_7_8		= SB_7,
	GVML_2VR16_8_9		= SB_8,
	GVML_2VR16_9_10		= SB_9,
	GVML_2VR16_10_11	= SB_10,
	GVML_2VR16_11_12	= SB_11,
	GVML_2VR16_12_13	= SB_12,
	GVML_2VR16_13_14	= SB_13,
};
*/
/* Flag IDs (in flags Vector Register) */
enum gvml_flgs {
	/* pre-defined flags for the flags vector-register */
	C_FLAG		= SM_BIT_0,		/* Carry in/out flag */
	UNUSED_B_FLAG		= SM_BIT_1,		/* Borrow in/out flag */
	UNUSED_OF_FLAG		= SM_BIT_2,		/* Overflow flag */
	PE_FLAG		= SM_BIT_3,		/* Parity error */
	PRINT_FLAG	= SM_BIT_4,		/* reserved for print debug function */
	UNUSED_M256_FLAG	= SM_BIT_5,		/* marker with 1 set for (each vector-register-index % 256) == 0 */
	UNUSED_M2K_FLAG	= SM_BIT_6,		/* marker with 1 set for (each vector-register-index % 2048) == 0 */

	/* Markers for general purpose usage Callee must preserve */
	GP0_FLAG	= SM_BIT_7,
	GP1_FLAG	= SM_BIT_8,
	GP2_FLAG	= SM_BIT_9,
	GP3_FLAG	= SM_BIT_10,
	GP4_FLAG	= SM_BIT_11,

	/* Markers for general purpose usage Callee may overwrite preserve */
	TMP0_FLAG	= SM_BIT_12,
	TMP1_FLAG	= SM_BIT_13,
	TMP2_FLAG	= SM_BIT_14,
	TMP3_FLAG	= SM_BIT_15,
};

/* Flag bitmasks (correspond to enum gvml_flgs flag IDs) */
enum gvml_mrks_n_flgs {
	GVRC_FLAG_0_UNAVAIL = 1 << SM_BIT_0,
	GVRC_FLAG_1 = 1 << SM_BIT_1,
	GVRC_FLAG_2 = 1 << SM_BIT_2,
	GVRC_FLAG_3_UNAVAIL = 1 << SM_BIT_3,
	GVRC_FLAG_4_UNAVAIL = 1 << SM_BIT_4,
	GVRC_FLAG_5 = 1 << SM_BIT_5,
	GVRC_FLAG_6 = 1 << SM_BIT_6,
	GVRC_FLAG_7 = 1 << SM_BIT_7,
	GVRC_FLAG_8 = 1 << SM_BIT_8,
	GVRC_FLAG_9 = 1 << SM_BIT_9,
	GVRC_FLAG_10 = 1 << SM_BIT_10,
	GVRC_FLAG_11 = 1 << SM_BIT_11,
	GVRC_FLAG_12 = 1 << SM_BIT_12,
	GVRC_FLAG_13 = 1 << SM_BIT_13,
	GVRC_FLAG_14 = 1 << SM_BIT_14,
	GVRC_FLAG_15 = 1 << SM_BIT_15,

//	GVML_C_FLAG	= 1 << C_FLAG,		/* Carry in/out flag */
//	GVML_B_FLAG	= 1 << B_FLAG,		/* Borrow in/out flag */
//	GVML_OF_FLAG	= 1 << OF_FLAG,		/* Overflow flag */
//	GVML_PE_FLAG	= 1 << PE_FLAG,		/* Parity error flag */
//	GVML_16B_MRK	= 1 << M16_FLAG,	/* marker with 1 set for each (vector-register-index % 16) == 0 */
//	GVML_256B_MRK	= 1 << M256_FLAG,	/* marker with 1 set for each (vector-register-index % 256) == 0 */
//	GVML_2KB_MRK	= 1 << M2K_FLAG,	/* marker with 1 set for each (vector-register-index % 2048) == 0 */
//	GVML_MRK0	= 1 << GP0_FLAG,	/* General purpose marker */
//	GVML_MRK1	= 1 << GP1_FLAG,	/* General purpose marker */
//	GVML_MRK2	= 1 << GP2_FLAG,	/* General purpose marker */
//	GVML_MRK3	= 1 << GP3_FLAG,	/* General purpose marker */
//	GVML_MRK4	= 1 << GP4_FLAG,	/* General purpose marker */
};

enum gvml_power2_sizes {
	GVML_P2_1	= 0,
	GVML_P2_2	= 1,
	GVML_P2_4	= 2,
	GVML_P2_8	= 3,
	GVML_P2_16	= 4,
	GVML_P2_32	= 5,
	GVML_P2_64	= 6,
	GVML_P2_128	= 7,
	GVML_P2_256	= 8,
	GVML_P2_512	= 9,
	GVML_P2_1K	= 10,
	GVML_P2_2K	= 11,
	GVML_P2_4K	= 12,
	GVML_P2_8K	= 13,
	GVML_P2_16K	= 14,
	GVML_P2_32K	= 15,
};

/* Vector Memory Register IDs */
enum gvml_vm_reg {
	GVML_VM_0	= 0,
	GVML_VM_1	= 1,
	GVML_VM_2	= 2,
	GVML_VM_3	= 3,
	GVML_VM_4	= 4,
	GVML_VM_5	= 5,
	GVML_VM_6	= 6,
	GVML_VM_7	= 7,
	GVML_VM_8	= 8,
	GVML_VM_9	= 9,
	GVML_VM_10	= 10,
	GVML_VM_11	= 11,
	GVML_VM_12	= 12,
	GVML_VM_13	= 13,
	GVML_VM_14	= 14,
	GVML_VM_15	= 15,
	GVML_VM_16	= 16,
	GVML_VM_17	= 17,
	GVML_VM_18	= 18,
	GVML_VM_19	= 19,
	GVML_VM_20	= 20,
	GVML_VM_21	= 21,
	GVML_VM_22	= 22,
	GVML_VM_23	= 23,
	GVML_VM_24	= 24,
	GVML_VM_25	= 25,
	GVML_VM_26	= 26,
	GVML_VM_27	= 27,
	GVML_VM_28	= 28,
	GVML_VM_29	= 29,
	GVML_VM_30	= 30,
	GVML_VM_31	= 31,
	GVML_VM_32	= 32,
	GVML_VM_33	= 33,
	GVML_VM_34	= 34,
	GVML_VM_35	= 35,
	GVML_VM_36	= 36,
	GVML_VM_37	= 37,
	GVML_VM_38	= 38,
	GVML_VM_39	= 39,
	GVML_VM_40	= 40,
	GVML_VM_41	= 41,
	GVML_VM_42	= 42,
	GVML_VM_43	= 43,
	GVML_VM_44	= 44,
	GVML_VM_45	= 45,

	GVML_VM_TMP	= 46,
	GVML_VM_INDEX	= 47,

	GVML_VM_FIRST	= GVML_VM_0,
	GVML_VM_LAST	= GVML_VM_45,
};

/* rownum registers used for function parameters */
#define RN_REG_G0 RN_REG_0
#define RN_REG_G1 RN_REG_1
#define RN_REG_G2 RN_REG_2
#define RN_REG_G3 RN_REG_3
#define RN_REG_G4 RN_REG_4
#define RN_REG_G5 RN_REG_5
#define RN_REG_G6 RN_REG_6
#define RN_REG_G7 RN_REG_7

/* rownum registers to hold predefined temporary vector-registers.
 * These rownum registers shouldn't change during program execution.
 */
//#define RN_REG_T0 RN_REG_8
//#define RN_REG_T1 RN_REG_9
//#define RN_REG_T2 RN_REG_10
//#define RN_REG_T3 RN_REG_11
//#define RN_REG_T4 RN_REG_12
//#define RN_REG_T5 RN_REG_13
//#define RN_REG_T6 RN_REG_14

/* rownum register to hold pre-defined flags vector-registers.
 * This rownum register shouldn't change during program execution.
 */
#define RN_REG_FLAGS RN_REG_15


/* l1_addr registers used for function parameters */
#define L1_ADDR_REG0 L1_ADDR_REG_0
#define L1_ADDR_REG1 L1_ADDR_REG_1

/* l1_addr registers holding predefined values */
#define L1_ADDR_REG_RESERVED L1_ADDR_REG_2
#define L1_ADDR_REG_INDEX L1_ADDR_REG_3

#define L2_ADDR_REG0 L2_ADDR_REG_0

#define RE_REG_G0 RE_REG_0
#define RE_REG_G1 RE_REG_1
#define RE_REG_T0 RE_REG_2
#define RE_REG_NO_RE RE_REG_3

#define EWE_REG_G0 EWE_REG_0
#define EWE_REG_G1 EWE_REG_1
#define EWE_REG_T0 EWE_REG_2
#define EWE_REG_NO_EWE EWE_REG_3


/* general purpose smaps registers */
#define SM_REG0 SM_REG_0
#define SM_REG1 SM_REG_1
#define SM_REG2 SM_REG_2
#define SM_REG3 SM_REG_3

/* smaps registers to hold predefined section maps.
 * These smaps registers shouldn't change during program execution.
 */
#define SM_0XFFFF SM_REG_4
#define SM_0X0001 SM_REG_5
#define SM_0X1111 SM_REG_6
#define SM_0X0101 SM_REG_7
#define SM_0X000F SM_REG_8
#define SM_0X0F0F SM_REG_9
#define SM_0X0707 SM_REG_10
#define SM_0X5555 SM_REG_11
#define SM_0X3333 SM_REG_12
#define SM_0X00FF SM_REG_13
#define SM_0X001F SM_REG_14
#define SM_0X003F SM_REG_15

/* Flag IDs (in flags Vector Register) */
/* pre-defined flags for the flags vector-register */
#define C_FLAG_DEF SM_BIT_0		/* Carry in/out flag */
#define B_FLAG_DEF SM_BIT_1		/* Borrow in/out flag */
#define OF_FLAG_DEF SM_BIT_2	/* Overflow flag */
#define PE_FLAG_DEF SM_BIT_3	/* Parity error */
#define M16_FLAG_DEF SM_BIT_4	/* marker with 1 set for (each vector-register-index % 16) == 0 */
#define M256_FLAG_DEF SM_BIT_5	/* marker with 1 set for (each vector-register-index % 256) == 0 */
#define M2K_FLAG_DEF SM_BIT_6	/* marker with 1 set for (each vector-register-index % 2048) == 0 */

/* Markers for general purpose usage Callee must preserve */
#define GP0_FLAG_DEF SM_BIT_7
#define GP1_FLAG_DEF SM_BIT_8
#define GP2_FLAG_DEF SM_BIT_9
#define GP3_FLAG_DEF SM_BIT_10
#define GP4_FLAG_DEF SM_BIT_11

/* Markers for general purpose usage Callee may overwrite preserve */
#define TMP0_FLAG_DEF SM_BIT_12
#define TMP1_FLAG_DEF SM_BIT_13
#define TMP2_FLAG_DEF SM_BIT_14
#define TMP3_FLAG_DEF SM_BIT_15

//enum rownum_regs_type_and_vals {
//
//	/* pre-defined temporary vector-registers */
//	RN_REG_T0_ROW = 16,
//	RN_REG_T1_ROW = 17,
//	RN_REG_T2_ROW = 18,
//	RN_REG_T3_ROW = 19,
//	RN_REG_T4_ROW = 20,
//	RN_REG_T5_ROW = 21,
//	RN_REG_T6_ROW = 22,
//
//	/* pre-defined flags vector-register */
//	RN_REG_FLAGS_ROW = 23,
//
//	/* General purpose vector-flags */
//	FIRST_GP_FLAG = GP0_FLAG,
//	LAST_GP_FLAG = GP4_FLAG,
//};

/*
enum mmb_vr16 {
	VR16_G0		= GVML_VR16_0,
	VR16_G1		= GVML_VR16_1,
	VR16_G2		= GVML_VR16_2,
	VR16_G3		= GVML_VR16_3,
	VR16_G4		= GVML_VR16_4,
	VR16_G5		= GVML_VR16_5,
	VR16_G6		= GVML_VR16_6,
	VR16_G7		= GVML_VR16_7,
	VR16_G8		= GVML_VR16_8,
	VR16_G9		= GVML_VR16_9,
	VR16_G10	= GVML_VR16_10,
	VR16_G11	= GVML_VR16_11,
	VR16_G12	= GVML_VR16_12,
	VR16_G13	= GVML_VR16_13,
	VR16_G14	= GVML_VR16_14,
	VR16_G15	= GVML_VR16_15,
	VR16_G16	= GVML_VR16_16,
	VR16_G17	= GVML_VR16_17,
	VR16_G18	= GVML_VR16_18,
	VR16_G19	= GVML_VR16_19,
	VR16_G20	= GVML_VR16_20,
	VR16_M4_IDX	= 15,
	VR16_T0		= 16,
	VR16_T1		= 17,
	VR16_T2		= 18,
	VR16_T3		= 19,
	VR16_T4		= 20,
	VR16_T5		= 21,
	VR16_PRINT	= 21,
	VR16_M4_IDX	= 22,
	VR16_FLAGS	= 23,

	VR16_G_FIRST	= VR16_G0,
	VR16_G_LAST	= VR16_G14,
	VR16_T_FIRST	= VR16_T0,
	VR16_T_LAST	= VR16_T6,

	APL_INVAL_ROWNUM = 0xff,
};
*/
typedef struct SFlagAvail {
	enum gvml_flgs flg;
	bool bavail;
} tSFlagAvail;

extern tSFlagAvail cFlagAvailTbl[];
extern int cNumFlags;

#define VR_RUNNING_IDX (GVRC_VR16_M1)
typedef enum egvrc {
	egvrcAvail,
	egvrcBlocked, // cannot use, cannot copy out
	egvrcUsed, // in use but can be copied out
	egvrcArg,
	egvrcLastRead, // used to pass into function, but, once read, may be discarded
} tegvrc;

typedef struct SVR_AVAIL {
	tegvrc mrks[NUM_MRKS];
	tegvrc vrs[NUM_VRS];
	tegvrc vmrs[NUM_VMRS];
	int use_mrk[NUM_MRKS];
	int use_vr[NUM_VRS];
	int use_vmr[NUM_VMRS];
	int mrk_regs[NUM_MRKS];
	int vr_regs[NUM_VRS];
	int num_mrks_alloced ;
	int num_vrs_alloced;
} tSVR_AVAIL;

void setup_avail_info(tSVR_AVAIL *avail_info);
int alloc_use(int num, int *use, tegvrc *resources, int avail, char *name);
void set_mrk_reg(tSVR_AVAIL *avail_info, int ireg, int imrk);
void set_vr_reg(tSVR_AVAIL *avail_info, int ireg, int ivr);
void set_seu_regs(tSVR_AVAIL *avail_info);
int init_avail_info(tSVR_AVAIL *avail_info, int num_mrks, int num_vrs, int num_vmrs);



void init_db(enum gvrc_vr16 vr_db, enum gvrc_vr16 vr_windex, enum gvml_mrks_n_flgs mrk_starts,
					enum gvml_mrks_n_flgs mrk_ends, u16 * db_buff, u16 * wstarts_buff, u16 db_cols,
                    u16 db_rows, u16 wnum, u16 * recs_end_idx, int total_hbs, tSVR_AVAIL * avail_info);
int ilp_1q_find_seq(	u16 * out_indices_buff, enum gvrc_vr16 vr_db, enum gvrc_vr16 vr_windex,
						enum gvml_mrks_n_flgs mrk_starts, enum gvml_mrks_n_flgs mrk_ends, int qnum,
						u16 *query, u16 * qlen_ret, int max_results, int total_hbs, tSVR_AVAIL * avail_info);
u16 ilp_1q_find_close(	u16 * out_vals_buff, u16 * out_indices_buff, enum gvrc_vr16 vr_db, u16 db_rows,
						enum gvrc_vr16 vr_windex, enum gvml_mrks_n_flgs mrk_starts,
						enum gvml_mrks_n_flgs mrk_ends, int qnum, u16 *query, u16 * qlen_ret, u16 k,
						tSVR_AVAIL * avail_info);
int ilp_1q_find_thresh(	enum gvrc_vr16 vr_db, enum gvrc_vr16 vr_windex,
						enum gvml_mrks_n_flgs mrk_starts, enum gvml_mrks_n_flgs mrk_ends, int qnum_src,
						u16 *query_data, u16 * qlen_ret, enum gvrc_vr16 vr_valid_mrks, tSVR_AVAIL * avail_info);
int ilp_rels_find(		enum gvrc_vr16 vr_db, enum gvrc_vr16 vr_windex,
						enum gvml_mrks_n_flgs mrk_starts, enum gvml_mrks_n_flgs mrk_ends, int qnum_src,
						u16 *query_data, u16 * qlen_ret, enum gvrc_vr16 vr_valid_mrks, tSVR_AVAIL * avail_info);
int ilp_1q_shift(		enum gvrc_vr16 vr_db, enum gvrc_vr16 vr_windex, enum gvrc_vr16 vr_input,
						u16 input_row, u16 qnum_start,
						enum gvml_mrks_n_flgs mrk_starts, enum gvml_mrks_n_flgs mrk_ends,
						enum gvml_mrks_n_flgs mrk_stored,
						enum gvrc_vr16 vr_stored_dbr_idx, enum gvrc_vr16 vr_stored_qidx,
						enum gvml_mrks_n_flgs mrk_error, tSVR_AVAIL * avail_info);

#endif /* IPL_APL_H_ */
