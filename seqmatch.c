// This is main etest version of seqmatch.c and should be deprecated!
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <wordexp.h>
#include <sys/stat.h>
#include <string.h>
#include <memory.h>

typedef uint16_t u16;
#define bool	_Bool
#define true	1
#define false	0

#include "seqmatch.h"
#include <float.h>
#include <time.h>
#include <unistd.h>

static bool gc_print_results = false;

static u16 popcount(u16 *pa, u16 *pb, u16 el_len)
{
	u16 count = 0;
	u16 a, b;
	for (u16 iel = 0; iel < el_len; iel++) {
		a = pa[iel]; b = pb[iel];
		for (u16 ib = 0; ib < sizeof(u16) * 8; ib++) {
			if ((a & 0x01) != (b & 0x01)) {
				count++;
			}
			a >>= 1; b >>= 1;
		}
	}
	return count;

}

tSMDB *seq_match_init(int max_recs, u16 el_len)
{
	tSMDB *db = (tSMDB *)malloc(sizeof(tSMDB));
	db->el_len = el_len;
	for (int ihb = 0; ihb < NUM_HBS; ihb++) {
		db->recs[ihb] = (u16 **)malloc(max_recs * sizeof(u16 *));
		db->lens[ihb] = (u16 *)calloc(max_recs, sizeof(u16));
		db->num_recs[ihb] = 0;
	}
	db->start_idx[0] = 0;
	return db;
}

static void seq_match_set_ihb_start_idx(tSMDB *db, int ihb, int start_idx)
{
	db->start_idx[ihb] = start_idx;
}

void seq_match_add_rec(tSMDB *db, int ihb, __attribute__((unused)) u16 irec_abs, u16 *els, u16 nels)
{
	int irec = db->num_recs[ihb];
	db->recs[ihb][irec] = (u16 *)malloc(nels * db->el_len * sizeof(u16));
	memcpy(db->recs[ihb][irec], els, nels * db->el_len * sizeof(u16));
	db->lens[ihb][irec] = nels;
	db->num_recs[ihb]++;
}

int seq_match_find_close(	tSMDB *db, u16 *ret_buf, __attribute__((unused)) u16 * ret_hd_buf,
							int max_ret, __attribute__((unused)) int iq, u16 *qdata,
							__attribute__((unused)) u16 *hds, __attribute__((unused)) u16 *copy_move, u16 qlen)
{
	//printf("seq_match_find_close called. max_ret = %hu\n", max_ret);
	u16 num_found = 0;
	int worst_idx_idx = -1;
	u16 worst_hd = 0;
	for (u16 ir = 0; ir < HB_LEN; ir++) { // HB_LEN is more than the possible number of recs for that hb
		for (int ihb = 0; ihb < NUM_HBS; ihb++) {
			if (db->lens[ihb][ir] != qlen) continue;
			if (db->lens[ihb][ir] != qlen) continue;
			u16 hd = 0;
			for (u16 iel = 0; iel < qlen; iel++) {
				u16 *qlw = &(qdata[iel * db->el_len]);
				hd = (u16)(hd + popcount(&(db->recs[ihb][ir][iel * db->el_len]), qlw, db->el_len));
			}
			//printf("hd for irec %hu is %hu.\n", ir, hd);
			if (num_found < ((int)max_ret - 1)) {
				ret_hd_buf[num_found] = hd;
				ret_buf[num_found++] = ir;
				worst_hd = 0;
				for (int ifound = 0; ifound < (int)num_found; ifound++) {
					if (ret_hd_buf[ifound] >= worst_hd) {
						worst_hd = ret_hd_buf[ifound];
						worst_idx_idx = ifound;
					}
				}
				//printf("less than max_ret. placed at %d. worst is now %hu at pos %d\n", num_found-1, worst_hd, worst_idx_idx);
				continue;
			}
			if (hd >= worst_hd) continue;
			//printf("accepting hd and placing in pos %d.\n", worst_idx_idx);
			worst_hd = 0;
			ret_hd_buf[worst_idx_idx] = hd;
			ret_buf[worst_idx_idx] = ir;
			for (int ifound = 0; ifound < max_ret; ifound++) {
				if (ret_hd_buf[ifound] >= worst_hd) {
					worst_hd = ret_hd_buf[ifound];
					worst_idx_idx = ifound;
				}
				//printf("replaced. worst is now %hu at pos %d\n", worst_hd, worst_idx_idx);
			}
		}
	}
	ret_buf[num_found++] = 0xffff;
	//printf("%hu found\n", num_found);
	return num_found;
}

int seq_match_find_seq(	tSMDB *db, u16 *ret_buf, __attribute__((unused)) u16 * ret_hd_buf,
						int max_ret, __attribute__((unused)) int iq, u16 *qdata,
						__attribute__((unused)) u16 *hds, __attribute__((unused)) u16 *copy_move, u16 qlen)
{
	int num_found = 0;
	u16 ir_curr[NUM_HBS];
	memset(ir_curr, 0, NUM_HBS * sizeof(u16));
//	if (iq == 7) {
//		for (int iel = 0; iel < (int)qlen; iel++) {
//			printf("qnum %d: iel %d: %hu\n", iq, iel, qdata[iel * db->el_len]);
//		}
//
//	}
	bool b_keep_going = true;
	while (b_keep_going) {
		b_keep_going = false;
		for (int ihb = 0; ihb < NUM_HBS; ihb++) {
			if (ir_curr[ihb] == 0xffff) continue;
			b_keep_going = true;
			for (u16 ir = ir_curr[ihb]; ir < HB_LEN;  ir++) { // HB_LEN is more than the possible number of recs for that hb
				if (ir >= db->num_recs[ihb]) {
					//printf("seq_match_find_seq: iq: %d, found all there are for ihb %d. %hu so far\n", iq, ihb, num_found);
					ir_curr[ihb] = 0xffff;
					break;
				}
				if (db->lens[ihb][ir] != qlen) continue;
				bool bfound = true;
				for (u16 iel = 0; iel < qlen; iel++) {
					u16 *qlw = &(qdata[iel * db->el_len]);
	//				if (copy_move[iel] != 0) {
	//					qlw = &(db->recs[ir][(iel - copy_move[iel]) * db->el_len]);
	//				}
					u16 hd = popcount(&(db->recs[ihb][ir][iel * db->el_len]), qlw, db->el_len);
					if (hd > 0) {
						bfound = false;
						break;
					}
				}
				if (bfound) {
					ret_hd_buf[num_found] = 0;
					ret_buf[num_found++] = (u16)((int)ir + db->start_idx[ihb]);
					//printf("seq_match_find_seq: iq: %d, val # %hu is %hu.\n", iq, num_found-1, ret_buf[num_found-1]);
					ir_curr[ihb] = (u16)(ir + 1);
					break;
				}
			} // loop over recs in hb
			if (num_found >= max_ret) break;
		} // loop over hbs
		if (num_found >= max_ret) {
			//printf("seq_match_find_seq: iq: %d, found a full %hu.\n", iq, num_found);
			break;
		}
	} // while keep_goig
//	ret_buf[num_found++] = 0xffff;
	return num_found;
}


int seq_match_find_thresh(	tSMDB *db, u16 *ret_buf, __attribute__((unused)) u16 * ret_hd_buf,
						__attribute__((unused)) int max_ret, __attribute__((unused)) int iq, u16 *qdata,
						__attribute__((unused)) u16 *hds, __attribute__((unused)) u16 *copy_move, u16 qlen)
{
	int num_found = 0;
	u16 ir_curr[NUM_HBS];
	int db_pos[NUM_HBS];
	memset(ir_curr, 0, NUM_HBS * sizeof(u16));
	memset(db_pos, 0, NUM_HBS * sizeof(int));
//	if (iq == 38) {
//		printf("seq_match_find_thresh: %d. qlen %hu\n", iq, qlen);
//		for (int iel = 0; iel < (int)qlen; iel++) {
//			printf("iel %d: %hu, %hu, %hu, %hu, hd: %hu cm: %hu\n",
//					iel, qdata[(iel * db->el_len)], qdata[(iel * db->el_len)+1], qdata[(iel * db->el_len)+2],
//					qdata[(iel * db->el_len)+3], hds[iel], copy_move[iel]);
//		}
//
//	}
	int wthresh = 0;
	for (int iel = 0; iel < (int)qlen; iel++) {
		wthresh += (int)(hds[iel]);
	}
	int ret_line = (iq % 64) / 4;
	int ret_pos_delta = 4 - (iq % 4);
	bool b_keep_going = true;
	while (b_keep_going) {
		b_keep_going = false;
		for (int ihb = 0; ihb < NUM_HBS; ihb++) {
			if (ir_curr[ihb] == 0xffff) continue;
			b_keep_going = true;
			for (u16 ir = ir_curr[ihb]; ir < HB_LEN;  ir++) { // HB_LEN is more than the possible number of recs for that hb
				if (ir >= db->num_recs[ihb]) {
//					printf("seq_match_find_thresh: iq: %d, found all there are for ihb %d. %d so far\n", iq, ihb, num_found);
					ir_curr[ihb] = 0xffff;
					break;
				}
				db_pos[ihb] += (int)(db->lens[ihb][ir]);
				if (db->lens[ihb][ir] != qlen) continue;
				bool bfound = true;
				u16 whd = 0;
				for (u16 iel = 0; iel < qlen; iel++) {
					u16 *qlw = &(qdata[iel * db->el_len]);
					if (copy_move[iel] != 0) {
						qlw = &(db->recs[ihb][ir][(iel - copy_move[iel]) * db->el_len]);
					}
					u16 hd = popcount(&(db->recs[ihb][ir][iel * db->el_len]), qlw, db->el_len);
					if (hds[iel] == 0 && hd > 0) {
						bfound = false;
						break;
					}
					whd = (u16)(whd + hd);
				}
				if (bfound && whd > (u16)wthresh) {
					bfound = false;
				}
				if (bfound) {
					ret_hd_buf[num_found] = whd;
//					printf("seq_match_find_thresh: found match for iq %d, whd = %hu. wthresh = %d.\n", iq, whd, wthresh);
					int ret_pos = (ihb * HB_LEN) + db_pos[ihb] - ret_pos_delta;
					ret_buf[ret_pos] = (u16)(ret_buf[ret_pos] | (1 << ret_line));
					//ret_buf[num_found++] = (u16)((int)ir + db->start_idx[ihb]);
//					printf("seq_match_find_thresh: iq: %d, val # %d writes to line %d at pos %d of ihb %d. idx = %d\n",
//							iq, num_found, ret_line, db_pos[ihb] - ret_pos_delta, ihb, (int)ir + db->start_idx[ihb]);
					ir_curr[ihb] = (u16)(ir + 1);
					num_found++;
					break;
				}
			} // loop over recs in hb
//			if (num_found >= max_ret) break;
		} // loop over hbs
//		if (num_found >= max_ret) {
//			printf("seq_match_find_thresh: iq: %d, found a full %d.\n", iq, num_found);
//			break;
//		}
	} // while keep_goig
//	ret_buf[num_found++] = 0xffff;
	return num_found;
}



//u16 seq_match_find_match(tSMDB *db, u16 *ret_buf, u16 max_ret, __attribute__((unused)) u16 iq, u16 *qdata,
//                         u16 *hds, u16 *copy_move, u16 qlen)
int seq_match_find_match(	tSMDB *db, u16 *ret_buf, __attribute__((unused)) u16 * ret_hd_buf,
							__attribute__((unused)) int max_ret, __attribute__((unused)) int iq, u16 *qdata,
							__attribute__((unused)) u16 *hds, __attribute__((unused)) u16 *copy_move, u16 qlen)
{
	u16 num_found = 0;
	for (u16 ir = 0; ir < HB_LEN;  ir++) { // HB_LEN is more than the possible number of recs for that hb
		for (int ihb = 0; ihb < NUM_HBS; ihb++) {
			if (ir >= db->num_recs[ihb]) continue;
			if (db->lens[ihb][ir] != qlen) continue;
			bool bfound = true;
	//		char dbw[qlen+1];
	//		u16 cbuf[qlen];
	//		u16 hdbuf[qlen];
			for (u16 iel = 0; iel < qlen; iel++) {
	//			dbw[iel] = (char)db->recs[ir][(iel * db->el_len)];
	//			cbuf[iel] = copy_move[iel]; hdbuf[iel] = hds[iel];
				u16 *qlw = &(qdata[iel * db->el_len]);
				if (copy_move[iel] != 0) {
					qlw = &(db->recs[ihb][ir][(iel - copy_move[iel]) * db->el_len]);
				}
				u16 hd = popcount(&(db->recs[ihb][ir][iel * db->el_len]), qlw, db->el_len);
				if (hd > hds[iel]) {
					bfound = false;
					break;
				}
			}
			//dbw[qlen] = '\0';
			if (bfound) {
				ret_buf[num_found++] = ir;
			}
			if (num_found >= (max_ret - 1)) break;
		}
	}
	ret_buf[num_found++] = 0xffff;
	return num_found;
}

//int testlink(u16 * ret_buf) {
//	if (ret_buf == NULL) {
//		return 1;
//	}
//	return 0;
//}
//
int cpu_testseq(u16 *iquery, u16 *hd_thresh, u16 *copy_move, u16 *wstarts, u16 *isample, u16 *iqlens,
                int num_rows, int vec_size, int numw, int numq, u16 *ret_buf, u16 * ret_hd_buf,
				int ret_buf_max, int max_ret_per_q, bool b_nearest, bool b_thresh)
{
	printf("cpu_testseq called for %hu queries %hu max rets each. \n", numq, max_ret_per_q);
	pfn_find fn_test;
	if (b_nearest) {
		fn_test = seq_match_find_close;
	}
	else if (b_thresh) {
		fn_test = seq_match_find_thresh;
	}
	else {
		fn_test = seq_match_find_seq;
	}
	if (num_rows != EL_NUM_WORDS) {
		return 1;
	}
	tSMDB *smdb = seq_match_init(numw, EL_NUM_WORDS);
	u16 irec = 0;
	int ihb = 0;
	for (u16 iw = 0; iw < numw - 1; iw++) {
		u16 pos = wstarts[iw];
//		u16 startel = isample[pos];
//		if (startel == 0)
//			continue;
		u16 endpos = wstarts[iw + 1];
//		if (iw > 1360 && iw < 1380) {
//			printf("cpu_testseq: iw = %hu. pos = %hu. endpos = %hu. new_ihb = %d.\n", iw, pos, endpos, endpos / HB_LEN);
//		}
		if ((endpos / HB_LEN) > ihb) {
			//printf(	"wstart %hu seeming to end at %hu, exceeds buf len, incrementing ihb to %d. Setting start to %d\n",
			//		iw, endpos, ihb+1, (int)irec + 1);
			ihb++;
			irec++;
			seq_match_set_ihb_start_idx(smdb, ihb, irec);
			continue;
		}
		//u16 endel = isample[endpos];
		u16 reclen = (u16)(endpos - pos);
		u16 data[reclen * EL_NUM_WORDS];
		for (u16 iel = 0; iel < reclen; iel++) {
			for (u16 irow = 0; irow < EL_NUM_WORDS; irow++) {
				data[(iel * EL_NUM_WORDS) + irow] = isample[(irow * vec_size) + pos + iel];
			}
		}
		seq_match_add_rec(smdb, ihb, irec, data, reclen);
		irec++;
	}

	struct timespec start, stop;

	int ret_buf_len = numq*max_ret_per_q*(int)sizeof(u16);
	if (b_thresh) {
		ret_buf_len = NUM_HBS * HB_LEN * sizeof(u16);
		memset(ret_buf, 0, ret_buf_len);
	}
	else {
		memset(ret_buf, 0xff, ret_buf_len);
	}
	memset(ret_hd_buf, 0xff, numq*max_ret_per_q*sizeof(u16));
	clock_gettime(CLOCK_REALTIME, &start);
	int qpos = 0;
	int tot_ret = 0;
	for (int iq = 0; iq < numq; iq++) {
		if ((tot_ret + max_ret_per_q) > ret_buf_max) {
			break;
		}
		u16  qlen = iqlens[iq];
		u16 qdata[qlen * EL_NUM_WORDS];
		u16 *one_copy_move = &(copy_move[qpos]);
		u16 *hds = &(hd_thresh[qpos]);
		for (int iel = 0; iel < (int)qlen; iel++) {
			for (int irow = 0; irow < EL_NUM_WORDS; irow++) {
				qdata[(iel * EL_NUM_WORDS) + irow] = iquery[(qpos * EL_NUM_WORDS) + (irow * qlen) + iel];
			}
		}
//		u16 num_ret = seq_match_find_match(smdb, &(ret_buf[tot_ret]), max_ret_per_q, iq, qdata, hds, one_copy_move, qlen);
		int ret_buf_first = max_ret_per_q*iq;
		if (b_thresh) {
			ret_buf_first = 0;
		}
		int num_ret = (*fn_test)(smdb, &(ret_buf[ret_buf_first]), &(ret_hd_buf[ret_buf_first]), max_ret_per_q, iq,
								qdata, hds, one_copy_move, qlen);
		tot_ret += num_ret;

		qpos += qlen; // (qlen * EL_NUM_WORDS);
	}
	//usleep(1000000);
	clock_gettime(CLOCK_REALTIME, &stop);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
	double result = (double)((stop.tv_sec - start.tv_sec) * 1e6 + (stop.tv_nsec - start.tv_nsec) / 1e3);    // in microseconds
#pragma GCC diagnostic pop
	printf("All cpu results returned in %.*e us\n", DECIMAL_DIG, result);

	if (gc_print_results) {
		printf("cpu replies:\n");
		int rpos = 0;
		for (int iq = 0; iq < numq; iq++) {
			for (int iret = 0; iret < max_ret_per_q; iret++, rpos++) {
				if (ret_buf[rpos] == 0xffff) continue;
				printf("cpu: rpos %hu, qnum %hu: val %hu, hd %hu\n", rpos, iq, ret_buf[rpos], ret_hd_buf[rpos]);
			}
		}
//		u16 num_q_ret = 0;
//		for (u16 iret = 0; iret < tot_ret; iret++) {
//			if (num_q_ret >= numq) break;
//			printf("cpu: iret %hu, qnum %hu: val %hu, hd %hu\n", iret, num_q_ret, ret_buf[iret], ret_hd_buf[iret]);
//			if (ret_buf[iret] == 0xffff) num_q_ret++;
//		}
	}
	return 0;
}
