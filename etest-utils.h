/*
 * etest-utils.h
 *
 *  Created on: Aug 16, 2019
 *      Author: eehrman
 */

#ifndef NEW_ROOT_GSI_DEVICE_LIBS_TEST_ETEST_TEST_SRC_ETEST_UTILS_H_
#define NEW_ROOT_GSI_DEVICE_LIBS_TEST_ETEST_TEST_SRC_ETEST_UTILS_H_

u16 _vm_reg_to_set_ext(int vm_reg, uint *parity_set, uint *row_in_set, uint *parity_grp, uint *parity_row);
u8 gsi_encode_l2_addr(uint byte_idx, uint bit_idx);




#endif /* NEW_ROOT_GSI_DEVICE_LIBS_TEST_ETEST_TEST_SRC_ETEST_UTILS_H_ */
