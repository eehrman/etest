#ifndef READL_RL_TEST_APL_H_
#define READL_RL_TEST_APL_H_

typedef unsigned short int u16;

void set_rl(u16 val);
void set_gl(u16 val);
void set_ggl(u16 val);

void get_rl(u16 *buffer);
void get_gl(u16 *buffer);
void get_ggl(u16 *buffer);

void etest_test_apl_init(void);
void gvrc_cpy_imm_16_mrk(enum gvml_mrks_n_flgs vreg, u16 val, u16 mrk);
void gvrc_cpy_imm_16(enum gvrc_vr16 vreg, u16 val);
void gvrc_eq_imm_16(enum gvml_mrks_n_flgs res_mrk, enum gvrc_vr16 x, u16 imm_val);
void gvrc_calc_index_m1_16(enum gvrc_vr16 dst);
void gvrc_m_reset(enum gvml_mrks_n_flgs mrk);
void gvrc_m_cpy(enum gvml_mrks_n_flgs dst_mrk, enum gvml_mrks_n_flgs src_mrk);
void gvrc_iv_extract_u16_mrk_g32k(u16 *out_val, enum gvrc_vr16 data_vr, enum gvml_mrks_n_flgs mrk_in);
void gvrc_iv_extract_u16_mrk_g2k(u16 *out_buf, enum gvrc_vr16 data_vr, enum gvml_mrks_n_flgs mrk_in);
void gvrc_iv_test_for_one_true_g32k(u16 *pbfound, enum gvml_mrks_n_flgs amrk);
void print_m_output_clean(const char *name, enum gvml_mrks_n_flgs m_for_out, u16 num_out);
void print_m_output_range(const char *name, enum gvml_mrks_n_flgs m_for_out, u16 out_start, u16 num_out);
void print_vr_output_clean(const char *name, enum gvrc_vr16 vr_for_out, u16 num_out);
void print_vr_output_range(const char *name, enum gvrc_vr16 vr_for_out, u16 out_start, u16 num_out);
void print_vr_output_spread5(	const char *name, enum gvrc_vr16 vr0, enum gvrc_vr16 vr1,
								enum gvrc_vr16 vr2, enum gvrc_vr16 vr3, enum gvrc_vr16 vr4,
								u16 out_start, u16 num_out);
void extract_vr(u16 * ret_buf, enum gvrc_vr16 vr_valid_mrks);

void __gvml_cpy_imm_16(enum gvrc_vr16 vreg, u16 val);
void gvrc_set_16_msk(enum gvrc_vr16 avreg, u16 amsk);
void gvrc_create_index_m1_g2k_t0(enum gvrc_vr16 dst);
void gvrc_m_or(enum gvml_mrks_n_flgs res, enum gvml_mrks_n_flgs x, enum gvml_mrks_n_flgs y);
/* copy 16 bit vector register: dst = src */
void gvrc_cpy_16(enum gvrc_vr16 dst, enum gvrc_vr16 src);
/* copy 16 bit vector register using mask: dst = (dst & ~msk) | (src & msk) */
void gvrc_cpy_16_msk(enum gvrc_vr16 dst, enum gvrc_vr16 src, u16 msk);
/* shift src 1 bit up (arithmetic shift left) */
void gvrc_asl1(enum gvrc_vr16 adst, enum gvrc_vr16 asrc);
/* shift src 1 bit down (arithmetic shift right) */
void gvrc_asr1(enum gvrc_vr16 adst, enum gvrc_vr16 asrc);
void gvrc_asl2(enum gvrc_vr16 adst, enum gvrc_vr16 asrc);
void gvrc_asr2(enum gvrc_vr16 adst, enum gvrc_vr16 asrc);
void gvrc_asl4(enum gvrc_vr16 adst, enum gvrc_vr16 asrc);
void gvrc_asr4(enum gvrc_vr16 adst, enum gvrc_vr16 asrc);
/* Take four vrs, each with data in only one section and spread it from lsb to msb
 * src_mask tells the fn which bit of each src reg to take, e.g lsb 1 << 0, 4th bit 1 << 3*/
void gvrc_spread4(	enum gvrc_vr16 adst, enum gvrc_vr16 as1, enum gvrc_vr16 as2, enum gvrc_vr16 as3,
					enum gvrc_vr16 as4, u16 asrc_mask) ;
void gvrc_spread5(	enum gvrc_vr16 adst, enum gvrc_vr16 as1, enum gvrc_vr16 as2, enum gvrc_vr16 as3,
					enum gvrc_vr16 as4, enum gvrc_vr16 as5, u16 asrc_mask);
void gvrc_iv_mark_min_u16_m1_g2k(enum gvml_mrks_n_flgs mrk_out, enum gvrc_vr16 vr_in,
								 enum gvrc_vr16 t0, enum gvrc_vr16 t1, enum gvrc_vr16 t2, enum gvrc_vr16 t3);
void gvrc_copy_N_bytes_l2_to_l1(u8 l1_bank_id, enum gvml_vm_reg vm_reg, uint l1_grp, int num_bytes, bool l2_ready_set, u8 l2_start_byte);
void gvrc_copy_N_bytes_l1_to_l2(u8 l1_bank_id, enum gvml_vm_reg vm_reg, uint l1_grp, int num_bytes, bool l2_ready_set, u8 l2_start_byte);
void gvrc_io_load_16(enum gvrc_vr16 dst, enum gvml_vm_reg vm_reg);
void gvrc_io_store_16(enum gvml_vm_reg vm_reg, enum gvrc_vr16 src);
void gvrc_2to2(enum gvrc_vr16 aa, enum gvrc_vr16 ab, enum gvrc_vr16 at1);
void gvrc_3to2(	enum gvrc_vr16 aa, enum gvrc_vr16 ab, enum gvrc_vr16 ac,
				enum gvrc_vr16 at1, enum gvrc_vr16 at2);

void gvrc_reset_16(enum gvrc_vr16 vreg);
void gvrc_m_not(enum gvml_mrks_n_flgs res, enum gvml_mrks_n_flgs x);
void gvrc_iv_brdcst_mrk_data_g2k(enum gvrc_vr16 vr_dst, enum gvrc_vr16 vr_src, enum gvml_mrks_n_flgs m_mrk);
void gvrc_cpy_from_mrk_16_msk(enum gvrc_vr16 avr_dst, u16 asrc_mrk, u16 adst_msk);
void gvrc_cpy_to_mrk_16_msk(u16 adst_msk, enum gvrc_vr16 avr_src, u16 asrc_mrk);
void gvrc_copy_hhb_16(enum gvrc_vr16 mov);

void set_bank_en_mask(u8 bank_en);
void enable_bank(uint apc_id, uint bank_id);
void enable_bank_reset(void);

int gvml_apl_init(void);

#endif /* READL_RL_TEST_APL_H_ */
