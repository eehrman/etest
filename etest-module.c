/*
 * Copyright (C) 2019, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

//#include <gsi/task_api.h>

typedef unsigned short u16;

//#include <string.h>
#include <stddef.h>
#include <gsi/libapl.h>
#include <gsi/libsys/log.h>
#include <gsi/libgal.h>

#include "etest-intern.h"
//#include <gsi/libsys/log.h>
//#include <gsi/libapl.h>
//#include <gsi/libgal.h>
#include "ipl-apl-funcs.apl.h"
#include "etest-apl-funcs.apl.h"
#define GSI_UNUSED(_param_)	_param_ __attribute__((unused))
#define NULL ((void *)0)
#include "my_gal_funcs.h"


char *strncpy(char *dest, const char *src, size_t n);

#define BUFFER_SIZE 16
#define NUM_OF_NIBBLES 4
#define NUM_OF_BITS 4
#define SUCCESS 0
#define FAILURE -1
#define CHECK_RET(name,ret) ( (ret == SUCCESS ? gsi_info("%s: PASS",name) : gsi_info("%s: FAIL",name) ) )
#define L2_MEMORY_SIZE ((int)((u32)1<<15))
#define HB_NUM_COLS 2048
#define NUM_APUCS 2
#define APUC_NUM_BANKS 4
#define NUM_HBS (NUM_APUCS * APUC_NUM_BANKS * 2)
#define BANK_NUM_COLS (HB_NUM_COLS * 2)
#define CACHE_LINE_SIZE (32)

#define CHUNK_SIZE_512 (512)
#define GSI_CAST_32   (uint32_t)(unsigned long)
#define GSI_CAST_32_PTR  (uint32_t *)(unsigned long)

typedef struct SDBData {
	u16 num_rows;
	u16 num_cols;
	u16 numw;
	u16 recs_end_idx[NUM_HBS];
	int total_hbs;
	enum gvml_flgs mrk_start; // The ordinal, not 1 << X
	enum gvml_flgs mrk_end;
	enum gvml_flgs mrk_stored;
	int last_iflag_used;
	enum gvrc_vr16 vr_stored_dbr_idx;
	enum gvml_vm_reg vm_stored_dbr_idx;
	enum gvrc_vr16 vr_stored_qidx;
	enum gvml_vm_reg vm_stored_qidx;
	enum gvrc_vr16 vr_find_thresh_flags;
	enum gvml_vm_reg vm_find_thresh_flags;
	enum gvml_vm_reg vm_for_output;
} tSDBData;

tSDBData g_db_info;


GAL_INCLUDE_INIT_TASK

static u16 get_expected_value_ggl(u16 data)
{
	u16 mask = 0xf;
	u16 expected_value = 0;
	for (u16 i = 0; i < NUM_OF_NIBBLES; i++) {
		if ((data & mask) == mask) {
			expected_value |= mask;
		}
		mask = (u16)((u16)mask << 4);
	}

	return expected_value;
}

static gsi_prod_status_t set_get_rl()
{
	gsi_prod_status_t ret = SUCCESS;
	u16 data = 7;
	u16 buffer[BUFFER_SIZE] = {0};

	set_rl(data);
	get_rl(buffer);

	for (u16 i = 0; i < BUFFER_SIZE; i++) {
		if (buffer[i] != data) {
			ret = FAILURE;
			break;
		}
	}

	return ret;
}

static gsi_prod_status_t set_get_gl()
{
	gsi_prod_status_t ret = SUCCESS;
	u16 data = 0xFFFF; // any value other than 0xFFFF should evaluate to 0
	u16 expected_value = (data == 0xFFFF ? 0xFFFF : 0);
	u16 buffer[BUFFER_SIZE] = {0};

	set_gl(data);
	get_gl(buffer);

	for (u16 i = 0; i < BUFFER_SIZE; i++) {
		if (buffer[i] != expected_value) {
			ret = FAILURE;
			break;
		}
	}

	return ret;
}

static gsi_prod_status_t set_get_ggl()
{
	gsi_prod_status_t ret = SUCCESS;
	u16 data = 0xF0F0;
	u16 buffer[BUFFER_SIZE] = {0};
	u16 expected_value = get_expected_value_ggl(data);

	set_ggl(data);
	get_ggl(buffer);

	for (u16 i = 0; i < BUFFER_SIZE; i++) {
		if (buffer[i] != expected_value) {
			ret = FAILURE;
			break;
		}
	}

	return ret;
}

static void dump_all_mmb(u8 * dump_buf, enum gvml_vm_reg vm_for_output)
{
	uint num_ouputs_per_apuc = 4;
	uint num_cols_per_output = BANK_NUM_COLS / num_ouputs_per_apuc;
	uint num_bytes_per_output = APUC_NUM_BANKS * sizeof(u16) * num_cols_per_output;
	uint num_outputs_per_ivr = NUM_APUCS * num_ouputs_per_apuc;

	static gal_l2dma_hndl_t *ioxs[GSI_APUC_NUM_APCS];
	struct gal_l2dma_l4_l2_transaction transaction;
	transaction.l2_addr.l2_byte_row = 0;
	transaction.l2_addr.l2_col_group = 0;
	transaction.l2_mode = GAL_L2T_MODE_64;
	transaction.l2_rep_colgrp_stride = 0;
	transaction.l2_step_col_group = 0;
	transaction.l4_addr = 0; // invalid! Will be set immediately
	transaction.l4_step_stride = 0;
	transaction.num_steps = 1;
	transaction.step_size = num_bytes_per_output;

	memset(dump_buf, 0, BANK_NUM_COLS * NUM_APUCS * APUC_NUM_BANKS * (SB_23+1) * sizeof(u16));
	int ivr_stage = (int)GVRC_VR16_FLAGS_BACKUP;
	gvrc_cpy_16(0, GVRC_VR16_M1);
	for (int ivr = 0; ivr <= SB_23; ivr++) {
		for (int ihalf = 0; ihalf < 2; ihalf++) {
//			gsi_log("dump_all_mmb: storing ivr %d", ivr);
			if (ihalf == 0) {
				gvrc_io_store_16(g_db_info.vm_for_output, ivr);
			}
			else {
				gvrc_cpy_16(ivr_stage, ivr);
				gvrc_copy_hhb_16(ivr_stage);
				gvrc_io_store_16(g_db_info.vm_for_output, ivr_stage);
			}

//			gsi_log("dump_all_mmb: sending from l1 to l2");

			gal_l2dma_l2_ready_rst_all();
			for (int ibank = 0; ibank < GSI_MMB_NUM_BANKS; ibank++) {
				u8 bank_id = (u8)ibank;
				uint l1_grp = 0;
				int num_vert_bytes = sizeof(u16);
				bool l2_ready_set = (ibank == (GSI_MMB_NUM_BANKS-1));
				u8 l2_start_byte = (u8)(ibank * sizeof(u16)); // 0; //
				gvrc_copy_N_bytes_l1_to_l2(	bank_id, g_db_info.vm_for_output, l1_grp, num_vert_bytes,
											l2_ready_set, l2_start_byte);
			}
			// Repeat for the second APC.
//			gsi_log("dump_all_mmb:l1 to l2 complete. Writing to l4");

			int offset = num_bytes_per_output * ((ivr * num_outputs_per_ivr) + ihalf);
//			gsi_log("apuc 0. ihalf %d. ivr %d. offset %d", ihalf, ivr, offset);
			transaction.l2_addr.l2_col_group = 0;
			transaction.l4_addr = (u8 *)(dump_buf + offset);
			ioxs[0] = gal_l2dma_l2_to_mem_start(0, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
			gal_l2dma_sync(ioxs[0], true);

			offset = num_bytes_per_output * ((ivr * num_outputs_per_ivr) + 2 + ihalf);
//			gsi_log("apuc 0. ihalf %d. 2nd hb ivr %d. offset %d", ihalf, ivr, offset);
			transaction.l2_addr.l2_col_group = 32;
			transaction.l4_addr = (u8 *)(dump_buf + offset);
			ioxs[0] = gal_l2dma_l2_to_mem_start(0, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
			gal_l2dma_sync(ioxs[0], true);

	//		if (ivr == 22) {
	//			gsi_log("dumping ivr %d:", ivr);
	//			for (int i = 0; i < 256; i++) {
	//				gsi_log("%d: 0x%hx", i, *((u16 *)(transaction.l4_addr) + i));
	//			}
	//		}

			offset = num_bytes_per_output * ((ivr * num_outputs_per_ivr) + num_ouputs_per_apuc + ihalf);
//			gsi_log("apuc 1. ihalf %d. ivr %d. offset %d", ihalf, ivr, offset);
			transaction.l2_addr.l2_col_group = 0;
			transaction.l4_addr = (u8 *)(dump_buf + offset);
			ioxs[1] = gal_l2dma_l2_to_mem_start(1, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
			gal_l2dma_sync(ioxs[1], true);

			offset = num_bytes_per_output * ((ivr * num_outputs_per_ivr) + num_ouputs_per_apuc + 2 + ihalf);
//			gsi_log("apuc 1. ihalf %d. 2nd hb ivr %d. offset %d", ihalf, ivr, offset);
			transaction.l2_addr.l2_col_group = 32;
			transaction.l4_addr = (u8 *)(dump_buf + offset);
			ioxs[1] = gal_l2dma_l2_to_mem_start(1, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
			gal_l2dma_sync(ioxs[1], true);
} // end ihalf
	}

}
void print_m_output_range(const char *name, enum gvml_mrks_n_flgs m_for_out, u16 out_start, u16 num_out)
{
//	enum gvrc_vr16 vr_col_idx = GVRC_VR16_M1;
	enum gvml_mrks_n_flgs m_curr = (enum gvml_mrks_n_flgs)(1 << PRINT_FLAG);
	//gvrc_calc_index_m1_16(vr_col_idx);
	gvrc_m_reset(m_curr);
	u16 ret_val = 93;
	for (u16 io = out_start; io < out_start + num_out; io++) {
		gvrc_eq_imm_16(m_curr, GVRC_VR16_M1, (u16)io);
		gvrc_iv_extract_u16_mrk_g32k(&ret_val, GVRC_VR16_FLAGS, m_curr);
		char *mval = ((ret_val & (u16)m_for_out) == 0  ? "false" : "true");
		gsi_log("%s: col = %d, val = %s", name, io, mval);
	}
}

void print_m_output_clean(const char *name, enum gvml_mrks_n_flgs m_for_out, u16 num_out)
{
	print_m_output_range(name, m_for_out, 0, num_out);
}

void print_vr_output_range(const char *name, enum gvrc_vr16 vr_for_out, u16 out_start, u16 num_out)
{
//	enum gvrc_vr16 vr_col_idx = GVRC_VR16_M1;
	enum gvml_mrks_n_flgs m_curr = (enum gvml_mrks_n_flgs)(1 << PRINT_FLAG);
	//gvrc_calc_index_m1_16(vr_col_idx);
	gvrc_m_reset(m_curr);
	u16 ret_val = 93;
	for (u16 io = out_start; io < out_start + num_out; io++) {
		gvrc_eq_imm_16(m_curr, GVRC_VR16_M1, (u16)io);
		gvrc_iv_extract_u16_mrk_g32k(&ret_val, vr_for_out, m_curr);
		gsi_log("%s: col = %d, val = %hu", name, io, ret_val);
	}
}

void extract_vr(u16 * ret_buf, enum gvrc_vr16 vr_valid_mrks)
{
	enum gvml_mrks_n_flgs m_curr = (enum gvml_mrks_n_flgs)(1 << PRINT_FLAG);
	//gvrc_calc_index_m1_16(vr_col_idx);
	gvrc_m_reset(m_curr);
	u16 ret_val = 93;
	for (u16 io = 0; io < NUM_HBS * HB_NUM_COLS; io++) {
		gvrc_eq_imm_16(m_curr, GVRC_VR16_M1, (u16)io);
		gvrc_iv_extract_u16_mrk_g32k(&ret_val, vr_valid_mrks, m_curr);
		ret_buf[io] = ret_val;
	}
}

void print_vr_output_clean(const char *name, enum gvrc_vr16 vr_for_out, u16 num_out)
{
	print_vr_output_range(name, vr_for_out, 0, num_out);
}

static int power(int base, int exp)
{
    int result = 1;
    while(exp) { result *= base; exp--; }
    return result;
}

void print_vr_output_spread5(	const char *name, enum gvrc_vr16 vr0, enum gvrc_vr16 vr1,
								enum gvrc_vr16 vr2, enum gvrc_vr16 vr3, enum gvrc_vr16 vr4,
								u16 out_start, u16 num_out)
{
//	enum gvrc_vr16 vr_col_idx = GVRC_VR16_M1;
	enum gvml_mrks_n_flgs m_curr = (enum gvml_mrks_n_flgs)(1 << PRINT_FLAG);
	//gvrc_calc_index_m1_16(vr_col_idx);
	gvrc_m_reset(m_curr);
	u16 ret_vals[5];
	u16 cross_vals[16]; // 16 as in u16
	for (u16 io = out_start; io < out_start + num_out; io++) {
		gvrc_eq_imm_16(m_curr, GVRC_VR16_M1, (u16)io);
		gvrc_iv_extract_u16_mrk_g32k(&(ret_vals[0]), vr0, m_curr);
		gvrc_iv_extract_u16_mrk_g32k(&(ret_vals[1]), vr1, m_curr);
		gvrc_iv_extract_u16_mrk_g32k(&(ret_vals[2]), vr2, m_curr);
		gvrc_iv_extract_u16_mrk_g32k(&(ret_vals[3]), vr3, m_curr);
		gvrc_iv_extract_u16_mrk_g32k(&(ret_vals[4]), vr4, m_curr);
		for (int i = 0; i < 16; i++) {
			int val = 0;
			for (int j=0; j<5; j++) {
				u16 bval = (((ret_vals[j] & (1 << i)) == 0) ? 0 : 1);
				val += (bval * power(2, j));
			}
			cross_vals[i] = (u16)val;
		}
		gsi_log("%s: col = %d, vals = %hu, %hu, %hu, %hu, %hu, %hu, %hu, %hu, %hu, %hu, %hu, %hu, %hu, %hu, %hu, %hu",
				name, io, cross_vals[0], cross_vals[1], cross_vals[2], cross_vals[3], cross_vals[4], cross_vals[5],
				cross_vals[6], cross_vals[7], cross_vals[8], cross_vals[9], cross_vals[10], cross_vals[11],
				cross_vals[12], cross_vals[13], cross_vals[14], cross_vals[15]);
	}
}



typedef gsi_prod_status_t (*fnp)(void * inp, void * outp);

typedef struct SFuncNameAndPtr {
	char *name;
	fnp dev_fn_ptr;
} tSFuncData;

static gsi_prod_status_t apu_ilp_get_fns(void * inp, void * outp);
static gsi_prod_status_t etest_tests(void * inp, void * outp);
static gsi_prod_status_t apu_ilp_test_fns(void * inp, void * outp);
static gsi_prod_status_t apu_init(void * inp, void * outp);
static gsi_prod_status_t apu_test_data_path(void * inp, void * outp);
static gsi_prod_status_t apu_ilp_init_db(void * inp, void * outp);
static gsi_prod_status_t apu_ilp_find_seq(void * inp, void * outp);
static gsi_prod_status_t apu_remote(void * inp, void * outp);
static gsi_prod_status_t apu_get_dump_data(void * inp, void * outp);

tSFuncData g_dev_func_tbl[] = {
		{.name = "ilp_get_funcs", .dev_fn_ptr = apu_ilp_get_fns},
		{.name = "etest_tests", .dev_fn_ptr = etest_tests},
		{.name = "apu_ilp_test_fns", .dev_fn_ptr = apu_ilp_test_fns},
		{.name = "apu_init", .dev_fn_ptr = apu_init},
		{.name = "apu_test_data_path", .dev_fn_ptr = apu_test_data_path},
		{.name = "ilp_db_init", .dev_fn_ptr = apu_ilp_init_db},
		{.name = "ilp_find_seq", .dev_fn_ptr = apu_ilp_find_seq},
		{.name = "apu_remote", .dev_fn_ptr = apu_remote},
		{.name = "apu_get_dump_data", .dev_fn_ptr = apu_get_dump_data}
};


//char * g_func_tbl[] = {"ilp_get_funcs", "ilp_db_init", "ilp_find_close", "ilp_find_dist",
//					"ilp_find_seq", "ilp_find_match"};



GAL_TASK_ENTRY_POINT(apu_ilp_get_fns, in, out)
{
	gsi_prod_status_t ret = SUCCESS;
	unsigned char * in_start = (unsigned char *)in;
	unsigned char * out_start = (unsigned char *)out;

	tSBufHdr * phdr = (tSBufHdr *)(in_start + sizeof(tSRemoteHdr));
	unsigned char * pout = (unsigned char *)(out_start + sizeof(unsigned int));
	gsi_log("dev side apu_ilp_get_fns called.\n");
	if (phdr->num_params != 3) {
		ret = FAILURE;
		return ret;
	}
	phdr->num_params = 4;
	u16 max_num_funcs = (u16)phdr->params[t_get_fns_max_num_funcs];
	u16 max_name_len = (u16)phdr->params[t_get_fns_max_name_len];
	unsigned int num_funcs = (unsigned int)(sizeof(g_dev_func_tbl) / sizeof(*g_dev_func_tbl));
	gsi_log("%d functions found. len for each %hu", num_funcs, max_name_len);
	if (num_funcs >= max_num_funcs) {
		ret = FAILURE;
		return ret;
	}
	char * buff = (char *)(pout + sizeof(unsigned int));
	for (u16 fn = 0; fn < num_funcs; ++fn) {
		gsi_log("fn found: num %hu, name %s", fn, g_dev_func_tbl[fn].name);
		strncpy(buff + (max_name_len * fn), g_dev_func_tbl[fn].name, max_name_len-1);
	}
	phdr->params[t_get_fns_num_funcs] = num_funcs;
	*(unsigned int *)out_start = (unsigned int)((2 * sizeof(unsigned int)) + (num_funcs * max_name_len));
	*(unsigned int *)pout = num_funcs;
	return ret;
}

GAL_TASK_ENTRY_POINT(apu_get_dump_data, in, out)
{
	gsi_prod_status_t ret = SUCCESS;
	unsigned char * in_start = (unsigned char *)in;
	unsigned char * out_start = (unsigned char *)out;

	tSBufHdr * phdr = (tSBufHdr *)(in_start + sizeof(tSRemoteHdr));
	unsigned char * p_dump_buf = (unsigned char * )gal_dynamic_mem_handle_to_apu_ptr(((tSRemoteHdr *)in_start)->dump_buf_handle);
	unsigned char * pout = (unsigned char *)(out_start + sizeof(unsigned int));
	u16 * pdata = (u16 *)pout;
	gsi_log("dev side apu_get_dump_data called.\n");
	if (phdr->num_params != 4) {
		gsi_log("Error! apu_get_dump_data expects 4 params but received %hu.\n", phdr->num_params);
		ret = FAILURE;
		return ret;
	}
	unsigned int ivr = phdr->params[t_get_dump_data_ivr];
	unsigned int col_start = phdr->params[t_get_dump_data_start];
	unsigned int num_cols = phdr->params[t_get_dump_data_num_cols];
	gsi_log("apu_get_dump_data: ivr: %u, col_start: %u, num_cols %u", ivr, col_start, num_cols);

	u16 * pvr = (u16 *)(p_dump_buf + (22 * L2_MEMORY_SIZE * sizeof(u16)));
	gsi_log("apu_get_dump_data dumping ivr 22");
	for (int i = 0; i < 256; i++) {
		gsi_log("%d: 0x%hx", i, *(pvr+i));
	}

	for (int iapc = 0; iapc < NUM_APUCS; iapc++) {
		u16 * pvr = (u16 *)(p_dump_buf + (ivr * L2_MEMORY_SIZE * sizeof(u16))
							+ (iapc * APUC_NUM_BANKS * BANK_NUM_COLS * sizeof(u16)));
		for (int ibank = 0; ibank < APUC_NUM_BANKS; ibank++) {
			for (int icol = 0; icol < BANK_NUM_COLS; icol++) {
				int irec = (iapc * APUC_NUM_BANKS * BANK_NUM_COLS) + (ibank * BANK_NUM_COLS) + icol;
				if (irec >= col_start && irec < (col_start + num_cols)) {
					u16 data = pvr[(icol * APUC_NUM_BANKS) + ibank];
					//gsi_log("Sending %d: iapc %d, ibank %d, icol %d data: 0x%hx", irec, iapc, ibank, icol, data);
					*(pdata++) = data;
				}
			}
		}
	}
	*(unsigned int *)out_start = (unsigned int)(sizeof(unsigned int) + (num_cols * sizeof(u16)));
	ret = SUCCESS;
	return ret;
}


GAL_TASK_ENTRY_POINT(etest_tests, in, out)
{
	gsi_prod_status_t ret = SUCCESS;
	gsi_prod_status_t testStatus[3];
	gsi_info("\nRunning %s", ((struct read_rl_test_in_out *)out)->buffer);

	//gvml_init_once();
	etest_test_apl_init();

	testStatus[0] = set_get_rl();
	CHECK_RET("set_get_rl", testStatus[0]);

	testStatus[1] = set_get_gl();
	CHECK_RET("set_get_gl", testStatus[1]);

	testStatus[2] = set_get_ggl();
	CHECK_RET("set_get_ggl", testStatus[2]);

	for (u16 i = 0; i < 3; i++) {
		if (testStatus[i] == FAILURE) {
			ret = FAILURE;
		}
	}

	return ret;
}


GAL_TASK_ENTRY_POINT(apu_ilp_test_fns, in, out)
{
	gsi_prod_status_t ret = SUCCESS;
	tSBufHdr * phdr = (tSBufHdr *)out;
	gsi_log("dev side apu_ilp_test_fns called with %hu params.\n", phdr->num_params);
	if (phdr->num_params != 0) {
		ret = FAILURE;
		return ret;
	}
	etest_test_apl_init();
	enum gvrc_vr16 vra = GVRC_VR16_0;
	enum gvrc_vr16 vrb = GVRC_VR16_1;
	enum gvrc_vr16 vrc = GVRC_VR16_2;
	enum gvrc_vr16 vrd = GVRC_VR16_3;
	enum gvrc_vr16 vre = GVRC_VR16_4;
	enum gvrc_vr16 vrt1 = GVRC_VR16_9;
	enum gvrc_vr16 vrt2 = GVRC_VR16_10;
	//enum gvrc_vr16 vr_m1_index = GVML_M4_IDX;
	gvrc_cpy_imm_16(vra, (u16)0xAAAA);
	gvrc_cpy_imm_16(vrb, (u16)0xCCCC);
	gvrc_cpy_imm_16(vrc, (u16)0xF0F0);


//	print_vr_output_clean("sum", vra, 4);
//	print_vr_output_clean("carry", vrb, 4);
	gvrc_3to2(vra, vrb, vrc, vrt1, vrt2);
	print_vr_output_clean("3sum", vra, 4);
	print_vr_output_clean("3carry", vrc, 4);
	print_vr_output_clean("aANDb", vrt1, 4);
	print_vr_output_clean("aXORb", vrt2, 4);
	gvrc_cpy_imm_16(vrd, (u16)0xFF00);
	//gvrc_cpy_16(vrb, vrd);
	gvrc_2to2(vra, vrd, vrt1); // input: sum of 1st op, new val. output: a is the lsb, d is the carry
	gvrc_2to2(vrc, vrd, vrt1); // carry of second op, carry of last sum. output: d is the msb, c is the middle bit
	print_vr_output_clean("msb", vrd, 4);
	print_vr_output_clean("next", vrc, 4);
	print_vr_output_clean("lsb", vra, 4);

	gvrc_asl1(vrt1, vra);
	gvrc_cpy_16_msk(vrb, vrt1, 0xAAAA);
	gvrc_2to2(vra, vrb, vrt1); // input: shifted bits of a, unshifted of a. output: a is the lsb, b is the carry
	gvrc_asl1(vrt1, vrc);
	gvrc_cpy_16_msk(vre, vrt1, 0xAAAA);
	gvrc_3to2(vrc, vre, vrb, vrt1, vrt2); // input: shifted bits of c, unshifted of c. b, the cin. output: c is the middle bit, b is the carry
	gvrc_asl1(vrt1, vrd);
	gvrc_cpy_16_msk(vre, vrt1, 0xAAAA);
	gvrc_3to2(vrd, vre, vrb, vrt1, vrt2); // input: shifted bits of d, unshifted of d. b, the cin. output: d is the 3d bit, b is the msb
	print_vr_output_clean("msb", vrb, 4);
	print_vr_output_clean("3rd", vrd, 4);
	print_vr_output_clean("2nd", vrc, 4);
	print_vr_output_clean("lsb", vra, 4);
	for (int ival = 0; ival < 8; ival++) {
		gvrc_cpy_imm_16(vre, 0);
		gvrc_spread4(	vre, vra, vrc, vrd, vrb, (u16)(1 << ((ival * 2)+1)));
		gsi_log("val %d", ival);
		print_vr_output_clean("spreaded", vre, 4);
	}

	return SUCCESS;
}

GAL_TASK_ENTRY_POINT(apu_init, in, out)
{
	gsi_prod_status_t ret = SUCCESS;
	unsigned char * in_start = (unsigned char *)in;
	unsigned char * out_start = (unsigned char *)out;
//	unsigned char * p_dump_buf = (unsigned char * )gal_dynamic_mem_handle_to_apu_ptr(((tSRemoteHdr *)in_start)->dump_buf_handle);

	tSBufHdr * phdr = (tSBufHdr *)(in_start + sizeof(tSRemoteHdr));
//	unsigned char * pout = (unsigned char *)(out_start + sizeof(unsigned int));
	gsi_log("dev side of apu_init called.\n");
	if (phdr->num_params != 1) {
		gsi_log("Error! apu_init expects 1 params but received %hu.\n", phdr->num_params);
		ret = FAILURE;
		return ret;
	}
	if (phdr->num_bufs != 2) {
		gsi_log("Error! apu_init expects 2 buffers but received %hu.\n", phdr->num_bufs);
		ret = FAILURE;
		return ret;
	}
	*(unsigned int *)out_start = (unsigned int)(sizeof(unsigned int));

	etest_test_apl_init();
	enum gvml_vm_reg vm_reg_m1_index = GVML_VM_0;
	enum gvrc_vr16 vr_m1_index = GVRC_VR16_M1;

	gsi_info("apu_test_data_path buf offsets: %u and %u. \n", phdr->buf_offsets[0], phdr->buf_offsets[1]);
	u16 * in_buf_1 = (u16 *)((char *)phdr + phdr->buf_offsets[0]);
	u16 * in_buf_2 = (u16 *)((char *)phdr + phdr->buf_offsets[1]);
	gsi_info("value for %d and %d, %hu, %hu", (4068 * 4)+3, (4069 * 4)+0, in_buf_2[(4068 * 4)+3], in_buf_2[(4069 * 4)+0]);
	gsi_info("addr for in_start %p, bad data %p", in_start, &(in_buf_2[(4068 * 4)+3]));

	static gal_l2dma_hndl_t *ioxs[GSI_APUC_NUM_APCS];
	struct gal_l2dma_l4_l2_transaction transaction;
	transaction.l2_addr.l2_byte_row = 0;
	transaction.l2_addr.l2_col_group = 0;
	transaction.l2_mode = GAL_L2T_MODE_64;
	transaction.l2_rep_colgrp_stride = 0;
	transaction.l2_step_col_group = 0;
	transaction.l4_addr = (u8 *)(in_buf_1);
	transaction.l4_step_stride = 0;
	transaction.num_steps = 1;
	transaction.step_size = L2_MEMORY_SIZE;

	gsi_info("before gal_l2dma_mem_to_l2_start");
	ioxs[0] = gal_l2dma_mem_to_l2_start(0, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
	gal_l2dma_sync(ioxs[0], true);

	transaction.l4_addr = (u8 *)(in_buf_2);
	ioxs[1] = gal_l2dma_mem_to_l2_start(1, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
	gal_l2dma_sync(ioxs[1], true);

	gal_l2dma_l2_ready_rst_all();
	for (int ibank = 0; ibank < GSI_MMB_NUM_BANKS; ibank++) {
		u8 bank_id = (u8)ibank;
		uint l1_grp = 0;
		int num_vert_bytes = 2;
		bool l2_ready_set = (ibank == (GSI_MMB_NUM_BANKS-1));
		u8 l2_start_byte = (u8)(ibank * 2);

		gvrc_copy_N_bytes_l2_to_l1(bank_id, vm_reg_m1_index, l1_grp, num_vert_bytes, l2_ready_set, l2_start_byte);
	}

	gvrc_io_load_16(vr_m1_index, vm_reg_m1_index);

	// init the mgal l2dma l4 to l3 and back code
	mgal_l2dma_async_memcpy_init(0);
	mgal_l2dma_async_memcpy_init(1);

	//dump_all_mmb(p_dump_buf, GVML_VM_0);

	ret = SUCCESS;
	return ret;
}

static u16 * gvrc_alloc_l3_buf(int size)
{
	unsigned long l3buf_na = (unsigned long)gal_malloc(size + (CACHE_LINE_SIZE));
	u16 * l3buf = (u16 * )(l3buf_na - (l3buf_na % CACHE_LINE_SIZE) );
	gsi_log("Allocated 0x%lx\n", (long)l3buf);
	memset(l3buf, 0, size);
	return l3buf;
}

static int gvrc_copy_l4_to_l3_start(void * src, int size)
{
	static gal_l2dma_hndl_t *iox;
	struct gal_l2dma_l4_l2_transaction transaction;
	transaction.l2_addr.l2_byte_row = 0;
	transaction.l2_addr.l2_col_group = 0;
	transaction.l2_mode = GAL_L2T_MODE_64;
	transaction.l2_rep_colgrp_stride = 0;
	transaction.l2_step_col_group = 0;
	transaction.l4_addr = (u8 *)(src);
	transaction.l4_step_stride = 0;
	transaction.num_steps = 1;
	transaction.step_size = (uint)size;

	gsi_info("before gal_l2dma_mem_to_l2_start");
	iox = gal_l2dma_mem_to_l2_start(0, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
	return (int)iox;
}

static int gvrc_copy_l4_to_l3_next(void * dst, int size, int prev_iiox)
{
	gal_l2dma_hndl_t *prev_iox = (gal_l2dma_hndl_t *)prev_iiox;
	gal_l2dma_sync(prev_iox, true);

	static gal_l2dma_hndl_t *iox;
	struct gal_l2dma_l4_l2_transaction transaction;
	transaction.l2_addr.l2_byte_row = 0;
	transaction.l2_addr.l2_col_group = 0;
	transaction.l2_mode = GAL_L2T_MODE_64;
	transaction.l2_rep_colgrp_stride = 0;
	transaction.l2_step_col_group = 0;
	transaction.l4_addr = (u8 *)(dst);
	transaction.l4_step_stride = 0;
	transaction.num_steps = 1;
	transaction.step_size = (uint)size;

	iox = gal_l2dma_l2_to_mem_start(0, 1, &transaction, GAL_L2DMA_L2_READY_NOP);
	gal_l2dma_sync(iox, true);
//	for (int ib = 0; ib < 20 ; ib++) {
//		gsi_log("%hu, ", dst[ib]);
//	}
	return (int)iox;
}

//function name :  l2dma_mem_to_mem_512.
//description : copy 512 bytes from L4 into L3 via l2t.
//param : u32 dst - l2dma destination address.
//param : u32 src - l2dma source address.
//
void l2dma_mem_to_mem_512(u32 dst, u32 src);

//function name :  l2dma_set_ready_cmd.
//description :
//param : u8 apc_id - apc select  0/1 .
//
void l2dma_set_ready_cmd(u8 apc_id);

static void * gvrc_copy_l4_to_l3(void * src, int chunk_size, uint32_t *non_align_l3_buff)
{
	//allocate l3 buffer, L4 was already allocated at host.

	uint8_t *curr_l4_ptr =   (uint8_t *)(unsigned long)src;

//	uint32_t *non_align_l3_buff;

	//chunk_size = 1024; //L3 size example

	//allocate and clear L3 buffer:
//	non_align_l3_buff = gal_malloc(chunk_size + (CACHE_LINE_SIZE));
//	memset(non_align_l3_buff, 0, (chunk_size + (CACHE_LINE_SIZE)));


	//clear cache
#ifndef APUC_TYPE_sw_sim

	//garc_cache_dcache_flush_mlines((u32)non_align_l3_buff, chunk_size + (CACHE_LINE_SIZE));
	//garc_cache_dcache_invalidate_mlines((u32)non_align_l3_buff, chunk_size + (CACHE_LINE_SIZE));

	gal_cache_dcache_flush_mlines((u32)non_align_l3_buff, chunk_size + (CACHE_LINE_SIZE));
	gal_cache_dcache_invalidate_mlines((u32)non_align_l3_buff, chunk_size + (CACHE_LINE_SIZE));


#endif

	uint32_t *l3_buff = GSI_CAST_32_PTR((GSI_CAST_32  non_align_l3_buff)
										+ CACHE_LINE_SIZE - (GSI_CAST_32 non_align_l3_buff % CACHE_LINE_SIZE));

	//prepare dma for work
	l2dma_set_ready_cmd(0);

	//first run read block into L3
	uint32_t number_of_dma_cycles_per_chunks = chunk_size / CHUNK_SIZE_512;
	u32 number_of_dma_cycles_per_chunks_counter = number_of_dma_cycles_per_chunks;
	u32 l3_buff_512;
	u32 curr_l4_ptr_512;

	//setup pointers for copy.
	l3_buff_512 = GSI_CAST_32  l3_buff;
	curr_l4_ptr_512 = GSI_CAST_32 curr_l4_ptr;

	while (number_of_dma_cycles_per_chunks_counter) {
		l2dma_mem_to_mem_512(l3_buff_512, curr_l4_ptr_512);
		gsi_log("Copied 512 bytes");

		//increment pointer by 512 steps.
		l3_buff_512 += CHUNK_SIZE_512;
		curr_l4_ptr_512 += CHUNK_SIZE_512;
		number_of_dma_cycles_per_chunks_counter--;
	}

	//return (void *)non_align_l3_buff;
	return (void *)l3_buff;

}

static void gvrc_copy_sync(int iiox) {
	gal_l2dma_hndl_t *prev_iox = (gal_l2dma_hndl_t *)iiox;
	gal_l2dma_sync(prev_iox, true);
}

static int l4_to_l3_trans_copy(u16 * l3dst, u16 * l4src) {
	static gal_l2dma_hndl_t *ioxs[GSI_APUC_NUM_APCS];
	struct gal_l2dma_l4_l2_transaction transaction;
	transaction.l2_addr.l2_byte_row = 0;
	transaction.l2_addr.l2_col_group = 0;
	transaction.l2_mode = GAL_L2T_MODE_64;
	transaction.l2_rep_colgrp_stride = 0;
	transaction.l2_step_col_group = 0;
	transaction.l4_addr = (u8 *)(l4src);
	transaction.l4_step_stride = 0;
	transaction.num_steps = 1;
	transaction.step_size = L2_MEMORY_SIZE;

	gsi_info("about to copy l4 to l2");
	ioxs[0] = gal_l2dma_mem_to_l2_start(0, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
	gal_l2dma_sync(ioxs[0], true);

	transaction.l4_addr = (u8 *)(l3dst);
	ioxs[0] = gal_l2dma_l2_to_mem_start(0, 1, &transaction, GAL_L2DMA_L2_READY_NOP);
	gal_l2dma_sync(ioxs[0], true);

	return 0;
}

GAL_TASK_ENTRY_POINT(apu_test_data_path, in, out)
{
	gsi_prod_status_t ret = SUCCESS;
	tSBufHdr * phdr = (tSBufHdr *)out;
	gsi_log("dev side of apu_test_data_path called.\n");
	if (phdr->num_params != 0) {
		gsi_log("Error! apu_test_data_path expects 0 params but received only %hu.\n", phdr->num_params);
		ret = FAILURE;
		return ret;
	}
	if (phdr->num_bufs != 4) {
		gsi_log("Error! apu_test_data_path expects 4 buffers but received %hu.\n", phdr->num_bufs);
		ret = FAILURE;
		return ret;
	}

	etest_test_apl_init();
	enum gvrc_vr16 vr_test1 = GVRC_VR16_0;
	enum gvrc_vr16 vr_test2 = GVRC_VR16_1;
	enum gvml_vm_reg vm_reg_test = GVML_VM_0;
	gvrc_reset_16(vr_test1);
	gvrc_reset_16(vr_test2);
	//print_vr_output_clean("test print",vr_test2,64);

	gsi_info("apu_test_data_path buf offsets: %u and %u. \n", phdr->buf_offsets[0], phdr->buf_offsets[1]);
	u16 * in_buf_1 = (u16 *)((char *)phdr + phdr->buf_offsets[0]);
	u16 * in_buf_2 = (u16 *)((char *)phdr + phdr->buf_offsets[1]);
	u16 * ret_buf_1 = (u16 *)((char *)phdr + phdr->buf_offsets[2]);
	u16 * ret_buf_2 = (u16 *)((char *)phdr + phdr->buf_offsets[3]);

	int buf_size = phdr->buf_offsets[1] - phdr->buf_offsets[0];
	int buf_muls = buf_size / 512;
	gsi_log("Will copy %d bufs.", buf_muls);
	u16 * l3mbuf1 = mgal_malloc_cache_aligned(512);
	u16 * l3mbuf2 = mgal_malloc_cache_aligned(512);
	mgal_l2dma_async_memcpy_init(0);
	mgal_l2dma_async_memcpy_init(1);
	bool bflip = true;
	for (int itest = 0; itest < 2; itest++) {
		u8* pin_buf = (u8*)in_buf_1;
		u8* pret_buf = (u8*)ret_buf_1;
		if (itest == 1) {
			pin_buf = (u8*)in_buf_2;
			pret_buf = (u8*)ret_buf_2;
		}
		mgal_l2dma_async_memcpy((u8*)l3mbuf1, pin_buf, 512, 0, true, true, true);
		mgal_l2dma_async_memcpy((u8*)l3mbuf2, pin_buf + 512, 512, 0, true, true, true);
		for (int ibuf=0; ibuf < buf_muls; ibuf++, bflip = !bflip) {
			u8 * p1 = (u8*)(bflip ? l3mbuf1 : l3mbuf2);
			//u8 * p2 = (u8*)(bflip ? l3mbuf2 : l3mbuf1);
			mgal_l2dma_async_memcpy(pret_buf + (512 * ibuf), p1, 512, 1, true, true, true);
			if (ibuf < (buf_muls-2))
				mgal_l2dma_async_memcpy(p1, pin_buf + (512 * (ibuf+2)), 512, 0, true, true, true);
			;
		}
		mgal_l2dma_async_memcpy_end(0);
		mgal_l2dma_async_memcpy_end(1);
	}
//	bool bpassed = true;
//	for (int i = 0; i < 256; i++) {
//		if (l3mbuf1[i] != in_buf_1[i]) {
//			gsi_log("Error! Bytes at loc %d not equal", i);
//			bpassed = false;
//		}
//	}
//	if (bpassed) {
//		gsi_log("Simple transfer succeeded.\n");
//	}
	mgal_free_cache_aligned(l3mbuf1);
	mgal_free_cache_aligned(l3mbuf2);
	return 0;


	unsigned long l3buf_na = (unsigned long)gal_malloc(L2_MEMORY_SIZE + (CACHE_LINE_SIZE));
	u16 * l3buf = (u16 * )(l3buf_na + CACHE_LINE_SIZE - (l3buf_na % CACHE_LINE_SIZE) );
	gsi_log("Allocated 0x%lx\n", (long)l3buf);
	memset(l3buf, 0, L2_MEMORY_SIZE);
/*
	l4_to_l3_trans_copy(l3buf, in_buf_1);
	memcpy(ret_buf_1, l3buf, L2_MEMORY_SIZE);
	l4_to_l3_trans_copy(l3buf, in_buf_2);
	memcpy(ret_buf_2, l3buf, L2_MEMORY_SIZE);
	ret = SUCCESS;
	return ret;
*/

/*
	int block_size = (1 << 12);
	uint32_t *non_align_l3_buff = gal_malloc(block_size + (CACHE_LINE_SIZE));
	memset(non_align_l3_buff, 0, (block_size + (CACHE_LINE_SIZE)));

	u16 * l3buft = (u16 *)gvrc_copy_l4_to_l3(in_buf_1, block_size, non_align_l3_buff);
	for (int ib = 0; ib < 200 ; ib++) {
		gsi_log("%hu vs %hu, ", in_buf_1[ib], l3buft[ib]);
	}
	gal_free(non_align_l3_buff);
	return 0;
*/
//	for (int i = 0; i < L2_MEMORY_SIZE * (int)(sizeof(u64) / sizeof(u16)); i++) {
//		ret_buf[i] = in_buf[i];
//	}
	static gal_l2dma_hndl_t *ioxs[GSI_APUC_NUM_APCS];
	struct gal_l2dma_l4_l2_transaction transaction;
	transaction.l2_addr.l2_byte_row = 0;
	transaction.l2_addr.l2_col_group = 0;
	transaction.l2_mode = GAL_L2T_MODE_64;
	transaction.l2_rep_colgrp_stride = 0;
	transaction.l2_step_col_group = 0;
	transaction.l4_addr = (u8 *)(in_buf_1);
	transaction.l4_step_stride = 0;
	transaction.num_steps = 1;
	transaction.step_size = L2_MEMORY_SIZE;

	gsi_info("before gal_l2dma_mem_to_l2_start");
	ioxs[0] = gal_l2dma_mem_to_l2_start(0, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
	gal_l2dma_sync(ioxs[0], true);

	/*
	transaction.l4_addr = (u8 *)(in_buf_2);
	ioxs[1] = gal_l2dma_mem_to_l2_start(1, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
	gal_l2dma_sync(ioxs[1], true);
	*/

	/*
	gal_l2dma_l2_ready_rst_all();
	for (int ibank = 0; ibank < GSI_MMB_NUM_BANKS; ibank++) {
		u8 bank_id = (u8)ibank;
		uint l1_grp = 0;
		int num_vert_bytes = 2;
		bool l2_ready_set = (ibank == (GSI_MMB_NUM_BANKS-1));
		u8 l2_start_byte = 0; // (u8)(ibank * 2);

		gvrc_copy_N_bytes_l2_to_l1(bank_id, vm_reg_test, l1_grp, num_vert_bytes, l2_ready_set, l2_start_byte);
	}
	*/

	// load of l1 from below instead
	/*
	gvrc_cpy_16(vr_test1, VR_RUNNING_IDX);
	gvrc_io_store_16(vm_reg_test, vr_test1);
	gvrc_reset_16(vr_test1);
	gvrc_copy_N_bytes_l1_to_l2(bank_id, vm_reg_test, l1_grp, num_vert_bytes, l2_ready_set, l2_start_byte);
	*/

	//handling allocation failure.
//	if ((long)l3buf < 0) {
//		gsi_log("gsi_apuc_malloc - failed allocate L3 memory for l3buf");
//		ret = FAILURE;
//		return ret;
//	}


	unsigned long l3buf2_na = (unsigned long)gal_malloc(L2_MEMORY_SIZE + (CACHE_LINE_SIZE));
	u16 * l3buf2 = (u16 * )(l3buf2_na + CACHE_LINE_SIZE- (l3buf2_na % CACHE_LINE_SIZE) );
	gsi_log("Allocated 0x%lx\n", (long)l3buf2);
	//memset(l3buf2, 0, L2_MEMORY_SIZE);

	transaction.l4_addr = (u8 *)(l3buf);
	ioxs[0] = gal_l2dma_l2_to_mem_start(0, 1, &transaction, GAL_L2DMA_L2_READY_SET);
	gal_l2dma_sync(ioxs[0], true);
	for (int ib = 0; ib < 20 ; ib++) {
		gsi_log("%hu, ", l3buf[ib]);
	}
	/*
	transaction.l4_addr = (u8 *)(l3buf2);
	ioxs[1] = gal_l2dma_l2_to_mem_start(1, 1, &transaction, GAL_L2DMA_L2_READY_NOP);
	gal_l2dma_sync(ioxs[1], true);
	*/
	memcpy(ret_buf_1, l3buf, L2_MEMORY_SIZE);
	for (int ib = 0; ib < 20 ; ib++) {
		gsi_log("%hu, ", in_buf_2[ib]);
	}
	memcpy(ret_buf_2, in_buf_2, L2_MEMORY_SIZE);
	ret = SUCCESS;
	return ret;



	gvrc_io_load_16(vr_test2, vm_reg_test);

	enum gvml_mrks_n_flgs m_curr = (enum gvml_mrks_n_flgs)(1 << GP4_FLAG);
	gvrc_m_reset(m_curr);
	u16 ret_val = 93;
	for (int iapc=0; iapc < 2; iapc++) {
		u16 * pbuf = ret_buf_1;
		if (iapc == 1) {
			pbuf = ret_buf_2;
		}
		for (int icol = 0; icol < GSI_APC_NUM_COLS; icol++) {
			for (int ibank = 0; ibank < GSI_MMB_NUM_BANKS; ibank++) {
	//			gvrc_calc_index_m1_16(vr_col_idx);
				gvrc_eq_imm_16(m_curr, VR_RUNNING_IDX, (u16)(icol | (ibank << 12) | (iapc << 14)));
				gvrc_iv_extract_u16_mrk_g32k(&ret_val, vr_test2, m_curr);
				pbuf[(icol * GSI_MMB_NUM_BANKS) + ibank] = ret_val;
			}
		}
	}

	gvrc_io_load_16(vr_test2, vm_reg_test);
	print_vr_output_range("test print",vr_test2, 0, 64);
	print_vr_output_range("test print 2",vr_test2, 4064, 64);
	print_vr_output_range("test print 3",vr_test2, 4096 * 4, 64);
	//print_vr_output_range("test print2",vr_test2, 2048, 64);

	/*
	transaction.l4_addr = (u8 *)(ret_buf_1);
	ioxs[0] = gal_l2dma_l2_to_mem_start(0, 1, &transaction, GAL_L2DMA_L2_READY_NOP);
	gal_l2dma_sync(ioxs[0], true);

	transaction.l4_addr = (u8 *)(ret_buf_2);
	ioxs[0] = gal_l2dma_l2_to_mem_start(1, 1, &transaction, GAL_L2DMA_L2_READY_NOP);
	gal_l2dma_sync(ioxs[1], true);
	*/

//	ioxs[1] = gal_l2dma_mem_to_l2_start(APC_B, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
//	gal_l2dma_sync(ioxs[1], true);

	ret = SUCCESS;
	return ret;
}

GAL_TASK_ENTRY_POINT(apu_ilp_init_db, in, out)
{
	gsi_prod_status_t ret = SUCCESS;
	unsigned char * in_start = (unsigned char *)in;
	unsigned char * out_start = (unsigned char *)out;

	tSBufHdr * phdr = (tSBufHdr *)(in_start + sizeof(tSRemoteHdr));
//	unsigned char * pout = (unsigned char *)(out_start + sizeof(unsigned int));
	gsi_log("dev side of apu_init called.\n");
	if (phdr->num_params != 3) {
		gsi_log("Error! apu_ilp_init_db expects 3 params but received %hu.\n", phdr->num_params);
		ret = FAILURE;
		return ret;
	}
	if (phdr->num_bufs != 2) {
		gsi_log("Error! apu_ilp_init_db expects 2 buffers but received %hu.\n", phdr->num_bufs);
		ret = FAILURE;
		return ret;
	}
	*(unsigned int *)out_start = (unsigned int)(sizeof(unsigned int));

	gsi_log("dev side apu_ilp_init_db called.\n");
	int data_len = phdr->params[t_init_db_data_len];
	int numw = phdr->params[t_init_db_numw];
	int src_rows = phdr->params[t_init_db_num_rows];
	u16 * dbuf = (u16 *)((char *)phdr + phdr->buf_offsets[0]);
	u16 * wstarts_buf = (u16 *)((char *)phdr + phdr->buf_offsets[1]);
	if (dbuf == NULL || wstarts_buf == NULL || numw == 0 || data_len == 0 || src_rows == 0) {
		ret = FAILURE;
		return ret;
	}
	g_db_info.num_rows = (u16)src_rows;
	g_db_info.num_cols = (u16)data_len;
	g_db_info.numw = (u16)numw;
	int ihb = 0;
	for (int istart = 0; istart < numw; istart++) {
		if (((int)(wstarts_buf[istart]) / HB_NUM_COLS) > ihb) {
			g_db_info.recs_end_idx[ihb] = wstarts_buf[istart-1];
			//gsi_log("apu_ilp_init_db. Added an end for hb %d at %hu\n", ihb, g_db_info.recs_end_idx[ihb]);
			ihb++;
		}
	}
	g_db_info.recs_end_idx[ihb] = wstarts_buf[numw-1];
	gsi_log("apu_ilp_init_db. Added an end for hb %d at %hu\n", ihb, g_db_info.recs_end_idx[ihb]);
	g_db_info.total_hbs = ihb + 1;
	//etest_test_apl_init();
//	gvml_apl_init();
	enum gvrc_vr16 vr_db_start = GVRC_VR16_0;
//	enum gvml_flgs mrk_start = (enum gvml_flgs)GP0_FLAG;
//	enum gvml_flgs mrk_end = (enum gvml_flgs)GP0_FLAG;
	int iflag = 0;
	bool bFlagsFound = false;
	for (; iflag < cNumFlags; iflag++) {
		if (cFlagAvailTbl[iflag].bavail) {
			g_db_info.mrk_start = cFlagAvailTbl[iflag].flg;
			break;
		}
	}
	for (iflag++; iflag < cNumFlags; iflag++) {
		if (cFlagAvailTbl[iflag].bavail) {
			g_db_info.mrk_end = cFlagAvailTbl[iflag].flg;
			bFlagsFound = true;
			g_db_info.last_iflag_used = iflag;
			break;
		}
	}
	gsi_log("found avail marks at %hu and %hu.", (u16)g_db_info.mrk_start, (u16)g_db_info.mrk_end);
	if (!bFlagsFound) {
		gsi_log("Error! apu_ilp_init_db could not find avail flags.");
		return 1;
	}
	enum gvrc_vr16 vr_widx = vr_db_start + src_rows;
	tSVR_AVAIL avail_info;
	setup_avail_info(&avail_info);
//	avail_info.vrs[GVRC_VR16_12] = egvrcBlocked;
//	avail_info.vrs[GVRC_VR16_13] = egvrcBlocked;
	for (enum gvrc_vr16 e = vr_db_start; e <= vr_widx; e++) {
		avail_info.vrs[e] = egvrcArg;
	}
	enum gvml_mrks_n_flgs m_wstarts = (enum gvml_mrks_n_flgs)(1 << g_db_info.mrk_start); // GVML_MRK0; prefer to use 1 << notation than hide with GVML_MRK0
	enum gvml_mrks_n_flgs m_wends = (enum gvml_mrks_n_flgs)(1 << g_db_info. mrk_end);
	avail_info.mrks[g_db_info.mrk_start] = egvrcArg;
	avail_info.mrks[g_db_info.mrk_end] = egvrcArg;
	init_db(vr_db_start, vr_widx, m_wstarts, m_wends, dbuf, wstarts_buf, (u16)data_len, (u16)src_rows,
			(u16)numw, g_db_info.recs_end_idx, g_db_info.total_hbs, &avail_info);
//	print_vr_output_clean("vr_db", vr_db_start, 64);
//	ret = (u16)m_wstarts;
	return ret;
}

static int ilp_shift_init(	 enum gvrc_vr16 vr_valid_mrks, tSVR_AVAIL * p_parent_avail_info)
{
	tSVR_AVAIL avail_info;
	avail_info = *p_parent_avail_info;
	//avail_info.vrs[vr_valid_mrks] = egvrcArg;
	g_db_info.vr_stored_dbr_idx = vr_valid_mrks + 1;
	g_db_info.vr_stored_qidx = vr_valid_mrks + 2;
	g_db_info.vr_find_thresh_flags = vr_valid_mrks + 3;
	g_db_info.vm_find_thresh_flags = GVML_VM_1;
	g_db_info.vm_stored_dbr_idx = GVML_VM_2;
	g_db_info.vm_stored_qidx = GVML_VM_3;
	g_db_info.vm_for_output = GVML_VM_4;
//	avail_info.vrs[g_db_info.vr_stored_dbr_idx] = egvrcArg;
//	avail_info.vrs[g_db_info.vr_stored_qidx] = egvrcArg;
	// for all fns only block the db and widx. Thresh will mark as
	int iflag = g_db_info.last_iflag_used;
	bool bFlagsFound = false;
	for (iflag++; iflag < cNumFlags; iflag++) {
		if (cFlagAvailTbl[iflag].bavail) {
			g_db_info.mrk_stored = cFlagAvailTbl[iflag].flg;
//			g_db_info.mrk_stored = (enum gvml_mrks_n_flgs)(1 << cFlagAvailTbl[iflag].flg);
//			avail_info.mrks[cFlagAvailTbl[iflag].flg] = egvrcArg;
			bFlagsFound = true;
			break;
		}
	}
	if (!bFlagsFound) {
		gsi_log("Failed to allocate all required marks. Shift not run.\n");
		return 1;
	}
	g_db_info.last_iflag_used = cFlagAvailTbl[iflag].flg;
	gvrc_m_reset((enum gvml_mrks_n_flgs)(1 << g_db_info.mrk_stored));
	gvrc_reset_16(g_db_info.vr_find_thresh_flags);
	gvrc_reset_16(g_db_info.vr_stored_dbr_idx);
	gvrc_reset_16(g_db_info.vr_stored_qidx);
	gvrc_cpy_from_mrk_16_msk(g_db_info.vr_find_thresh_flags, (u16)(1 << g_db_info.mrk_stored), (u16)(1 << 0));
	gvrc_io_store_16(g_db_info.vm_find_thresh_flags, g_db_info.vr_find_thresh_flags);
	gvrc_io_store_16(g_db_info.vm_stored_dbr_idx, g_db_info.vr_stored_dbr_idx);
	gvrc_io_store_16(g_db_info.vm_stored_qidx, g_db_info.vr_stored_qidx);
	gsi_log("ilp_shift_init allocating mrk stored at %d \n", g_db_info.mrk_stored);
	return 0;
}

// moves 64 queries onto the conveyor belt
static int ilp_shift(	unsigned char * ret_idx_buf, int * p_i_out_buf, bool bfirst, bool blast, bool b_store_empty,
						int qnum, enum gvrc_vr16 vr_db_start, enum gvrc_vr16 vr_windex, enum gvrc_vr16 vr_valid_mrks,
						enum gvml_mrks_n_flgs mrk_starts, enum gvml_mrks_n_flgs mrk_ends, u16 *pberr,
						tSVR_AVAIL * p_parent_avail_info)
{
	int ret = 0;
	*pberr = 0;
	if (bfirst) {
		ret = ilp_shift_init(vr_valid_mrks, p_parent_avail_info);
		if (ret) {
			return ret;
		}
	}
	tSVR_AVAIL avail_info;
	avail_info = *p_parent_avail_info;
	// for all fns only block the db and widx. Thresh will mark as
	int iflag = g_db_info.last_iflag_used;
	bool bFlagsFound = false;
	enum gvml_flgs mrk_error;
	for (iflag++; iflag < cNumFlags; iflag++) {
		if (cFlagAvailTbl[iflag].bavail) {
//			mrk_error = (enum gvml_mrks_n_flgs)(1 << cFlagAvailTbl[iflag].flg);
			mrk_error = cFlagAvailTbl[iflag].flg;
			avail_info.mrks[cFlagAvailTbl[iflag].flg] = egvrcArg;
			bFlagsFound = true;
			break;
		}
	}
	if (!bFlagsFound) {
		gsi_log("Failed to allocate all required marks. Shift not run.\n");
		return 1;
	}
//	gsi_log("ilp_shift allocating mrk error at %d \n", (int)mrk_error);
	gvrc_m_reset((enum gvml_mrks_n_flgs)(1 << mrk_error));

	u16 berr = 0;
//	gvrc_iv_test_for_one_true_g32k(&berr, (enum gvml_mrks_n_flgs)(1 << mrk_error));
//	// Check for error
//	// if error and first, thresh set too high
//	if (berr) {
//		gsi_log("ilp_shift: ERROR! Test for gvrc_iv_test_for_one_true_g32k failed. berr = %hu", berr);
//		return 1;
//	}
//	gsi_log("ilp_shift: gvrc_iv_test_for_one_true_g32k succeed after reset. berr = %hu", berr);

	gvrc_io_load_16(g_db_info.vr_find_thresh_flags, g_db_info.vm_find_thresh_flags);
	gvrc_io_load_16(g_db_info.vr_stored_dbr_idx, g_db_info.vm_stored_dbr_idx);
	gvrc_io_load_16(g_db_info.vr_stored_qidx, g_db_info.vm_stored_qidx);

	gvrc_cpy_to_mrk_16_msk((u16)(1 << g_db_info.mrk_stored), g_db_info.vr_find_thresh_flags, (u16)(1 << 0));

	avail_info.vrs[g_db_info.vr_stored_dbr_idx] = egvrcArg;
	avail_info.vrs[g_db_info.vr_stored_qidx] = egvrcArg;
	avail_info.mrks[g_db_info.mrk_stored] = egvrcArg;
	avail_info.mrks[mrk_error] = egvrcArg;

	int qnum_bigbatch = qnum / (1 << 16);
	if (qnum_bigbatch > 0) {
		gsi_log("ERROR: bigbatch processing not implemented yet. You are limited to 65k queries,");
		return 1;
	}
	int qnum_bigbatch_start = qnum % (1 << 16);
	u16 qnum_start = (u16)(qnum_bigbatch_start - 64);
	int qnum_rem = qnum_bigbatch_start % 64;
	if (qnum_rem != 0) {
		qnum_start = (u16)(qnum_bigbatch_start - qnum_rem);
	}
	for (u16 irow = 0; irow < (u16)16; irow++) {
		ilp_1q_shift(	vr_db_start, vr_windex, vr_valid_mrks, irow, qnum_start, mrk_starts, mrk_ends,
						(enum gvml_mrks_n_flgs)(1 << g_db_info.mrk_stored), g_db_info.vr_stored_dbr_idx,
						g_db_info.vr_stored_qidx, (enum gvml_mrks_n_flgs)(1 << mrk_error), &avail_info);
	}

	//print_m_output_range("ilp mrk_error",(enum gvml_mrks_n_flgs)(1 << mrk_error), 0, 32767);
	//u16 berr;
//	gvrc_m_reset((enum gvml_mrks_n_flgs)(1 << mrk_error)); // REMOVE!!!!
	gvrc_iv_test_for_one_true_g32k(&berr, (enum gvml_mrks_n_flgs)(1 << mrk_error));
	// Check for error
	// if error and first, thresh set too high
	if (berr && b_store_empty) {
		gsi_log("ilp_shift: ERROR! Thresh set too high. conflict err on first shift (ever or after send). berr = %hu", berr);
		return 1;
	}
	// If error or last
	// 	find vm to hold transfer
	// 	transfer out to first avail output buffer
	// 	incr output buffer
	// 	clear buffers
	// 	load from l1 again
	//static uint num_ouputs_per_apuc = 4;
#define num_ouputs_per_apuc	4
#define num_cols_per_output (BANK_NUM_COLS / num_ouputs_per_apuc)
	static uint num_bytes_per_output = APUC_NUM_BANKS * sizeof(u16) * num_cols_per_output;
	static uint num_outputs_per_ivr = NUM_APUCS * num_ouputs_per_apuc;
	if (berr || blast) {
//		if (berr) {
//			print_m_output_range("berr large dump", (enum gvml_mrks_n_flgs)(1 << mrk_error), 0, 1 << 15);
//		}
//		gsi_log("ilp_shift: Writing to output buffer. berr = %d. blast = %d, *p_i_out_buf = %d", berr, blast, *p_i_out_buf);
//		print_vr_output_range("qidx dump", g_db_info.vr_stored_qidx, 5240, 24);
//		print_vr_output_range("ilp output",g_db_info.vr_stored_dbr_idx, 0, 64);
		// set up structure first, not because that is the right order but because most of it needs setting up only once
		static gal_l2dma_hndl_t *ioxs[GSI_APUC_NUM_APCS];
		struct gal_l2dma_l4_l2_transaction transaction;
		transaction.l2_addr.l2_byte_row = 0;
		transaction.l2_addr.l2_col_group = 0;
		transaction.l2_mode = GAL_L2T_MODE_64;
		transaction.l2_rep_colgrp_stride = 0;
		transaction.l2_step_col_group = 0;
		transaction.l4_addr = 0; // invalid! Will be set immediately
		transaction.l4_step_stride = 0;
		transaction.num_steps = 1;
		transaction.step_size = num_bytes_per_output; // for APU 1.2 return to L2_MEMORY_SIZE; // * 2 because writing u16 / 2 because only half the mem

		int ivr_stage = (int)GVRC_VR16_FLAGS_BACKUP;
		for (int idata = 0; idata < 2; idata++) {
			if (berr) {
				enum gvml_vm_reg vm = g_db_info.vm_stored_qidx;
				if (idata == 0) {
					vm = g_db_info.vm_stored_dbr_idx;
				}
				gvrc_io_load_16(ivr_stage, vm);
			}
			else {
				enum gvrc_vr16 vr = g_db_info.vr_stored_qidx;
				if (idata == 0) {
					vr = g_db_info.vr_stored_dbr_idx;
				}
				gvrc_cpy_16(ivr_stage, vr);
			}
			for (int ihalf = 0; ihalf < 2; ihalf++) {
				if (ihalf != 0) {
					gvrc_copy_hhb_16(ivr_stage);
				}
				gvrc_io_store_16(g_db_info.vm_for_output, ivr_stage);

				//gal_l2dma_l2_ready_rst_all();

//				gsi_log("ilp_shift: idata %d ihalf %d. sending from l1 to l2", idata, ihalf);

				gal_l2dma_l2_ready_rst_all();
				for (int ibank = 0; ibank < GSI_MMB_NUM_BANKS; ibank++) {
					u8 bank_id = (u8)ibank;
					uint l1_grp = 0;
					int num_vert_bytes = sizeof(u16);
					bool l2_ready_set = (ibank == (GSI_MMB_NUM_BANKS-1));
					u8 l2_start_byte = (u8)(ibank * sizeof(u16)); // 0; //
					gvrc_copy_N_bytes_l1_to_l2(	bank_id, g_db_info.vm_for_output, l1_grp, num_vert_bytes,
												l2_ready_set, l2_start_byte);
				}
				// Repeat for the second APC. Repeat both for the qidx

//				gsi_log("ilp_shift:l1 to l2 complete. Writing to l4");

//				int offset = num_bytes_per_output * ((ivr * num_outputs_per_ivr) + ihalf);
//				transaction.l4_addr = (u8 *)(ret_idx_buf + ((L2_MEMORY_SIZE / 2) * sizeof(u16) * (*p_i_out_buf)));
//				(*p_i_out_buf)++;
//				ioxs[0] = gal_l2dma_l2_to_mem_start(0, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
//				gal_l2dma_sync(ioxs[0], true);

				int offset = num_bytes_per_output * (((*p_i_out_buf) * num_outputs_per_ivr) + ihalf);
//				gsi_log("apuc 0. ihalf %d. i_out_buf %d. offset %d", ihalf, *p_i_out_buf, offset);
				transaction.l2_addr.l2_col_group = 0;
				transaction.l4_addr = (u8 *)(ret_idx_buf + offset);
				ioxs[0] = gal_l2dma_l2_to_mem_start(0, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
				gal_l2dma_sync(ioxs[0], true);

				offset = num_bytes_per_output * (((*p_i_out_buf) * num_outputs_per_ivr) + 2 + ihalf);
//				gsi_log("apuc 0. ihalf %d. 2nd hb i_out_buf %d. offset %d", ihalf, *p_i_out_buf, offset);
				transaction.l2_addr.l2_col_group = 32;
				transaction.l4_addr = (u8 *)(ret_idx_buf + offset);
				ioxs[0] = gal_l2dma_l2_to_mem_start(0, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
				gal_l2dma_sync(ioxs[0], true);

//				transaction.l4_addr = (u8 *)(ret_idx_buf + ((L2_MEMORY_SIZE / 2) * sizeof(u16) * (*p_i_out_buf)));
//				(*p_i_out_buf)++;
//				ioxs[1] = gal_l2dma_l2_to_mem_start(1, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
//				gal_l2dma_sync(ioxs[1], true);

				offset = num_bytes_per_output * (((*p_i_out_buf) * num_outputs_per_ivr) + num_ouputs_per_apuc + ihalf);
//				gsi_log("apuc 1. ihalf %d. i_out_buf %d. offset %d", ihalf, *p_i_out_buf, offset);
				transaction.l2_addr.l2_col_group = 0;
				transaction.l4_addr = (u8 *)(ret_idx_buf + offset);
				ioxs[1] = gal_l2dma_l2_to_mem_start(1, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
				gal_l2dma_sync(ioxs[1], true);

				offset = num_bytes_per_output * (((*p_i_out_buf) * num_outputs_per_ivr) + num_ouputs_per_apuc + 2 + ihalf);
//				gsi_log("apuc 1. ihalf %d. 2nd hb i_out_buf %d. offset %d", ihalf, *p_i_out_buf, offset);
				transaction.l2_addr.l2_col_group = 32;
				transaction.l4_addr = (u8 *)(ret_idx_buf + offset);
				ioxs[1] = gal_l2dma_l2_to_mem_start(1, 1, &transaction,  GAL_L2DMA_L2_READY_SET);
				gal_l2dma_sync(ioxs[1], true);
				//gal_l2dma_l2_ready_rst_all();
			} // end ihalf loop
			(*p_i_out_buf)++;
		} // end idata loop: dbr_idx and then qidx
		gvrc_m_reset((enum gvml_mrks_n_flgs)(1 << g_db_info.mrk_stored));
		gvrc_reset_16(g_db_info.vr_stored_dbr_idx);
		gvrc_reset_16(g_db_info.vr_stored_qidx);
	} // end if berr || blast
	else {
		// only reset the vr_valid_mrks, which was our input if we did not write out the data to l4
		// this means that the data was copied into the stored and is not needed. Otherwise we need to
		// try again with the same vr_valid_mrks with nothing already stored
		gvrc_reset_16(vr_valid_mrks);
	}

	// Every 64 q's
	// 1. do shift on all 16 rows
	// 2. if error,
	// 	a. write staged l2 and clear
	//  	b. repeat 1.
	// 3. write stored to staged

	gvrc_reset_16(g_db_info.vr_find_thresh_flags);
	gvrc_cpy_from_mrk_16_msk(g_db_info.vr_find_thresh_flags, (u16)(1 << g_db_info.mrk_stored), (u16)(1 << 0));
	gvrc_io_store_16(g_db_info.vm_find_thresh_flags, g_db_info.vr_find_thresh_flags);
	gvrc_io_store_16(g_db_info.vm_stored_dbr_idx, g_db_info.vr_stored_dbr_idx);
//	print_vr_output_range("qidx for store to l1", g_db_info.vr_stored_qidx, 5240, 24);
	gvrc_io_store_16(g_db_info.vm_stored_qidx, g_db_info.vr_stored_qidx);

	*pberr = berr;
	return 0;
}

GAL_TASK_ENTRY_POINT(apu_remote, in, out)
{
	gsi_prod_status_t ret = SUCCESS;

	unsigned char * in_start = (unsigned char *)in;
//	unsigned char * out_start = (unsigned char *)out;

	tSBufHdr * phdr = (tSBufHdr *)(in_start + sizeof(tSRemoteHdr));
//	u16 * pout = (u16 *)(out_start + sizeof(unsigned int));
	gsi_log("dev side apu_remote called.\n");
	gsi_log("sizeof(tSRemoteHdr) = %d num params = %d. params[0] = %d\n",
			(int)sizeof(tSRemoteHdr), (int)(phdr->num_params), (int)(phdr->params[0]));

	ret = FAILURE;
	ret = (*(g_dev_func_tbl[phdr->params[0]].dev_fn_ptr))(in, out);

//	u16 ret_data[] = {1, 7, 7, 2};
//	*(unsigned int *)out_start = (unsigned int)(sizeof(unsigned int) + sizeof(ret_data));
//	for (int i = 0; i < 4; i++) {
//		pout[i] = ret_data[i];
//	}
	return ret;
}

GAL_TASK_ENTRY_POINT(apu_ilp_find_seq, in, out)
{
	gsi_prod_status_t ret = SUCCESS;
	unsigned char * in_start = (unsigned char *)in;
	unsigned char * out_start = (unsigned char *)out;

	tSBufHdr * phdr = (tSBufHdr *)(in_start + sizeof(tSRemoteHdr));
//	unsigned char * p_dump_buf = (unsigned char * )gal_dynamic_mem_handle_to_apu_ptr(((tSRemoteHdr *)in_start)->dump_buf_handle);
	u16 * ret_idx_buf = (u16 *)(out_start + sizeof(unsigned int));
	gsi_log("dev side apu_ilp_find_seq called.\n");

#ifdef APUC_TYPE_hw
	u32 * pArcDbgReg = (u32*)0xF000_0208;
	gsi_log("writing 2022 to %p", pArcDbgReg);
	*pArcDbgReg = 2022;
#endif
	if (phdr->num_params != 9) {
		gsi_log("Error! apu_ilp_find_seq expects 9 params but received %hu.\n", phdr->num_params);
		ret = FAILURE;
		return ret;
	}
	int max_ret_per_q = phdr->params[t_find_seq_max_ret_per_q];
	int src_rows = phdr->params[t_find_seq_num_rows];
	int numq = phdr->params[t_find_seq_numq];
	int num_blocks = phdr->params[t_find_seq_num_blocks];
	int block_size = phdr->params[t_find_seq_block_size];
	int qbatch = phdr->params[t_find_seq_qbatch];
	bool b_nearest = (bool)(phdr->params[t_find_seq_b_nearest]);
	bool b_thresh = (bool)(phdr->params[t_find_seq_b_thresh]);
	gsi_log("apu_ilp_find_seq: qbatch = %d. b_nearest = %d, b_thresh = %d", qbatch, b_nearest, b_thresh);
	unsigned char * dbuf = (unsigned char *)((char *)phdr + phdr->buf_offsets[0]);
	//u16 * qlens_buf = (u16 *)((char *)phdr + phdr->buf_offsets[1]);
	//u16 * ret_idx_buf = (u16 *)((char *)phdr + phdr->buf_offsets[1]);
	u16 * ret_hd_buf = NULL; // (u16 *)((char *)phdr + phdr->buf_offsets[2]);
	//int out_buf_size = numq * max_ret_per_q;
	if (dbuf == NULL || ret_idx_buf == NULL || numq == 0 || src_rows == 0) {
		ret = FAILURE;
		return ret;
	}

	enum gvrc_vr16 vr_db_start = GVRC_VR16_0;
	enum gvrc_vr16 vr_widx = vr_db_start + src_rows;
	enum gvrc_vr16 vr_valid_mrks = vr_widx + 1; // NB For now, only used for thresh
	tSVR_AVAIL avail_info;
	setup_avail_info(&avail_info);
	// for all fns only block the db and widx. Thresh will mark as
	for (enum gvrc_vr16 e = vr_db_start; e <= vr_widx; e++) {
		avail_info.vrs[e] = egvrcArg;
	}
	if (b_thresh) {
		avail_info.vrs[vr_valid_mrks] = egvrcArg;
	}
	enum gvml_mrks_n_flgs m_wstarts = (enum gvml_mrks_n_flgs)(1 << g_db_info.mrk_start); // GVML_MRK0; prefer to use 1 << notation than hide with GVML_MRK0
	enum gvml_mrks_n_flgs m_wends = (enum gvml_mrks_n_flgs)(1 << g_db_info. mrk_end);
	avail_info.mrks[g_db_info.mrk_start] = egvrcArg;
	avail_info.mrks[g_db_info.mrk_end] = egvrcArg;
//	const u16 c_qblock_num = 128;
//	const u16 c_qblock_size = 4 * sizeof(u16) * c_qblock_num;
//	u16 * l3buf = (u16 *)gvrc_copy_l4_to_l3(dbuf, block_size);
//	int cmp_ret = memcmp(l3buf, dbuf, block_size);
//	gsi_log("memcmp on gvrc_copy_l4_to_l3 return %d.", cmp_ret);
//	u16 * qbuf1 = gvrc_alloc_l3_buf(block_size);
//	u16 * qbuf2 = gvrc_alloc_l3_buf(block_size);
//	int qiox = gvrc_copy_l4_to_l3_start(dbuf, block_size);
//	qiox = gvrc_copy_l4_to_l3_next(qbuf1, block_size, qiox);
//	gvrc_copy_sync(qiox);
//	tSBlockInfo * pinfo = (tSBlockInfo *)qbuf1;
//	u16 * pqdata = (u16 *)((unsigned char *)pinfo + sizeof(tSBlockInfo));
//	u16 * plen_data = (u16 *)((unsigned char *)pinfo + pinfo->qlens_offset);
//	bool bfirst_buf = true;

//	if (num_blocks > 0) {
//		qiox = gvrc_copy_l4_to_l3_start(dbuf + block_size, block_size);
//	}
	int qnum = 0;
	u16 * l3mbuf1 = mgal_malloc_cache_aligned(512);
	u16 * l3mbuf2 = mgal_malloc_cache_aligned(512);
//	uint32_t *non_align_l3_buff = gal_malloc(block_size + (CACHE_LINE_SIZE));
//	memset(non_align_l3_buff, 0, (block_size + (CACHE_LINE_SIZE)));
	u8* pin_buf = (u8*)dbuf;
	mgal_l2dma_async_memcpy((u8*)l3mbuf1, pin_buf, 512, 0, true, true, true);
	bool bflip = false;
	bool bfirst = true; bool blast = false; bool b_store_empty = true;
	int i_out_buf = 0;
	gvrc_reset_16(vr_valid_mrks);
	u16 berr;
	for (int iblock = 0; iblock < num_blocks; iblock++, bflip = !bflip) {
		u8 * p1 = (u8*)(bflip ? l3mbuf1 : l3mbuf2);
		u8 * p2 = (u8*)(bflip ? l3mbuf2 : l3mbuf1);
		if (iblock < (num_blocks-1)) {
			// each call to the memcpy does a sync on the previous call before returning.
			mgal_l2dma_async_memcpy(p1, pin_buf + ((iblock+1) * block_size), 512, 0, true, true, true);
		}
		else {
			mgal_l2dma_async_memcpy_end(0);
		}
//		mgal_l2dma_async_memcpy_end(0);
//		mgal_l2dma_async_memcpy_end(1);
//		u16 * l3buf = (u16 *)gvrc_copy_l4_to_l3(dbuf + (iblock * block_size), block_size, non_align_l3_buff);
		tSBlockInfo * pinfo = (tSBlockInfo *)p2;
		//tSBlockInfo * pinfo = (tSBlockInfo *)(dbuf + (iblock * block_size));
		u16 * pqdata = (u16 *)((unsigned char *)pinfo + sizeof(tSBlockInfo));
		//u16 * plen_data = (u16 *)((unsigned char *)pinfo + pinfo->qlens_offset);
		int buff_off = 0;
		u16 len_ret;
//		int half_point = pinfo->block_numq / 2;
//		gsi_log("apu_ilp_find_seq(): block %d, numq %d.", iblock, pinfo->block_numq);
		for (int iq = 0; iq < pinfo->block_numq; iq+=qbatch, qnum+=qbatch) {
//			if (qnum < 2731) {
//				gsi_log("test_mod: block %d, iq %d, qnum %d: ", iblock, iq, qnum);
//				for (int iel = 0; iel < (plen_data[iq]*src_rows); iel++) {
//					gsi_log("%hu, ", pqdata[buff_off + iel]);
//				}
//				gsi_log("\n");
//			}
			if (b_nearest && !b_thresh) {
				gsi_log("dev side calling ilp_1q_find_close for qnum = %d\n", qnum);
				ilp_1q_find_close(	&(ret_hd_buf[qnum*max_ret_per_q]), &(ret_idx_buf[qnum*max_ret_per_q]),
									vr_db_start, g_db_info.num_rows,
									vr_widx, m_wstarts, m_wends, qnum, &(pqdata[buff_off]), &len_ret,
									(u16)max_ret_per_q,	&avail_info);
			}
			else if (b_thresh) {
				//gsi_log("dev side calling ilp_1q_find_thresh for qnum = %d\n", qnum);
				if ((qnum % 64) == 0) {
					// if not zero, process and extract values
					if (qnum != 0) {
//						gsi_log("Error! Not implemented yet!");
						gsi_log("Calling ilp_shift for qnum = %d at i_out_buf = %d", qnum, i_out_buf);
						int old_i_out_buf = i_out_buf;
						ilp_shift(	(u8 *)ret_idx_buf, &i_out_buf, bfirst, blast, b_store_empty, qnum,
									vr_db_start, vr_widx, vr_valid_mrks,
									m_wstarts, m_wends, &berr, &avail_info);
						//if (qnum == 384) {
							//dump_all_mmb(p_dump_buf);
						//}
						bfirst = false; b_store_empty = false;
						if (i_out_buf > old_i_out_buf) {
							//bfirst = true;
							b_store_empty = true;
							// if we are here, the i_out_buf was incremented which means data was written to l4 which
							// means the store failed. We need to try again.
							gsi_log("Calling repeat ilp_shift for qnum = %d at i_out_buf = %d", qnum, i_out_buf);
							ilp_shift(	(u8 *)ret_idx_buf, &i_out_buf, bfirst, blast, b_store_empty, qnum,
										vr_db_start, vr_widx, vr_valid_mrks,
										m_wstarts, m_wends, &berr, &avail_info);
							b_store_empty = false;
						}
//						return 1;
					}
				}
				if (b_nearest) {
					gsi_log("dev side calling pnlp function.");
					ilp_rels_find(	vr_db_start,
										vr_widx, m_wstarts, m_wends, qnum, &(pqdata[buff_off]), &len_ret,
										vr_valid_mrks, &avail_info);
				}
				else {
					ilp_1q_find_thresh(	vr_db_start,
										vr_widx, m_wstarts, m_wends, qnum, &(pqdata[buff_off]), &len_ret,
										vr_valid_mrks, &avail_info);
				}
			}
			else {
				gsi_log("dev side calling ilp_1q_find_seq for qnum = %d\n", qnum);
				ilp_1q_find_seq(	&(ret_idx_buf[qnum*max_ret_per_q]), vr_db_start,
									vr_widx, m_wstarts, m_wends, qnum, &(pqdata[buff_off]), &len_ret,
									max_ret_per_q,	g_db_info.total_hbs, &avail_info);
			}
			// len_ret is expressed as 4 times smaller than it is because of the query quanta of 4
			gsi_log("qnum: %d, len ret = %hu, buff_off %d, add %p", qnum, len_ret, buff_off, &(pqdata[buff_off]));
			buff_off += (len_ret*(src_rows + 2))+(2*qbatch); // for each el, 4 words, hd, copymove, then for q: 1 len, 1 overall thresh
//			if (iq == half_point) {
//				if (iblock < (num_blocks-1)) {
//					gsi_log("apu_ilp_find_seq: calling gvrc_copy_l4_to_l3_next for block %d", iblock+1);
//					qiox = gvrc_copy_l4_to_l3_next((bfirst_buf ? qbuf2 : qbuf1), block_size, qiox);
//				}
//			}
		}
//		if (iblock < (num_blocks-1)) {
//			gvrc_copy_sync(qiox);
//			bfirst_buf = !bfirst_buf;
//			pinfo = (tSBlockInfo *)(bfirst_buf ? qbuf1 : qbuf2);
//			pqdata = (u16 *)((unsigned char *)pinfo + sizeof(tSBlockInfo));
//			plen_data = (u16 *)((unsigned char *)pinfo + pinfo->qlens_offset);
//			if (iblock < (num_blocks-2)) {
//				gsi_log("apu_ilp_find_seq: calling gvrc_copy_l4_to_l3_start for block %d", iblock+2);
//				qiox = gvrc_copy_l4_to_l3_start(dbuf + (block_size * (iblock + 2)), block_size);
//			}
//		}
	}
	if (b_thresh) {
		blast = true;
		gsi_log("Calling ilp_shift for last write");
		ilp_shift(	(u8 *)ret_idx_buf, &i_out_buf, bfirst, blast, b_store_empty,
					qnum, vr_db_start, vr_widx, vr_valid_mrks, m_wstarts, m_wends, &berr, &avail_info);
		if (berr != 0) {
			gsi_log("Calling repeat of ilp_shift for last write");
			b_store_empty = false;
			ilp_shift(	(u8 *)ret_idx_buf, &i_out_buf, bfirst, blast, b_store_empty, qnum, vr_db_start, vr_widx,
						vr_valid_mrks, m_wstarts, m_wends, &berr, &avail_info);
		}
//		dump_all_mmb(p_dump_buf, GVML_VM_0);
		// Old and incorrect: Each APUC puts out one buf. So each buf is the half mem size of the APU. ret idx and q idx each get the own i_out_buf
		*(unsigned int *)out_start = (unsigned int)(sizeof(unsigned int) + (L2_MEMORY_SIZE  * sizeof(u16) * i_out_buf));
		gsi_log("ilp_find_seq for b_thresh: writing a message of length %u", *(unsigned int *)out_start);
		gsi_log("NB!!! restore the extract for testing - are we ready to delete this msg?");
		//extract_vr(ret_idx_buf, vr_valid_mrks);
	}

	mgal_l2dma_async_memcpy_end(0);
//	mgal_l2dma_async_memcpy_end(1);
//	gal_free(non_align_l3_buff);
	mgal_free_cache_aligned(l3mbuf1);
	mgal_free_cache_aligned(l3mbuf2);
	//*(unsigned int *)out_start = (unsigned int)(sizeof(unsigned int));

	return ret;
}
