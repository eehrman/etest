/*
 * Copyright (C) 2019, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#ifndef READ_RL_TEST_INTERN_H
#define READ_RL_TEST_INTERN_H

struct read_rl_test_in_out {
	char buffer[64];
};

#define MAX_SOCK_BUFF_SIZE 0x00ffffff // return to 0x0fffffff
typedef struct SRemoteHdr {
	unsigned int in_buff_size;
	unsigned int ret_buff_size;
	unsigned int dump_buf_handle;
} tSRemoteHdr;


#define BUF_HDR_MAX_PARAMS 10
#define BUF_HDR_MAX_BUFS 20

typedef struct __attribute__((__packed__)) SBufHdr {
	u16 num_params;
	u16 num_bufs;
	unsigned int params[BUF_HDR_MAX_PARAMS];
	unsigned int buf_offsets[BUF_HDR_MAX_BUFS];
	unsigned int buf_sizes[BUF_HDR_MAX_BUFS];
	u16 buf_size_total;
} tSBufHdr;

enum {
	t_remote_fn_req = 0,
	t_get_fns_max_name_len = 1,
	t_get_fns_max_num_funcs = 2,
	t_get_fns_num_funcs = 3,
	t_init_db_data_len = 1,
	t_init_db_numw = 2,
	t_init_db_num_rows = 3,
	t_find_seq_numq = 1,
	t_find_seq_max_ret_per_q = 2,
	t_find_seq_num_rows = 3,
	t_find_seq_b_nearest = 4, // lowest hd, not only the exact
	t_find_seq_num_blocks = 5,
	t_find_seq_block_size = 6,
	t_find_seq_qbatch = 7,
	t_find_seq_b_thresh = 8, // basically. call find_thresh
	t_get_dump_data_ivr = 1,
	t_get_dump_data_start = 2,
	t_get_dump_data_num_cols = 3,
	//t_test_data_path_
};

typedef struct SBlockInfo {
	int block_numq;
	int qlens_offset;
} tSBlockInfo;

#define MAX_NUM_BLOCKS 300000
#define MAX_THRESH_PER_EL 3

#endif /* READ_RL_TEST_INTERN_H */
