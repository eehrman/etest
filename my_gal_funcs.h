/*
 * Copyright (C) 2019, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <gsi/libgal.h>
// #include <gsi/libsys/types.h>

#ifndef __MY_GAL_FUNCS_H_
#define __MY_GAL_FUNCS_H_

#define MGAL_L2DMA_APC_ID_0 0
#define MGAL_L2DMA_APC_ID_1 1

extern void *memset (void *__s, int __c, size_t __n);
/* Copy N bytes of SRC to DEST.  */
extern void *memcpy (void *__restrict __dest, const void *__restrict __src,
		     size_t __n);

static inline void *mgal_malloc_cache_aligned(u32 size)
{
	u8 *p = gal_malloc(size + 32 + 32);
	u32 *algn_p_u32 = (u32 *)((u32)(p + 32) & 0xffffffe0);

	*(algn_p_u32 - 1) = (u32)p;

	return (void *)algn_p_u32;
}

static inline void mgal_free_cache_aligned(void *algn_p)
{
	gal_free((void *)(*(((u32 *)algn_p) - 1)));
}

#ifdef APUC_TYPE_hw

#define MGAL_RFU_L2_A_CMD_LO_REG 0xF0000300
#define MGAL_RFU_L2_B_CMD_LO_REG 0xF0000380
#define MGAL_RFU_L2_A_CMD_HI_REG 0xF0000308
#define MGAL_RFU_L2_B_CMD_HI_REG 0xF0000388
#define MGAL_RFU_L2_A_CMD_ADDR_REG 0xF0000310
#define MGAL_RFU_L2_B_CMD_ADDR_REG 0xF0000390


//bit 31
#define MGAL_L2DMA_CMD_LOW_ENABLE_MASK (1 << 31)
//bit30
#define MGAL_L2DMA_CMD_LOW_PSEL_DMC_MASK (1 << 30)
#define MGAL_L2DMA_CMD_LOW_PSEL_NO_DMC_MASK (0 << 30)
//bits 29,28
#define MGAL_L2DMA_CMD_LOW_CMD_TYPE_RDSYS_MASK (0 << 28)
#define MGAL_L2DMA_CMD_LOW_CMD_TYPE_WRSYS_MASK (1 << 28)
#define MGAL_L2DMA_CMD_LOW_CMD_TYPE_MCRDL2_MASK (2 << 28)
#define MGAL_L2DMA_CMD_LOW_CMD_TYPE_MCWRL2_MASK (3 << 28)
//bit 27,26
#define MGAL_L2DMA_CMD_LOW_SYS_BURST8_MASK (0 <<26)
#define MGAL_L2DMA_CMD_LOW_SYS_BURST16_MASK (1 <<26)
#define MGAL_L2DMA_CMD_LOW_SYS_BURST32_MASK (2 <<26)
#define MGAL_L2DMA_CMD_LOW_SYS_BURST64_MASK (3 <<26)
//bits 25,24
#define MGAL_L2DMA_CMD_LOW_ATTRIBUTE_CMD_NOP_MASK (0 << 24)
#define MGAL_L2DMA_CMD_LOW_ATTRIBUTE_CMD_RESERVED_MASK (1 << 24)
#define MGAL_L2DMA_CMD_LOW_ATTRIBUTE_CMD_RST_READY_MASK (2 << 24)
#define MGAL_L2DMA_CMD_LOW_ATTRIBUTE_CMD_SET_READY_MASK (3 << 24)
//bits 23,22-number of bursts per transaction.
#define MGAL_L2DMA_CMD_LOW_NUM_OF_BURST1_MASK (0 << 22)
#define MGAL_L2DMA_CMD_LOW_NUM_OF_BURST2_MASK (1 << 22)
#define MGAL_L2DMA_CMD_LOW_NUM_OF_BURST4_MASK (2 << 22)
#define MGAL_L2DMA_CMD_LOW_NUM_OF_BURST8_MASK (3 << 22)

//This field apply to MC* and WRL2 command
//types:
//The number of
//command execution
//iterations NumRep =
//NumRep_Reg+1.
//bits 21-16
#define MGAL_L2DMA_CMD_LOW_NUM_OF_REPET_MASK (0x3F << 16)
//bit 15
#define MGAL_L2DMA_CMD_LOW_IRQ_EN_MASK (1 << 15)
//bits 14-8
#define MGAL_L2DMA_CMD_LOW_ROW_ADDR_MASK (0x7F << 8)

//bits 7-6
#define MGAL_L2DMA_CMD_LOW_L2T_MODE8 (0<<6)
#define MGAL_L2DMA_CMD_LOW_L2T_MODE16 (1<<6)
#define MGAL_L2DMA_CMD_LOW_L2T_MODE32 (2<<6)
#define MGAL_L2DMA_CMD_LOW_L2T_MODE64 (3<<6)

/*
 * Write uncached u32 data
 */
static inline void mgal_cache_uc_u32_wr(u32 addr, u32 data)
{
	*(volatile _Uncached u32 *)addr = data;
}

/*
 * Read uncached u32 data
 */
static inline u32 mgal_cache_uc_u32_rd(u32 addr)
{
	return *(volatile _Uncached u32 *)addr;
}

/*
 * func name : apc_l2dma_set_cmd_lo_reg_uc().
 * description : write l2dma low command RFU reg.
 * param : uint32_t val - address  .
 * return value : none.
 */
static inline void apc0_l2dma_set_cmd_lo_reg_uc(uint32_t val)
{
	mgal_cache_uc_u32_wr(MGAL_RFU_L2_A_CMD_LO_REG, val);
}
static inline void apc1_l2dma_set_cmd_lo_reg_uc(uint32_t val)
{
	mgal_cache_uc_u32_wr(MGAL_RFU_L2_B_CMD_LO_REG, val);
}

/*
 * func name : apc_l2dma_get_cmd_lo_reg_uc().
 * description : get  l2dma low command RFU reg.
 * param : uint32_t val - address  .
 * return value : uint32_t - rfu reg value.
 */
static inline uint32_t apc0_l2dma_get_cmd_lo_reg_uc(void)
{
	return mgal_cache_uc_u32_rd(MGAL_RFU_L2_A_CMD_LO_REG);
}
static inline uint32_t apc1_l2dma_get_cmd_lo_reg_uc(void)
{
	return mgal_cache_uc_u32_rd(MGAL_RFU_L2_B_CMD_LO_REG);
}

/*
 * func name : apc_l2dma_set_cmd_hi_reg_uc().
 * description : write l2dma high command RFU reg.
 * param : uint32_t val - address  .
 * return value : none.
 */
static inline void apc0_l2dma_set_cmd_hi_reg_uc(uint32_t val)
{
	mgal_cache_uc_u32_wr(MGAL_RFU_L2_A_CMD_HI_REG, val);
}
static inline void apc1_l2dma_set_cmd_hi_reg_uc(uint32_t val)
{
	mgal_cache_uc_u32_wr(MGAL_RFU_L2_B_CMD_HI_REG, val);
}

/*
 * func name : apc_l2dma_set_sys_addr_reg_uc().
 * description : write system memory address into RFU reg.
 * param : uint32_t val - address  .
 * return value : none.
 */
static inline void apc0_l2dma_set_sys_addr_reg_uc(uint32_t val)
{
	mgal_cache_uc_u32_wr(MGAL_RFU_L2_A_CMD_ADDR_REG, val);
}
static inline void apc1_l2dma_set_sys_addr_reg_uc(uint32_t val)
{
	mgal_cache_uc_u32_wr(MGAL_RFU_L2_B_CMD_ADDR_REG, val);
}

/*
 * func name : l2dma_queue_set_l2_ready_cmd().
 * description : prepare l2dma for transfer operation.
 * return value : none.
 */
static inline void apc0_l2dma_queue_set_l2_ready_cmd(void)
{
	u32 low_cmd = 0;
	low_cmd |= MGAL_L2DMA_CMD_LOW_ENABLE_MASK;		 //Trigger command execution.	Set by SW, Cleared by 	HW when copy CMD
	//to Shadow Register.
	low_cmd |= MGAL_L2DMA_CMD_LOW_PSEL_NO_DMC_MASK;	 	//Peripheral Select : 0 = custom L2DMA.
	low_cmd |= MGAL_L2DMA_CMD_LOW_ATTRIBUTE_CMD_SET_READY_MASK;	//Attribute of RDL2,WRL2 command and	L2_READY_SR. 3="set-ready".

	apc0_l2dma_set_cmd_lo_reg_uc(low_cmd);			//write low cmd l2dma register.
}
static inline void apc1_l2dma_queue_set_l2_ready_cmd(void)
{
	u32 low_cmd = 0;
	low_cmd |= MGAL_L2DMA_CMD_LOW_ENABLE_MASK;		//Trigger command execution.	Set by SW, Cleared by 	HW when copy CMD
	//to Shadow Register.
	low_cmd |= MGAL_L2DMA_CMD_LOW_PSEL_NO_DMC_MASK;	 	//Peripheral Select : 0 = custom L2DMA.
	low_cmd |= MGAL_L2DMA_CMD_LOW_ATTRIBUTE_CMD_SET_READY_MASK;	//Attribute of RDL2,WRL2 command and	L2_READY_SR. 3="set-ready".

	apc1_l2dma_set_cmd_lo_reg_uc(low_cmd);			//write low cmd l2dma register.
}

static inline void apc0_l2dma_queue_dummy(void)
{
	u32 low_cmd = 0;
	low_cmd |= MGAL_L2DMA_CMD_LOW_ENABLE_MASK;		 //Trigger command execution.	Set by SW, Cleared by 	HW when copy CMD
	//to Shadow Register.
	low_cmd |= MGAL_L2DMA_CMD_LOW_PSEL_NO_DMC_MASK;	 	//Peripheral Select : 0 = custom L2DMA.

	apc0_l2dma_set_cmd_lo_reg_uc(low_cmd);			//write low cmd l2dma register.
}
static inline void apc1_l2dma_queue_dummy(void)
{
	u32 low_cmd = 0;
	low_cmd |= MGAL_L2DMA_CMD_LOW_ENABLE_MASK;		//Trigger command execution.	Set by SW, Cleared by 	HW when copy CMD
	//to Shadow Register.
	low_cmd |= MGAL_L2DMA_CMD_LOW_PSEL_NO_DMC_MASK;	 	//Peripheral Select : 0 = custom L2DMA.

	apc1_l2dma_set_cmd_lo_reg_uc(low_cmd);			//write low cmd l2dma register.
}


/*
 * func name : l2dma_queue_mem_to_l2t_512().
* description : copy memory into l2t using l2dma.
* param :u32 src - source address.
* return value : none.
*/
static inline void apc0_l2dma_queue_mem_to_l2t_512(u32 src)
{
	apc0_l2dma_set_sys_addr_reg_uc(src);

	u32 low_cmd =
	        MGAL_L2DMA_CMD_LOW_ENABLE_MASK |		//Trigger command execution.	Set by SW, Cleared by 	HW when copy CMD
	        //to Shadow Register.
	        MGAL_L2DMA_CMD_LOW_PSEL_DMC_MASK |		//Peripheral Select : 1 = DMC.
	        MGAL_L2DMA_CMD_LOW_CMD_TYPE_RDSYS_MASK |	//		(0 << 28) |	// RDSYS move data from l3 /l4 to l2t
	        MGAL_L2DMA_CMD_LOW_SYS_BURST64_MASK |		//		(3 << 26) |	// burst length 64
	        MGAL_L2DMA_CMD_LOW_L2T_MODE64			//		(3 << 6);	// mode 64
	        ;
	apc0_l2dma_set_cmd_lo_reg_uc(low_cmd);
}
static inline void apc1_l2dma_queue_mem_to_l2t_512(u32 src)
{
	apc1_l2dma_set_sys_addr_reg_uc(src);

	u32 low_cmd =
	        MGAL_L2DMA_CMD_LOW_ENABLE_MASK |		//Trigger command execution.	Set by SW, Cleared by 	HW when copy CMD
	        //to Shadow Register.
	        MGAL_L2DMA_CMD_LOW_PSEL_DMC_MASK |		//Peripheral Select : 1 = DMC.
	        MGAL_L2DMA_CMD_LOW_CMD_TYPE_RDSYS_MASK |	//		(0 << 28) |	// RDSYS move data from l3 /l4 to l2t
	        MGAL_L2DMA_CMD_LOW_SYS_BURST64_MASK |		//		(3 << 26) |	// burst length 64
	        MGAL_L2DMA_CMD_LOW_L2T_MODE64			//		(3 << 6);	// mode 64
	        ;
	apc1_l2dma_set_cmd_lo_reg_uc(low_cmd);
}

static inline void apc0_l2dma_queue_mem_to_l2t_256(u32 src)
{
	apc0_l2dma_set_sys_addr_reg_uc(src);

	u32 low_cmd =
	        MGAL_L2DMA_CMD_LOW_ENABLE_MASK |		//Trigger command execution.	Set by SW, Cleared by 	HW when copy CMD
	        //to Shadow Register.
	        MGAL_L2DMA_CMD_LOW_PSEL_DMC_MASK |		//Peripheral Select : 1 = DMC.
	        MGAL_L2DMA_CMD_LOW_CMD_TYPE_RDSYS_MASK |	//		(0 << 28) |	// RDSYS move data from l3 /l4 to l2t
	        MGAL_L2DMA_CMD_LOW_SYS_BURST32_MASK |		//		(3 << 26) |	// burst length 32
	        MGAL_L2DMA_CMD_LOW_L2T_MODE32			//		(2 << 6);	// mode 32
	        ;
	apc0_l2dma_set_cmd_lo_reg_uc(low_cmd);
}
static inline void apc1_l2dma_queue_mem_to_l2t_256(u32 src)
{
	apc1_l2dma_set_sys_addr_reg_uc(src);

	u32 low_cmd =
	        MGAL_L2DMA_CMD_LOW_ENABLE_MASK |		//Trigger command execution.	Set by SW, Cleared by 	HW when copy CMD
	        //to Shadow Register.
	        MGAL_L2DMA_CMD_LOW_PSEL_DMC_MASK |		//Peripheral Select : 1 = DMC.
	        MGAL_L2DMA_CMD_LOW_CMD_TYPE_RDSYS_MASK |	//		(0 << 28) |	// RDSYS move data from l3 /l4 to l2t
	        MGAL_L2DMA_CMD_LOW_SYS_BURST32_MASK |		//		(3 << 26) |	// burst length 32
	        MGAL_L2DMA_CMD_LOW_L2T_MODE32			//		(2 << 6);	// mode 32
	        ;
	apc1_l2dma_set_cmd_lo_reg_uc(low_cmd);
}

/*
 * func name : l2dma_queue_l2t_to_mem_512().
* description : copy l2t to memory using l2dma.
* param :u32 dst - destination address.
* return value : none.
*/
static inline void apc0_l2dma_queue_l2t_to_mem_512(u32 dst)
{
	apc0_l2dma_set_sys_addr_reg_uc(dst);

	u32 low_cmd =
	        MGAL_L2DMA_CMD_LOW_ENABLE_MASK |		//	(1 << 31) |	// active cmd
	        MGAL_L2DMA_CMD_LOW_PSEL_DMC_MASK |		//	(1 << 30) |	// dmc
	        MGAL_L2DMA_CMD_LOW_CMD_TYPE_WRSYS_MASK |	//	(1 << 28) |	// WRSYS move data from l3 /l4 to l2t
	        MGAL_L2DMA_CMD_LOW_SYS_BURST64_MASK |		//	(3 << 26) |	// burst length 64
	        MGAL_L2DMA_CMD_LOW_L2T_MODE64			//	(3 << 6);	// mode 64
	        ;
	apc0_l2dma_set_cmd_lo_reg_uc(low_cmd);
}
static inline  void apc1_l2dma_queue_l2t_to_mem_512(u32 dst)
{
	apc1_l2dma_set_sys_addr_reg_uc(dst);

	u32 low_cmd =
	        MGAL_L2DMA_CMD_LOW_ENABLE_MASK |		//	(1 << 31) |	// active cmd
	        MGAL_L2DMA_CMD_LOW_PSEL_DMC_MASK |		//	(1 << 30) |	// dmc
	        MGAL_L2DMA_CMD_LOW_CMD_TYPE_WRSYS_MASK |	//	(1 << 28) |	// WRSYS move data from l3 /l4 to l2t
	        MGAL_L2DMA_CMD_LOW_SYS_BURST64_MASK |		//	(3 << 26) |	// burst length 64
	        MGAL_L2DMA_CMD_LOW_L2T_MODE64			//	(3 << 6);	// mode 64
	        ;
	apc1_l2dma_set_cmd_lo_reg_uc(low_cmd);
}

static inline void apc0_l2dma_queue_l2t_to_mem_256(u32 dst)
{
	apc0_l2dma_set_sys_addr_reg_uc(dst);

	u32 low_cmd =
	        MGAL_L2DMA_CMD_LOW_ENABLE_MASK |		//	(1 << 31) |	// active cmd
	        MGAL_L2DMA_CMD_LOW_PSEL_DMC_MASK |		//	(1 << 30) |	// dmc
	        MGAL_L2DMA_CMD_LOW_CMD_TYPE_WRSYS_MASK |	//	(1 << 28) |	// WRSYS move data from l3 /l4 to l2t
	        MGAL_L2DMA_CMD_LOW_SYS_BURST32_MASK |		//	(3 << 26) |	// burst length 32
	        MGAL_L2DMA_CMD_LOW_L2T_MODE32			//	(2 << 6);	// mode 32
	        ;
	apc0_l2dma_set_cmd_lo_reg_uc(low_cmd);
}
static inline  void apc1_l2dma_queue_l2t_to_mem_256(u32 dst)
{
	apc1_l2dma_set_sys_addr_reg_uc(dst);

	u32 low_cmd =
	        MGAL_L2DMA_CMD_LOW_ENABLE_MASK |		//	(1 << 31) |	// active cmd
	        MGAL_L2DMA_CMD_LOW_PSEL_DMC_MASK |		//	(1 << 30) |	// dmc
	        MGAL_L2DMA_CMD_LOW_CMD_TYPE_WRSYS_MASK |	//	(1 << 28) |	// WRSYS move data from l3 /l4 to l2t
	        MGAL_L2DMA_CMD_LOW_SYS_BURST32_MASK |		//	(3 << 26) |	// burst length 32
	        MGAL_L2DMA_CMD_LOW_L2T_MODE32			//	(2 << 6);	// mode 32
	        ;
	apc1_l2dma_set_cmd_lo_reg_uc(low_cmd);
}

static inline void apc0_l2dma_clean_queue(void)
{
	while (apc0_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
	apc0_l2dma_queue_dummy();
	while (apc0_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
}
static inline void apc1_l2dma_clean_queue(void)
{
	while (apc1_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
	apc1_l2dma_queue_dummy();
	while (apc1_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
}

/*
 * func name : l2dma_mem_to_mem_512.
* description : copy from L4 to L2T,then copy from L2T to L3/L4.
* param :u32 dst - destination address.
* param :u32 src - source address.
* return value : none.
*/
static inline void mgal_l2dma_mem_to_mem_512(u8 *dst, u8 *src, u32 apc_id)
{
	if (MGAL_L2DMA_APC_ID_0 == apc_id) {
		//get L2DMA status CMDLO reg command EN bit (bit 32)
		while (apc0_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
		apc0_l2dma_queue_mem_to_l2t_512((u32)src);
		//get L2DMA status CMDLO reg command EN bit (bit 32)
		while (apc0_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
		apc0_l2dma_queue_l2t_to_mem_512((u32)dst);
	} else {
		//get L2DMA status CMDLO reg command EN bit (bit 32)
		while (apc1_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
		apc1_l2dma_queue_mem_to_l2t_512((u32)src);
		//get L2DMA status CMDLO reg command EN bit (bit 32)
		while (apc1_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
		apc1_l2dma_queue_l2t_to_mem_512((u32)dst);
	}
}

/*
 * func name : l2dma_l2t_to_mem_512.
* description : copy from L2T to dst on L3/L4.
* param :u32 dst - destination address.
* return value : none.
*/
static inline void mgal_l2dma_l2t_to_mem_512(u8 *dst, u32 apc_id)
{
	if (MGAL_L2DMA_APC_ID_0 == apc_id) {
		while (apc0_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
		apc0_l2dma_queue_l2t_to_mem_512((u32)dst);
	} else {
		while (apc1_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
		apc1_l2dma_queue_l2t_to_mem_512((u32)dst);
	}
}

static inline void mgal_l2dma_mem_to_mem_256(u8 *dst, u8 *src, u32 apc_id)
{
	if (MGAL_L2DMA_APC_ID_0 == apc_id) {
		//get L2DMA status CMDLO reg command EN bit (bit 32)
		while (apc0_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
		apc0_l2dma_queue_mem_to_l2t_256((u32)src);
		//get L2DMA status CMDLO reg command EN bit (bit 32)
		while (apc0_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
		apc0_l2dma_queue_l2t_to_mem_256((u32)dst);
	} else {
		//get L2DMA status CMDLO reg command EN bit (bit 32)
		while (apc1_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
		apc1_l2dma_queue_mem_to_l2t_256((u32)src);
		//get L2DMA status CMDLO reg command EN bit (bit 32)
		while (apc1_l2dma_get_cmd_lo_reg_uc() & (MGAL_L2DMA_CMD_LOW_ENABLE_MASK));
		apc1_l2dma_queue_l2t_to_mem_256((u32)dst);
	}
}

static inline void mgal_l2dma_async_memcpy_init(u32 apc_id)
{
	if (MGAL_L2DMA_APC_ID_0 == apc_id)
		apc0_l2dma_set_cmd_hi_reg_uc(0);
	else
		apc1_l2dma_set_cmd_hi_reg_uc(0);
}

static inline void mgal_l2dma_async_memcpy_end(u32 apc_id)
{
	if (MGAL_L2DMA_APC_ID_0 == apc_id)
		apc0_l2dma_clean_queue();
	else
		apc0_l2dma_clean_queue();
}

static inline void mgal_l2dma_async_memcpy(u8 *dst, u8 *src, u32 size, u32 apc_id, bool flush_src, bool inval_dst, bool flush_dst_edges)
{
	u32 left = size;

	if (unlikely(size < 256)) {
		memcpy(dst, src, size);

		return;
	}

	/* L3 address must be 8 byte aligned, in this case also the size must be 8 byte aligned */
	//GSI_DEBUG_ASSERT((gal_is_l3_addr(src) == 0) || ((((u32)src & 7) == 0) && ((size & 7) == 0)));
	//GSI_DEBUG_ASSERT((gal_is_l3_addr(dst) == 0) || ((((u32)dst & 7) == 0) && ((size & 7) == 0)));

	if (inval_dst && flush_dst_edges) {
		gal_cache_dcache_flush_mlines((u32)dst, 1);
		gal_cache_dcache_flush_mlines((u32)dst + size - 1, 1);
	}

	if (size >= 512) {
		if (flush_src)
			gal_cache_dcache_flush_mlines((u32)src, 512);
		if (inval_dst)
			gal_cache_dcache_invalidate_mlines((u32)dst, 512);
		mgal_l2dma_mem_to_mem_512(dst, src, apc_id);
		left -= 512;
		dst += 512;
		src += 512;
	} else {
		if (flush_src)
			gal_cache_dcache_flush_mlines((u32)src, 256);
		if (inval_dst)
			gal_cache_dcache_invalidate_mlines((u32)dst, 256);
		mgal_l2dma_mem_to_mem_256(dst, src, apc_id);
		left -= 256;
		dst += 256;
		src += 256;
	}
	while (left >= 512) {
		if (flush_src)
			gal_cache_dcache_flush_mlines((u32)src, 512);
		if (inval_dst)
			gal_cache_dcache_invalidate_mlines((u32)dst, 512);
		mgal_l2dma_mem_to_mem_512(dst, src, apc_id);
		left -= 512;
		dst += 512;
		src += 512;
	}
	if (left > 256) {
		if (flush_src)
			gal_cache_dcache_flush_mlines((u32)src, left);
		if (inval_dst)
			gal_cache_dcache_invalidate_mlines((u32)(dst - (512 - left)), left);
		mgal_l2dma_mem_to_mem_512(dst - (512 - left), src - (512 - left), apc_id);
	} else if (left > 0) {
		if (flush_src)
			gal_cache_dcache_flush_mlines((u32)src, left);
		if (inval_dst)
			gal_cache_dcache_invalidate_mlines((u32)(dst - (256 - left)), left);
		mgal_l2dma_mem_to_mem_256(dst - (256 - left), src - (256 - left), apc_id);
	}
}

static inline void mgal_l2dma_memset(u8 *dst, u8 data, u32 size, u32 apc_id, bool inval_dst)
{
	u8 *src;
	u32 left = size;

	if (unlikely(size < 256)) {
		memset(dst, data, size);

		return;
	}

	src = gal_malloc(512);

	if (inval_dst) {
		/* Flush dst buff edges */
		gal_cache_dcache_flush_mlines((u32)dst, 1);
		gal_cache_dcache_flush_mlines((u32)dst + size - 1, 1);
	}

	if (size >= 512) {
		memset(src, data, 512);
		gal_cache_dcache_flush_mlines((u32)src, 512);
		if (inval_dst)
			gal_cache_dcache_invalidate_mlines((u32)dst, 512);
		mgal_l2dma_mem_to_mem_512(dst, src, apc_id);
		left -= 512;
		dst += 512;
	} else {
		memset(src, data, 256);
		gal_cache_dcache_flush_mlines((u32)src, 256);
		if (inval_dst)
			gal_cache_dcache_invalidate_mlines((u32)dst, 256);
		mgal_l2dma_mem_to_mem_256(dst, src, apc_id);
		left -= 256;
		dst += 256;
	}
	while (left >= 512) {
		if (inval_dst)
			gal_cache_dcache_invalidate_mlines((u32)dst, 512);
		mgal_l2dma_l2t_to_mem_512(dst, apc_id);
		left -= 512;
		dst += 512;
	}
	if (left > 256) {
		if (inval_dst)
			gal_cache_dcache_invalidate_mlines((u32)dst - (512 - left), 512);
		mgal_l2dma_mem_to_mem_512(dst - (512 - left), src, apc_id);
	} else if (left > 0) {
		if (inval_dst)
			gal_cache_dcache_invalidate_mlines((u32)dst - (256 - left), 256);
		mgal_l2dma_mem_to_mem_256(dst - (256 - left), src, apc_id);
	}

	mgal_l2dma_async_memcpy_end(apc_id);
	gal_free(src);
}
#else	/* APUC_TYPE_hw defined */
static inline void mgal_l2dma_memset(u8 *dst, u8 data, u32 size, GSI_UNUSED(u32 apc_id), GSI_UNUSED(bool inval_dst))
{
	memset(dst, data, size);
}

static inline void mgal_l2dma_async_memcpy_init(GSI_UNUSED(u32 apc_id))
{
	return;
}

static inline void mgal_l2dma_async_memcpy_end(GSI_UNUSED(u32 apc_id))
{
	return;
}

static inline void mgal_l2dma_async_memcpy(u8 *dst,
                u8 *src,
                u32 size,
                GSI_UNUSED(u32 apc_id),
                GSI_UNUSED(bool flush_src),
                GSI_UNUSED(bool inval_dst),
                GSI_UNUSED(bool flush_dst_edges))
{
	memcpy(dst, src, size);
}
#endif	/* APUC_TYPE_hw not defined */


#endif
