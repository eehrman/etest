#include <stddef.h>
#include <gsi/libapl.h>
#include <gsi/libsys/log.h>
#include <gsi/libgal.h>
#include "etest-utils.h"

/* L1 addressing */
#define APL_VM_ROWS_PER_U16	4

/*
 * Data is organized in L1 in 9-bit groups (8 data + 1 parity):
 * grp0
 * byte[0] bits[0..3]
 * byte[2] bits[0..3]
 * parity[0]
 *
 * grp1
 * byte[0] bits[4..7]
 * byte[2] bits[4..7]
 * parity[2]
 *
 * grp2
 * byte[1] bits[0..3]
 * byte[3] bits[0..3]
 * parity[1]
 *
 * grp3
 * byte[1] bits[4..7]
 * byte[3] bits[4..7]
 * parity[3]
 */
u16 _vm_reg_to_set_ext(int vm_reg, uint *parity_set, uint *row_in_set, uint *parity_grp, uint *parity_row)
{
	uint row;

	*parity_set = vm_reg / 2;
	row = *parity_set * GSI_L1_VA_SET_ADDR_ROWS;
	if (parity_row)
		*parity_row = row + (2 * APL_VM_ROWS_PER_U16);

	if (vm_reg & 1) {
		*row_in_set = APL_VM_ROWS_PER_U16;
		row += APL_VM_ROWS_PER_U16;
		if (parity_grp)
			*parity_grp = 1;
	} else {
		*row_in_set = 0;
		if (parity_grp)
			*parity_grp = 0;
	}

	//GSI_DEBUG_ASSERT(row < GSI_L1_VA_NUM_ROWS);

	return (u16)row;
}

u8 gsi_encode_l2_addr(uint byte_idx, uint bit_idx)
{
	//GSI_DEBUG_ASSERT(bit_idx < 9);
	//GSI_DEBUG_ASSERT(byte_idx <= GSI_L2_CTL_ROW_ADDR_BYTE_IDX_MASK);
	return (u8)((byte_idx << GSI_L2_CTL_ROW_ADDR_BIT_IDX_BITS) | bit_idx);
}

//unsigned short gal_vm_reg_to_set_ext(int vm_reg, unsigned int *parity_set, unsigned int *row_in_set, unsigned int *parity_grp, unsigned int *parity_row)
//{
//	return _vm_reg_to_set_ext(vm_reg, parity_set, row_in_set, parity_grp, parity_row);
//}
//
