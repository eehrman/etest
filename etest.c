/*
 * Copyright (C) 2019, GSI Technology, Inc. All easts reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <string.h>
#include <time.h>
#include <float.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/param.h>
#include <netinet/in.h>
#include <pthread.h>
#include <gsi/libsys/log.h>
#include <gsi/libapl.h>
#include <gsi/libgal.h>
#include <gsi/preproc_defs.h>
#include <gsi/libgdl.h>
#include <gsi/libsys.h>
#include <gsi/common_api.h>
#include <gsi/libsys/types.h>
#include <gsi/gsi_sim_config.h>
//#include <gsi/common_params.h>

#include "etest-intern.h"
#include "etest-defs.h"
#include "seqmatch.h"


#define MAX_ENTS (1024*32)
#define FAILURE -1

#define L2_MEMORY_SIZE ((int)((u32)1<<15))
#define NUM_VRS 24


static bool gc_print_results = false;
static bool gcb_run_app_threads = false;
static uint seed;
static unsigned int g_num_threads = 1;
static pthread_mutex_t g_thread_lock;

typedef struct SFuncData {
	char *name;
	bool brequired;
	int dev_idx;
} tSFuncData;

tSFuncData g_dev_func_tbl[] = {
	{.name = "ilp_get_funcs", .brequired = true, .dev_idx = 0},
	{.name = "ilp_db_init", .brequired = true, .dev_idx = -1},
	{.name = "ilp_find_close", .brequired = true, .dev_idx = -1},
	{.name = "ilp_find_dist", .brequired = true, .dev_idx = -1},
	{.name = "ilp_find_seq", .brequired = true, .dev_idx = -1},
	{.name = "ilp_find_match", .brequired = true, .dev_idx = -1},
	{.name = "ilp_find_thresh", .brequired = true, .dev_idx = -1}
};

typedef enum e_tests {
	do_close,
	do_dist,
	do_seq,
	do_thresh,
	do_match
} te_tests;

te_tests do_test = do_thresh;
static bool gb_test_data_path = false;

tSFuncData *cc_get_funcs = &(g_dev_func_tbl[0]);
tSFuncData *cc_db_init = &(g_dev_func_tbl[1]);
tSFuncData *cc_find_close = &(g_dev_func_tbl[2]);
tSFuncData *cc_find_dist = &(g_dev_func_tbl[3]);
tSFuncData *cc_find_seq = &(g_dev_func_tbl[4]);
tSFuncData *cc_find_match = &(g_dev_func_tbl[5]);
size_t g_num_funcs = sizeof(g_dev_func_tbl) / sizeof(*g_dev_func_tbl);

static struct gsi_option test_options[] = {
	{
		.name = "seed",
		.flags = GSI_OPT_HAS_ARG,
		.type = GSI_OPT_INT,
		.opt_arg = &seed,
	},

//	{
//		.flags = GSI_OPT_INDIR,
//		.opt_arg = gsi_debug_options
//	},

	{ NULL }
};

static u16 run_task(gsi_prod_uint_t code_offset, gdl_mem_handle_t inp, gdl_mem_handle_t outp, unsigned int apuc_id)
{
	//gsi_prod_status_t ret = gdl_run_task(GDL_TEMPORARY_DEFAULT_CTX_ID, code_offset, inp, outp, GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE, GDL_TEMPORARY_DEFAULT_CORE_INDEX);
	gsi_prod_status_t ret = gdl_run_task(	GDL_TEMPORARY_DEFAULT_CTX_ID, code_offset, inp, outp,
											GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
											apuc_id); // values from 0 to 3

	if (ret == 0)
		return 0;
	else
		return 1;
}

int run_etest_test(void); // decl to avoid making a static function which gives a warning if not used
int run_etest_test(void)
{
	gsi_prod_status_t ret = 0;
	gsi_prod_size_t buf_size = gsi_prod_sizeof(struct read_rl_test_in_out);
	gdl_mem_handle_t dev_buf = gdl_mem_alloc_nonull(0, buf_size, GDL_CONST_MAPPED_POOL);
	struct read_rl_test_in_out *conf = gdl_mem_handle_to_host_ptr(dev_buf);

	strcpy(conf->buffer, "etest-test");

	ret = run_task(GDL_TASK(etest_tests), GDL_MEM_HANDLE_NULL, dev_buf, GDL_TEMPORARY_DEFAULT_CORE_INDEX);

	gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);

	return ret;
}

u16 test_fns(void);
u16 test_fns(void)
{
	u16 ret = 0;
	gsi_prod_size_t buf_size = (gsi_prod_size_t)(sizeof(tSBufHdr));
	gdl_mem_handle_t dev_buf = gdl_mem_alloc_nonull(0, buf_size, GDL_CONST_MAPPED_POOL);
	tSBufHdr * phdr = gdl_mem_handle_to_host_ptr(dev_buf);
	phdr->num_bufs = 0; phdr->num_params = 0; phdr->buf_size_total = 0;
	ret = run_task(GDL_TASK(apu_ilp_test_fns), GDL_MEM_HANDLE_NULL, dev_buf, GDL_TEMPORARY_DEFAULT_CORE_INDEX);
	gsi_info("apu_ilp_test_fns() done\n");
	gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);
	return ret;
}

static u16 get_dev_functions(void)
{
	const u16 c_max_name_len = 32;
	const u16 c_max_num_funcs = 128;
	u16 ret = 0;
	int buf_size_total = c_max_name_len * c_max_num_funcs * sizeof(char);
	gsi_prod_size_t buf_size = (gsi_prod_size_t)(sizeof(tSBufHdr) + buf_size_total);
	gdl_mem_handle_t dev_buf = gdl_mem_alloc_nonull(0, buf_size, GDL_CONST_MAPPED_POOL);
	tSBufHdr * phdr = gdl_mem_handle_to_host_ptr(dev_buf);
	phdr->num_bufs = 1; phdr->num_params = 2; phdr->buf_size_total = 0;
	phdr->params[t_get_fns_max_name_len] = c_max_name_len; phdr->params[t_get_fns_max_num_funcs] = c_max_num_funcs;
	phdr->buf_offsets[0] = (u16)sizeof(tSBufHdr);
	phdr->num_bufs = 1;

	ret = run_task(GDL_TASK(apu_ilp_get_fns), GDL_MEM_HANDLE_NULL, dev_buf, GDL_TEMPORARY_DEFAULT_CORE_INDEX);
	if (ret) {
		gsi_info("apu_ilp_get_fns() failed ret=%d\n", ret);
		gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);
		return ret;
	}

	if (phdr->num_params != 3) {
		gsi_info("Error! num_params not set properly on return from apu_ilp_get_fns.\n");
		gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);
		return ret;
	}
	int num_funcs = phdr->params[t_get_fns_num_funcs];
	char * buff = (char *)((char *)phdr + phdr->buf_offsets[0]);
	for (int fn = 0; fn < num_funcs; ++fn) {
		printf("%s\n", &(buff[fn * c_max_name_len]));
	}
	gsi_info("apu_ilp_get_fns() succeeded\n");
	gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);

	return ret;
}

static u16 host_init_apu(unsigned int apuc_id)
{
	pthread_mutex_lock(&g_thread_lock);
	u16 ret = 0;
	long unsigned int in_buf_len_1 = L2_MEMORY_SIZE * sizeof(u8);
	long unsigned int in_buf_len_2 = L2_MEMORY_SIZE * sizeof(u8);
	long unsigned int buf_size_total = in_buf_len_1 + in_buf_len_2;
	gsi_prod_size_t buf_size = (gsi_prod_size_t)(sizeof(tSBufHdr) + buf_size_total);
	gdl_mem_handle_t dev_buf = gdl_mem_alloc_nonull(0, buf_size, GDL_CONST_MAPPED_POOL);
	tSBufHdr * phdr = gdl_mem_handle_to_host_ptr(dev_buf);
	phdr->num_params = 0;
	//phdr->params[t_init_db_data_len] = data_len; phdr->params[t_init_db_numw] = numw;
	phdr->buf_offsets[0] = (unsigned int)sizeof(tSBufHdr);
	phdr->buf_offsets[1] = (unsigned int)(sizeof(tSBufHdr) + in_buf_len_1);
	phdr->num_bufs = 2;
	u16 * in_data_buf_1 = (u16 *)((char *)phdr + phdr->buf_offsets[0]);
	u16 * in_data_buf_2 = (u16 *)((char *)phdr + phdr->buf_offsets[1]);

	for (int ibank = 0; ibank < GSI_MMB_NUM_BANKS; ibank++) {
		for (int i = 0; i < L2_MEMORY_SIZE / 8; i++) {
			in_data_buf_1[(i*GSI_MMB_NUM_BANKS)+ibank] = (u16)(i | (ibank << 12));
			in_data_buf_2[(i*GSI_MMB_NUM_BANKS)+ibank] = (u16)(i | (ibank << 12) | (1 << 14));
		}
	}

	printf("start of init in buf:\n");
	for (int i = 0; i < 32; i++) {
		printf("%hu, ", in_data_buf_1[i]);
	}
	printf("\n");
	printf("start of init in buf 2:\n");
	for (int i = 0; i < 32; i++) {
		printf("%hu, ", in_data_buf_2[i]);
	}
	printf("\n");
	pthread_mutex_unlock(&g_thread_lock);
	ret = run_task(GDL_TASK(apu_init), GDL_MEM_HANDLE_NULL, dev_buf, apuc_id);
	if (ret) {
		gsi_info("host_init_apu() failed ret=%d\n", ret);
		gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);
		return ret;
	}

	gsi_info("host_init_apu() returning %d\n", ret);
	gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);
	return ret;
}


static u16 host_test_data_path()
{
	u16 ret = 0;
	long unsigned int in_buf_len_1 = L2_MEMORY_SIZE * sizeof(u8);
	long unsigned int in_buf_len_2 = L2_MEMORY_SIZE * sizeof(u8);
	long unsigned int ret_buf_len_1 = L2_MEMORY_SIZE * sizeof(u8);
	long unsigned int ret_buf_len_2 = L2_MEMORY_SIZE * sizeof(u8);
	long unsigned int buf_size_total = in_buf_len_1 + in_buf_len_2 + ret_buf_len_1 + ret_buf_len_2;
	gsi_prod_size_t buf_size = (gsi_prod_size_t)(sizeof(tSBufHdr) + buf_size_total);
	gdl_mem_handle_t dev_buf = gdl_mem_alloc_nonull(0, buf_size, GDL_CONST_MAPPED_POOL);
	tSBufHdr * phdr = gdl_mem_handle_to_host_ptr(dev_buf);
	phdr->num_params = 0;
	//phdr->params[t_init_db_data_len] = data_len; phdr->params[t_init_db_numw] = numw;
	phdr->buf_offsets[0] = (unsigned int)sizeof(tSBufHdr);
	phdr->buf_offsets[1] = (unsigned int)(sizeof(tSBufHdr) + in_buf_len_1);
	phdr->buf_offsets[2] = (unsigned int)(sizeof(tSBufHdr) + in_buf_len_1 + in_buf_len_2);
	phdr->buf_offsets[3] = (unsigned int)(sizeof(tSBufHdr) + in_buf_len_1 + in_buf_len_2 + ret_buf_len_1);
	phdr->num_bufs = 4;
	u16 * in_data_buf_1 = (u16 *)((char *)phdr + phdr->buf_offsets[0]);
	u16 * in_data_buf_2 = (u16 *)((char *)phdr + phdr->buf_offsets[1]);
	u16 * ret_data_buf_1 = (u16 *)((char *)phdr + phdr->buf_offsets[2]);
	u16 * ret_data_buf_2 = (u16 *)((char *)phdr + phdr->buf_offsets[3]);

	memset(ret_data_buf_1, 0, ret_buf_len_1);
	memset(ret_data_buf_2, 0, ret_buf_len_2);

	for (int i = 0; i < L2_MEMORY_SIZE / 2; i++) {
		in_data_buf_1[i] = (u16)(i / 8);
		in_data_buf_2[i] = (u16)((i + (L2_MEMORY_SIZE / 2)) / 8);
	}
	printf("start of in buf:\n");
	for (int i = 0; i < 32; i++) {
		printf("%hu, ", in_data_buf_1[i]);
	}
	printf("\n");
	printf("start of in buf 2:\n");
	for (int i = 0; i < 32; i++) {
		printf("%hu, ", in_data_buf_2[i]);
	}
	printf("\n");
	ret = run_task(GDL_TASK(apu_test_data_path), GDL_MEM_HANDLE_NULL, dev_buf, GDL_TEMPORARY_DEFAULT_CORE_INDEX);
	if (ret) {
		gsi_info("host_test_data_path() failed ret=%d\n", ret);
		gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);
		return ret;
	}
	printf("start of ret buf:\n");
	for (int i = 0; i < 32; i++) {
		printf("%hu, ", ret_data_buf_1[i]);
	}
	printf("\n");
	printf("start of ret buf 2:\n");
	for (int i = 0; i < 32; i++) {
		printf("%hu, ", ret_data_buf_2[i]);
	}
	printf("\n");
	for (int i = 0; i < L2_MEMORY_SIZE / 2; i++) {
		if (ret_data_buf_1[i] != in_data_buf_1[i]) {
			gsi_info("data copy error on apuc 0 at pos %d. ret, in, %hu != %hu", i, ret_data_buf_1[i],in_data_buf_1[i]);
			ret = 1;
			break;
		}
		if (ret_data_buf_2[i] != in_data_buf_2[i]) {
			gsi_info("data copy error on apuc 1 at pos %d. ret, in, %hu != %hu", i, ret_data_buf_2[i],in_data_buf_2[i]);
			ret = 1;
			break;
		}
	}

	gsi_info("host_test_data_path() returning %d\n", ret);
	gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);
	return ret;
}

static u16 init_db(u16 * idata, u16 * wstarts, int data_len, int numw, int num_rows, unsigned int apuc_id)
{
	pthread_mutex_lock(&g_thread_lock);
	u16 ret = 0;
	long unsigned int data_buf_len = data_len * num_rows * sizeof(u16);
	long unsigned int wstarts_buf_len = numw * sizeof(u16);
	long unsigned int buf_size_total = data_buf_len + wstarts_buf_len;
	gsi_prod_size_t buf_size = (gsi_prod_size_t)(sizeof(tSBufHdr) + buf_size_total);
	gdl_mem_handle_t dev_buf = gdl_mem_alloc_nonull(0, buf_size, GDL_CONST_MAPPED_POOL);
	tSBufHdr * phdr = gdl_mem_handle_to_host_ptr(dev_buf);
	phdr->num_params = 3; phdr->params[t_init_db_data_len] = data_len; phdr->params[t_init_db_numw] = numw;
	phdr->params[t_init_db_num_rows] = num_rows;
	phdr->buf_offsets[0] = (unsigned int)sizeof(tSBufHdr);
	phdr->buf_offsets[1] = (unsigned int)(sizeof(tSBufHdr) + data_buf_len);
	phdr->num_bufs = 2;
	u16 * data_buf = (u16 *)((char *)phdr + sizeof(tSBufHdr));
	u16 * wstarts_buf = (u16 *)((char *)data_buf + data_buf_len);
	memcpy(data_buf, idata, data_buf_len);
	memcpy(wstarts_buf, wstarts, wstarts_buf_len);

	pthread_mutex_unlock(&g_thread_lock);
	ret = run_task(GDL_TASK(apu_ilp_init_db), GDL_MEM_HANDLE_NULL, dev_buf, apuc_id);
	if (ret) {
		gsi_info("apu_ilp_init_db() failed ret=%d\n", ret);
		gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);
		return ret;
	}

	gsi_info("init_db() succeeded\n");
	gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);
	return ret;
}

typedef struct STestEl {
	u16 idx;
	u16 hd;
} tSTestEl;

static int el_comp (const void * elem1, const void * elem2)
{
	tSTestEl f = *((tSTestEl*)elem1);
	tSTestEl s = *((tSTestEl*)elem2);
    if (f.hd > s.hd) return  1;
    if (f.hd < s.hd) return -1;
    if (f.idx > s.idx) return  1;
    if (f.idx < s.idx) return -1;
    return 0;
}


static bool test_buffers(u16 * ret_buf, u16 * ret_hd_buf, u16 * test_buf, u16 * test_hd_buf,
						 int buf_len, int numq, int max_ret_per_q) {
	//int iret_buf = 0; int itest_buf = 0;
	tSTestEl ret_set[max_ret_per_q], test_set[max_ret_per_q];
	int ibuf = 0;
	for (int iq = 0; iq < numq; iq++) {
		ibuf = iq * max_ret_per_q;
		int iret = 0;
		for (; iret < max_ret_per_q+1; iret++, ibuf++) {
			if (ibuf >= buf_len) {
				gsi_log("Error! Exceeded return buffer at iq %d out of numq %d.\n", iq, numq);
				return false;
			}
//			if (iret < max_ret_per_q) {
//				gsi_log("test_buffers: iq: %d iret: %d, ret %hu, test %hu", iq, iret, ret_buf[ibuf], test_buf[ibuf]);
//			}
			if (iret == max_ret_per_q || ((ret_buf[ibuf] == 0xffff) && (test_buf[ibuf] == 0xffff))) {
				qsort (ret_set, iret, sizeof(*ret_set), el_comp);
				qsort (test_set, iret, sizeof(*test_set), el_comp);
				for (int iset = 0; iset < iret; iset++) {
//					gsi_log("ret: %hu %hu. test:  %hu %hu.",
//							ret_set[iset].idx, ret_set[iset].hd, test_set[iset].idx, test_set[iset].hd);
					if ((ret_set[iset].idx != test_set[iset].idx) || (ret_set[iset].hd != test_set[iset].hd)) {
						gsi_log("mismatch at iq %d, ret len %d, iel %d: ret vs. test, val %hu vs. %hu, hd %hu vs. %hu\n",
								iq, iret, iset, ret_set[iret].idx, test_set[iret].idx,
								ret_set[iret].hd, test_set[iret].hd);
						return false;
					}
				}
//				ibuf++, ibuf++, ibuf++;
				break;
			}
			if ((ret_buf[ibuf] == 0xffff) || (test_buf[ibuf] == 0xffff)) {
				gsi_log("set length mismatch at iq %d iret %d.\n", iq, iret);
				return false;
			}
			ret_set[iret].idx = ret_buf[ibuf]; test_set[iret].idx = test_buf[ibuf];
			ret_set[iret].hd = ret_hd_buf[ibuf]; test_set[iret].hd = test_hd_buf[ibuf];
		}
	}

	return true;
}

#define QBATCH 4

static u16 host_find_seq(u16 * qdata, u16 * qlens, u16 * hds, u16 * copymoves, int numq,
						__attribute__((unused)) int qlen_total, int num_rows,
						int max_ret_per_q, bool b_nearest, bool b_thresh,
						__attribute__((unused)) u16 * test_buf, __attribute__((unused)) u16 * test_hd_buf,
						unsigned int apuc_id)
{
	u16 ret = 0;

	pthread_mutex_lock(&g_thread_lock);
	int block_len = (int)sizeof(tSBlockInfo);
	const int block_max = (1 << 9); // 512 is the core block size for l4 to l3 transfers
	int block_num = 0;
	static tSBlockInfo block_info_tbl[MAX_NUM_BLOCKS];
	block_info_tbl[0].qlens_offset = block_len;
	block_info_tbl[0].block_numq = 0;
	//int elnum = 0;
	for (int iq=0; iq < numq; iq+=QBATCH) {
		int qsize = 0;
		int qsize_hd = 0;
		int qsize_copymove = 0;
		int qsize_len = 0;
		//int numqb = 0;

		for (int iqb = 0; iqb < QBATCH; iqb++) {
			int qblen = 0;
			if (iq+iqb < numq) {
				qblen = qlens[iq + iqb];
//				for (int iel = 0; iel < qblen; iel++, elnum++) {
//					printf("iq+iqb: %d. hd: %hu, cm: %hu\n", iq+iqb, hds[elnum], copymoves[elnum]);
//				}
				//numqb++;
			}
			qsize += qblen * num_rows * (int)sizeof(u16);
			qsize_hd += qblen * (int)sizeof(u16);
			qsize_copymove += qblen * (int)sizeof(u16);
			qsize_len += 2 * (int)sizeof(u16);// 2: 1 for overall thresh + 1 for storing the length value

		}

		int qsize_tot = qsize + qsize_hd + qsize_copymove + qsize_len;
		block_len += qsize_tot;
		if (block_len >= (block_max - 1)) {
//			printf("host_find_seq(): Block %d, at iq %d, block len %d exceeded block_max\n", block_num, iq, block_len);
			block_num++;
			if (block_num >= MAX_NUM_BLOCKS) {
				printf("Error! The maximum number of blocks of size %d supported by the code is %d.\n",
						block_max, MAX_NUM_BLOCKS);
				return 1;
			}
			block_len = (int)sizeof(tSBlockInfo);
			block_info_tbl[block_num].qlens_offset = block_len;
			block_info_tbl[block_num].block_numq = 0;
			block_len += qsize_tot;
		}
		block_info_tbl[block_num].qlens_offset += qsize_tot;
		block_info_tbl[block_num].block_numq+=QBATCH;
	}
	int num_blocks = block_num + 1;
	long unsigned int data_buf_len = num_blocks * block_max; // (u32)qlen_total * (u32)num_rows * sizeof(u16);
	// long unsigned int qlens_buf_len = (u32)numq * sizeof(u16);
	long unsigned int ret_buf_len = (u32)(numq * max_ret_per_q) * sizeof(u16);
	if (b_thresh) {
		ret_buf_len = (u32)(NUM_HBS * HB_LEN * sizeof(u16));
	}
	// there are two ret buffers, first for indices and the second for the hd distance of the val returned
	long unsigned int buf_size_total = data_buf_len + ret_buf_len + ret_buf_len;
	gsi_prod_size_t buf_size = (gsi_prod_size_t)(sizeof(tSBufHdr) + buf_size_total);
	gdl_mem_handle_t dev_buf = gdl_mem_alloc_nonull(0, buf_size, GDL_CONST_MAPPED_POOL);
	tSBufHdr * phdr = gdl_mem_handle_to_host_ptr(dev_buf);
	phdr->num_params = 8; phdr->params[t_find_seq_numq] = numq;
	phdr->params[t_find_seq_max_ret_per_q] = max_ret_per_q;
	phdr->params[t_find_seq_num_rows] = num_rows;
	phdr->params[t_find_seq_b_nearest] = (int)b_nearest;
	phdr->params[t_find_seq_b_thresh] = (int)b_thresh;
	phdr->params[t_find_seq_num_blocks] = num_blocks;
	phdr->params[t_find_seq_block_size] = block_max;
	phdr->params[t_find_seq_qbatch] = QBATCH;
	phdr->buf_offsets[0] = (unsigned int)sizeof(tSBufHdr);
	phdr->buf_offsets[1] = (unsigned int)(sizeof(tSBufHdr) + data_buf_len);
	phdr->buf_offsets[2] = (unsigned int)(sizeof(tSBufHdr) + data_buf_len + ret_buf_len);
	phdr->num_bufs = 3;
	u16 * data_buf = (u16 *)((char *)phdr + sizeof(tSBufHdr));
	block_num = 0;
	tSBlockInfo * pinfo = (tSBlockInfo * )data_buf;
	*pinfo = block_info_tbl[0];
	u16 * pdata = (u16 *)((u8*)data_buf + sizeof(tSBlockInfo));
	//u16 * plen_data = (u16 *)((u8*)data_buf + block_info_tbl[0].qlens_offset);
	u16 * psrc_data = qdata;
	u16 * psrc_hd = hds;
	u16 * psrc_copymove = copymoves;
	int q_in_block = 0;
	int numq_ru = ((numq + QBATCH) / QBATCH) * QBATCH; // numq r*ounded u*p
	for (int iq=0; iq < numq_ru; iq++) {
		int qlenb = 0;
		if (iq < numq) {
			qlenb = qlens[iq];
		}
		u16 thresh = 0;
		u16 * phds = psrc_hd;
		for (int ihd = 0; ihd < qlenb; ihd++, phds++) {
			thresh = (u16)(thresh + *phds);
		}
		//int qsize = (qlenb * (num_rows + 2)) + 2;
//		printf("block %d, iq %d, writing length %d at address %p.\n", block_num, iq, qlenb, pdata);
		*pdata++ = (u16)(qlenb>>2);
		*pdata++ = thresh;
		memcpy(pdata, psrc_data, qlenb * num_rows * (int)sizeof(u16));
		pdata += qlenb * num_rows;
		memcpy(pdata, psrc_hd, qlenb * (int)sizeof(u16));
		pdata += qlenb;
		memcpy(pdata, psrc_copymove, qlenb * (int)sizeof(u16));
		pdata += qlenb;


//		printf("data for iq = %d: ", iq);
//		for (int iqel = 0; iqel < qlenb; iqel++) {
//			printf("%hu %hu %hu %hu (%hu, %hu), ", psrc_data[iqel], psrc_data[qlenb + iqel],
//					psrc_data[(2 * qlenb) + iqel], psrc_data[(3 * qlenb) + iqel],
//					psrc_hd[iqel], psrc_copymove[iqel]);
//		}
//		printf("\n");
		psrc_data += qlenb * num_rows;
		psrc_hd += qlenb;
		psrc_copymove += qlenb;
		if (++q_in_block >= pinfo->block_numq) {
//			printf("host_find_seq(): block %d, at iq %d, q_in_block %d exceeded %d\n",
//					block_num, iq, q_in_block, pinfo->block_numq);
			block_num++;
			if (block_num >= num_blocks) {
				break;
			}
			pinfo = (tSBlockInfo *)(((u8 *)pinfo) + block_max);
			*pinfo = block_info_tbl[block_num];
//			printf("host_find_seq(): block %d, numq %d.\n", block_num, pinfo->block_numq);
			pdata = (u16 *)((u8*)pinfo + sizeof(tSBlockInfo));
			q_in_block = 0;
		}
	}
//	u16 * qlens_buf = (u16 *)((char *)data_buf + data_buf_len);
//	memcpy(data_buf, qdata, data_buf_len);
//	memcpy(qlens_buf, qlens, qlens_buf_len);
	u16 * ret_buf = (u16 *)((char *)data_buf + data_buf_len);
	u16 * ret_hd_buf = (u16 *)((char *)data_buf + data_buf_len + ret_buf_len);
	memset(ret_buf, 0xff, ret_buf_len * sizeof(u16));
	memset(ret_hd_buf, 0xff, ret_buf_len * sizeof(u16));
	pthread_mutex_unlock(&g_thread_lock);

	struct timespec start, stop;
	clock_gettime(CLOCK_REALTIME, &start);
	ret = run_task(GDL_TASK(apu_ilp_find_seq), GDL_MEM_HANDLE_NULL, dev_buf, apuc_id);
	clock_gettime(CLOCK_REALTIME, &stop);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
	double result = (double)((stop.tv_sec - start.tv_sec) * 1e6 + (stop.tv_nsec - start.tv_nsec) / 1e3);    // in microseconds
#pragma GCC diagnostic pop
	pthread_mutex_lock(&g_thread_lock);
	printf("All apu results returned in %.*e us\n", DECIMAL_DIG, result);
	if (ret) {
		gsi_info("apu_ilp_init_db() failed ret=%d\n", ret);
		gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);
		return ret;
	}
	//u16 num_q_ret = 0;
	ret = 1;
	if (!b_nearest)
		memset(ret_hd_buf, 0, ret_buf_len * sizeof(u16));

	if (gc_print_results) {
//		u16 num_q_ret = 0;
		gsi_info("apu replies:\n");
		int rpos = 0;
		for (int iq = 0; iq < numq; iq++) {
			for (int iret = 0; iret < max_ret_per_q; iret++, rpos++) {
				if (ret_buf[rpos] == 0xffff) continue;
				printf("apu: rpos %hu, qnum %hu: val %hu, hd %hu\n", rpos, iq, ret_buf[rpos], ret_hd_buf[rpos]);
			}
		}
//		for (u16 iret = 0; iret < ret_buf_len; iret++) {
//			if (num_q_ret >= numq) break;
//			gsi_info("iret %hu, qnum %hu: val %hu, hd %hu\n", iret, num_q_ret, ret_buf[iret], ret_hd_buf[iret]);
//			if (ret_buf[iret] == 0xffff) num_q_ret++;
//		}
	}

	if (b_thresh) {
		bool b_mismatch = false;
		for (int ihb = 0; ihb < NUM_HBS; ihb++) {
			for (int icol = 0; icol < HB_LEN; icol++) {
				int ret_pos = (ihb * HB_LEN) + icol;
				if (ret_buf[ret_pos] != test_buf[ret_pos]) {
					printf("Mismatch error at ihb %d col %d, ret vs test: 0x%hx vs 0x%hx\n",
							ihb, icol, ret_buf[ret_pos], test_buf[ret_pos]);
					b_mismatch = true;
					break;
				}
			}
		}
		if (!b_mismatch)
			printf("Thresh test passed!\n");
		else
			printf("Thresh test failed!\n");
	}
	else {
		if (test_buffers(ret_buf, ret_hd_buf, test_buf, test_hd_buf, (int)ret_buf_len, numq, (int)max_ret_per_q)) {
			ret = 0;
		}
	}
	gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);
	gsi_log("host_find_seq returning %d.\n", ret);
	pthread_mutex_unlock(&g_thread_lock);
	return ret;
}


static u16 create_simulator()
{
	struct gsi_sim_contexts board_config[1];
	board_config->apu_count = 1;
	board_config->apucs_per_apu = 4;
	board_config->mem_size = 1000000000;

	gsi_sim_create_simulator(1, board_config);

	return 0;
}


static void shutdown_simulator()
{
	gsi_sim_destroy_simulator();
}

static void error(char *msg)
{
    perror(msg);
    //exit(1);
}

unsigned int g_max_sock_buff_size = MAX_SOCK_BUFF_SIZE;
#define DUMP_BUF_SIZE (L2_MEMORY_SIZE * NUM_VRS * sizeof(u16))

int run_remote_server(void); // decl in order not to make an unused static

int run_remote_server()
{
	gsi_prod_status_t ret = 0;
	gdl_mem_handle_t dev_buf = gdl_mem_alloc_nonull(0, g_max_sock_buff_size, GDL_CONST_MAPPED_POOL);
	gdl_mem_handle_t dev_buf_ret = gdl_mem_alloc_nonull(0, g_max_sock_buff_size, GDL_CONST_MAPPED_POOL);
	gdl_mem_handle_t dev_buf_dump = gdl_mem_alloc_nonull(0, DUMP_BUF_SIZE, GDL_CONST_MAPPED_POOL);
	unsigned char * p_sock_buff = gdl_mem_handle_to_host_ptr(dev_buf);
	unsigned char * p_sock_buff_ret = gdl_mem_handle_to_host_ptr(dev_buf_ret);

	int sockfd, newsockfd, portno;
	unsigned int clilen;
	//char buffer[256];
	struct sockaddr_in serv_addr, cli_addr;
	int n;
	int option = 1;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)  {
		error("ERROR opening socket");
		ret = 1;
		return ret;
	}
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));
	bzero((char *) &serv_addr, sizeof(serv_addr));
	portno = 51717;
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);
	if (bind(sockfd, (struct sockaddr *) &serv_addr,
			 sizeof(serv_addr)) < 0) {
		error("ERROR on binding");
		ret = 1;
		return ret;
	}
    listen(sockfd,5);
	clilen = sizeof(cli_addr);
	int b_keep_going = 1;
	printf("Ready for clients on port %d.\n", portno);
	while (b_keep_going) {
		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
		if (newsockfd < 0) {
			 error("ERROR on accept");
			 ret = 1;
			 return ret;
		}
		bool b_client_alive = true;
		//bzero(buffer,256);
		while (b_client_alive) {
			b_client_alive = false;
			unsigned char * pbuff = p_sock_buff;
			bool bhdr_rcvd = false;
			unsigned int total_recvd = 0;
			unsigned int size_expected = g_max_sock_buff_size;
			bool b_data_ready = false;
			while (1) {
//				n = read(newsockfd,pbuff,MIN(g_max_sock_buff_size, size_expected-total_recvd));
				n = read(newsockfd,pbuff,size_expected-total_recvd);
				if (n < 0) {
					error("ERROR reading from socket");
					break;
				}
				else if (n == 0) {
					if (!bhdr_rcvd) {
						error("Socket closed either by client or error. System reports");
						break;
					}
					else {
						if (total_recvd < size_expected) {
							error("ERROR Received less than expected. Aborting.");
							break;
						}
						else {
							b_data_ready = true;
							break;
						}
					}
				}
				else {
//					printf("remote socket read %d bytes at total_recvd %u, first word %hu\n",
//							n, total_recvd, *(u16 *)pbuff);
					total_recvd += n;
					pbuff += n;
					if (!bhdr_rcvd) {
						if (total_recvd >= (unsigned int)sizeof(tSRemoteHdr)) {
							size_expected = ((tSRemoteHdr *)p_sock_buff)->in_buff_size;
							if (size_expected > g_max_sock_buff_size) {
								printf("ERROR: messages sent to server may not exceed %u. Aborting!\n", g_max_sock_buff_size);
								break;
							}
							bhdr_rcvd = true;
						}
					}
					if (bhdr_rcvd) { // NB Not else! Might have been set just now
						if (total_recvd == size_expected) {
							b_data_ready = true;
							break;
						}
					}
				}
			} // end while 1 till we get the whole message or got an error
			if (b_data_ready) {
				unsigned int max_ret_size = ((tSRemoteHdr *)p_sock_buff)->ret_buff_size;
				((tSRemoteHdr *)p_sock_buff)->dump_buf_handle = dev_buf_dump;
//				tSBufHdr * phdr = (tSBufHdr *)(p_sock_buff + sizeof(tSRemoteHdr));
				ret = run_task(GDL_TASK(apu_remote), dev_buf, dev_buf_ret, GDL_TEMPORARY_DEFAULT_CORE_INDEX);
				if (ret) {
					error("ERROR apu task failed!");
				}
//				else if (max_ret_size > g_max_sock_buff_size) {
//					error("ERROR: Requesting ret buf length greater than allowed.");
//				}
				else if (max_ret_size == 0) {
					b_client_alive = true;
				}
				else if (max_ret_size < (unsigned int)sizeof(unsigned int)) {
					error("ERROR: Requesting ret buf length greater than zero but less than minimum ret.");
					//close(newsockfd);
//					continue;
				}
				else {
					unsigned int ret_size = *(unsigned int*)p_sock_buff_ret;
					unsigned char * pbuff = p_sock_buff_ret;
					unsigned int total_sent = 0;
					printf("Writing %d bytes to client.\n", ret_size);
					while (1) {
						n = write(newsockfd,pbuff,MIN(g_max_sock_buff_size, ret_size - total_sent));
						if (n < 0) {
							error("ERROR writing to socket");
							break;
						}
						else if (n == 0) {
							error("Socket closed at write either by client or error. System reports");
							break;
						}
						else {
							pbuff += n;
							total_sent += n;
							if (total_sent == ret_size) {
								b_client_alive = true;
								break; // out of client loop, not out of while client alive loop
							}
						}
//						else if (n < ret_size) error("ERROR writing to socket fewer bytes than requested. Didn't expect that!");
					} // end while 1 on write
				} // end of if ret was good and data was sent back to client

			} //end if b_data_ready

		} // end while (b_client_alive)
		close(newsockfd); // closing the socket created for this client
		//printf("Here is the message: %s\n",buffer);
//		ret = gdl_run_task(	GDL_TEMPORARY_DEFAULT_CTX_ID, code_offset, inp, outp,
//							GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
//							GDL_TEMPORARY_DEFAULT_CORE_INDEX);
//		if (strcmp(buffer, "exit\n") == 0) {
//			b_keep_going = 0;
//		}
	}

//	for (int i = 0; i < num_tasks_to_run; i++) {
//	}
//
	gdl_mem_free(dev_buf_dump, GDL_CONST_MAPPED_POOL);
	gdl_mem_free(dev_buf, GDL_CONST_MAPPED_POOL);
	gdl_mem_free(dev_buf_ret, GDL_CONST_MAPPED_POOL);
	close(sockfd);
	gsi_info("Host-side server closing.\n");

	return ret;
}

static u16 run_app(unsigned int apuc_id)
{
	u16 c_ret_per_query = 4; // 1 << 4;
	int c_max_num_q = 1 << 18; // (u16)((0x0001 << 15) / c_ret_per_query);
	u16 ret = 0;
	ret = get_dev_functions();
	if (ret) return ret;

	u16 *iquery, *hd_thresh, *copy_move, *wstarts, *isample, *iqlens;
	isample = NULL; wstarts = NULL; hd_thresh = NULL; iquery = NULL; copy_move = NULL; iqlens = NULL;
	int num_rows = 0, vec_size = 0, numw = 0, qlen_total = 0, numq = 0; // qlen is the length of the qbuf, numq is the num of queries
	ret = (u16)get_sample_data(	&iquery, &hd_thresh, &copy_move, &wstarts, &isample, &iqlens,
								&num_rows, &vec_size, &numw, &qlen_total, &numq);
	if (ret) return ret;
	if (numq > c_max_num_q) {
		gsi_log("Error. Cannot handle more than %d queries for now.\n", c_max_num_q);
		return 1; // (gsi_status(EINVAL));
	}
	gsi_log("vec_size = %d, numw = %d, num q = %d, qlen sum = %d, sizeof(dtype) = %lu.\n",
			vec_size, numw, numq, qlen_total, sizeof(u16));
	int out_buf_len = (int)(numq * c_ret_per_query);
	if (do_test == do_thresh) {
		out_buf_len = NUM_HBS * HB_LEN;
	}

	u16 *ret_test_buf = (u16 *)malloc(out_buf_len * sizeof(u16));
	u16 *ret_test_hd_buf = (u16 *)malloc(out_buf_len * sizeof(u16));
	if (do_test == do_seq || do_test == do_close || do_test == do_thresh) {
		bool b_nearest = false;
		bool b_thresh = false;
		if (do_test == do_close) {
			b_nearest = true;
		}
		if (do_test == do_thresh) {
			b_thresh = true;
		}
		ret = (u16)cpu_testseq(	iquery, hd_thresh, copy_move, wstarts, isample, iqlens, num_rows,
								vec_size, numw, numq, ret_test_buf, ret_test_hd_buf,
								out_buf_len, c_ret_per_query, b_nearest, b_thresh);
//		ret = testlink(ret_buf);
	}
	gsi_log("calling host_init_apu\n");
	ret = host_init_apu(apuc_id);
	//return test_fns();
	if (gb_test_data_path) {
		gsi_log("calling host_test_data_path\n");
		ret = host_test_data_path();
		return ret;
	}

	gsi_log("host side calling init_db\n");
	ret = init_db(isample, wstarts, vec_size, numw, num_rows, apuc_id);
	if (ret) {
		gsi_log("Error! init_db returned %d.\n", ret);
		return ret;
	}
	switch (do_test) {
		case do_seq: {
			gsi_log("host side calling host_find_seq\n");
			const bool c_b_not_nearest = false;
			const bool c_b_not_thresh = false;
			ret = host_find_seq(iquery, iqlens, hd_thresh, copy_move, numq, qlen_total, num_rows, c_ret_per_query,
								c_b_not_nearest, c_b_not_thresh, ret_test_buf, ret_test_hd_buf, apuc_id);
			break;
		}
		case do_thresh: {
			gsi_log("host side calling host_find_seq for thresh\n");
			const bool c_b_not_nearest = false;
			const bool c_b_thresh = true;
			ret = host_find_seq(iquery, iqlens, hd_thresh, copy_move, numq, qlen_total, num_rows, c_ret_per_query,
								c_b_not_nearest, c_b_thresh, ret_test_buf, ret_test_hd_buf, apuc_id);
			break;
		}
		case do_close: {
			gsi_log("host side calling host_find_seq for nearest\n");
			const bool c_b_nearest = true;
			const bool c_b_not_thresh = false;
			ret = host_find_seq(iquery, iqlens, hd_thresh, copy_move, numq, qlen_total, num_rows, c_ret_per_query,
								c_b_nearest, c_b_not_thresh, ret_test_buf, ret_test_hd_buf, apuc_id);
			break;
		}
		default:
			gsi_log("Error! do_test value not handled yet.\n");
			break;
	}
	free(ret_test_buf);
	return ret;
}

void *run_app_thread(void * targp);

void *run_app_thread(void * targp)
{
	u16 ret = 0;

	unsigned int *p_apuc_id = (unsigned int *)targp;
	ret = run_app(*p_apuc_id);
	if (ret != 0) {
		gsi_info("\nTest Failed");
	} else {
		gsi_info("\nTest Passed");
	}
	return NULL;
}

int main(int argc, char *argv[])
{
	u16 ret = 0;
	uint num_ctxs;
	long mem_size, num_apucs;

	struct gdl_context_desc contexts_desc[GDL_MAX_NUM_CONTEXTS];
	if (gsi_libsys_init(argv[0]))
		gsi_fatal("gsi_libsys_init failed\n");

	gsi_getopts(argc, argv, test_options);

	create_simulator();

	gdl_init();
	gdl_context_count_get(&num_ctxs);
	gdl_context_desc_get(contexts_desc, num_ctxs);

	gdl_context_property_get(contexts_desc[0].ctx_id, GDL_CONTEXT_MEM_SIZE, &mem_size);
	gdl_context_property_get(contexts_desc[0].ctx_id, GDL_CONTEXT_NUM_APUCS, &num_apucs);

	gsi_info("num_ctxs = %u", num_ctxs);
	gsi_info("mem_size = %ld", mem_size);
	gsi_info("num_apucs = %ld", num_apucs);

	gdl_context_alloc(contexts_desc[0].ctx_id);

	if (gcb_run_app_threads) {
		pthread_t tids[4];
		unsigned int apuc_id_arr[] = {0, 1, 2, 3};
		for (unsigned int apuc_id = 0; apuc_id < g_num_threads; apuc_id++) {
			pthread_create(&(tids[apuc_id]), NULL, run_app_thread, (void *)&(apuc_id_arr[apuc_id]));
		}
		for (unsigned int apuc_id = 0; apuc_id < g_num_threads; apuc_id++) {
			pthread_join(tids[apuc_id], NULL);
		}
	}
	else {
		run_remote_server();
	}

	gdl_context_free(contexts_desc[0].ctx_id);
	gdl_exit();

	shutdown_simulator();

	return ret;
}
