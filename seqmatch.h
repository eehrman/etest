#define EL_NUM_WORDS 4
#define NUM_HBS 16
#define HB_LEN 2048

typedef struct SSMDB {
	//u16 ** indexes;
	u16 **recs[NUM_HBS];
	u16 *lens[NUM_HBS];
	int start_idx[NUM_HBS];
	u16 el_len;
	int num_recs[NUM_HBS];
} tSMDB;
typedef int (* pfn_find)(tSMDB *db, u16 *ret_buf, u16 *ret_hd_buf, int max_ret, int iq, u16 *qdata,
						u16 *hds, u16 *copy_move, u16 qlen);
typedef int (*tfn)(u16 * ret_buf);

tSMDB *seq_match_init(int max_recs, u16 el_len);
int get_sample_data(u16 **piquery, u16 **phd_thresh, u16 **pcopy_move, u16 **pwstarts, u16 **pisample, u16 **piqlens,
                    int *pnum_rows, int *pvec_size, int *pnumw, int *pqlen_sum, int *pnumq);
void seq_match_add_rec(tSMDB *db, int ihb, u16 irec, u16 *els, u16 nels);
//u16 seq_match_find_match(tSMDB *db, u16 *ret_buf, u16 max_ret, u16 iq, u16 *qdata, u16 *hds, u16 *copy_move, u16 qlen);
int cpu_testseq(u16 *iquery, u16 *hd_thresh, u16 *copy_move, u16 *wstarts, u16 *isample, u16 *iqlens,
                int num_rows, int vec_size, int numw, int numq, u16 *ret_buf, u16 * ret_hd_buf,
				int ret_buf_max, int max_ret_per_q, bool b_nearest, bool b_thresh);
int seq_match_find_seq(	tSMDB *db, u16 *ret_buf, u16 * ret_hd_buf,
						int max_ret, int iq, u16 *qdata,
						u16 *hds, u16 *copy_move, u16 qlen);
int seq_match_find_close(	tSMDB *db, u16 *ret_buf, u16 * ret_hd_buf,
						int max_ret, int iq, u16 *qdata,
						u16 *hds, u16 *copy_move, u16 qlen);
int seq_match_find_match(	tSMDB *db, u16 *ret_buf, u16 * ret_hd_buf,
						int max_ret, int iq, u16 *qdata,
						u16 *hds, u16 *copy_move, u16 qlen);
int seq_match_find_thresh(	tSMDB *db, u16 *ret_buf, u16 * ret_hd_buf,
						int max_ret, int iq, u16 *qdata,
						u16 *hds, u16 *copy_move, u16 qlen);
//pfn_find * seq_match_find_seq;
//int testlink(u16 * ret_buf);
