#include <stddef.h>
#include <gsi/libapl.h>
#include <gsi/libsys/log.h>
#include <gsi/libgal.h>
#include "ipl-apl-funcs.apl.h"

tSFlagAvail cFlagAvailTbl[] = {
		// All comments here reference the legacy names and uses. Kept here for checking code that is imported
		{C_FLAG, false},			/* Carry in/out flag */
		{UNUSED_B_FLAG	, true},			/* Borrow in/out flag */ // FREE ME!
		{UNUSED_OF_FLAG, true},			/* Overflow flag */ // FREE ME!
		{PE_FLAG, false},			/* Parity error */
		{PRINT_FLAG, false},			/* marker with 1 set for (each vector-register-index % 16) == 0 */
		{UNUSED_M256_FLAG, true},			/* marker with 1 set for (each vector-register-index % 256) == 0 */
		{UNUSED_M2K_FLAG, true},			/* marker with 1 set for (each vector-register-index % 2048) == 0 */
		{GP0_FLAG, true},			/* Markers for general purpose usage Callee must preserve */
		{GP1_FLAG, true},
		{GP2_FLAG, true},
		{GP3_FLAG, true},
		{GP4_FLAG, true},
		{TMP0_FLAG, true},			/* Markers for general purpose usage Callee may overwrite preserve */
		{TMP1_FLAG, true},
		{TMP2_FLAG, true},
		{TMP3_FLAG, true}
};

int cNumFlags = sizeof(cFlagAvailTbl) / sizeof(*cFlagAvailTbl);


void setup_avail_info(tSVR_AVAIL *avail_info)
{
	for (unsigned int ie = 0; ie < NUM_VRS; ie++) {
		enum gvrc_vr16 e = (enum gvrc_vr16)ie;
		if (e >= GVRC_VR16_FIRST && e < GVRC_VR16_LAST) { // block the last for debug print
			avail_info->vrs[e] = egvrcAvail;
		} else {
			avail_info->vrs[e] = egvrcBlocked;
		}
	}
	for (enum gvml_vm_reg e = 0; e < NUM_VMRS; e++) {
		if (e >= GVML_VM_FIRST && e <= GVML_VM_LAST) {
			avail_info->vmrs[e] = egvrcAvail;
		} else {
			avail_info->vmrs[e] = egvrcBlocked;
		}
	}
	for (unsigned int ie = 0; ie < NUM_MRKS; ie++) {
		enum sm_bits e = (enum sm_bits) ie;
		//if (e >= GP0_FLAG && e < GP4_FLAG) { // block the last for debug print
		if (cFlagAvailTbl[ie].bavail) {
			avail_info->mrks[e] = egvrcAvail;
			//gsi_log("setup_avail_info marking flag %u as avail.\n", ie);
		} else {
			avail_info->mrks[e] = egvrcBlocked;
		}
	}
}

static void init_regs(int num, int * mrks)
{
	for (int im = 0; im < num; im++, mrks++) {
		*mrks = -1;
	}
}

int alloc_use(int num, int *use, tegvrc *resources, int avail, char *name)
{
	int curr = 0;
	for (int im = 0; im < num; im++) {
		bool bfound = false;
		for (int iem = curr; iem < avail; iem++) {
//			gsi_log("init_avail_info: Allocating %s %d, %d has status %d.\n", name, im, iem, (int)resources[iem]);
			if (resources[iem] == egvrcAvail) {
				use[im] = iem; // (enum gvml_mrks_n_flgs)(1 << iem);
				curr = iem + 1;
				bfound = true;
				break;
			}
		}
		if (!bfound) {
			gsi_log("Error! Insufficient available resources of type %s. Can only give you %d.\n", name, im);
			return 1;
		}
	}
	return 0;
}

void set_mrk_reg(tSVR_AVAIL *avail_info, int ireg, int imrk)
{
	avail_info->mrk_regs[imrk] = ireg;
}

void set_vr_reg(tSVR_AVAIL *avail_info, int ireg, int ivr)
{
	avail_info->vr_regs[ivr] = ireg;
}

void set_seu_regs(tSVR_AVAIL *avail_info)
{
	for (int imrk = 0; imrk < avail_info->num_mrks_alloced; imrk++) {
		if (avail_info->mrk_regs[imrk] >= 0) {
//			gsi_log("set_seu_regs: %d: avail_info->mrk_regs[imrk] %d avail_info->use_mrk[imrk]) %d",
//					imrk, avail_info->mrk_regs[imrk], avail_info->use_mrk[imrk]);
			apl_set_sm_reg(avail_info->mrk_regs[imrk], (enum gvml_mrks_n_flgs)(1 << avail_info->use_mrk[imrk]));
		}
	}
	for (int ivr = 0; ivr < avail_info->num_vrs_alloced; ivr++) {
		if (avail_info->vr_regs[ivr] >= 0) {
			apl_set_rn_reg(avail_info->vr_regs[ivr], GVRC_VR16_0 + avail_info->use_vr[ivr]);
		}
	}
}

int init_avail_info(tSVR_AVAIL *avail_info, int num_mrks, int num_vrs, int num_vmrs)
{
	//gsi_log("init_avail_info called to allocate %d mrks, %d vrs and %d vmrs.\n",  num_mrks, num_vrs, num_vmrs);
	if (alloc_use(num_mrks, avail_info->use_mrk, avail_info->mrks, NUM_MRKS, "marker") != 0) {
		return 1;
	}
	if (alloc_use(num_vrs, avail_info->use_vr, avail_info->vrs, NUM_VRS, "vector register") != 0) {
		return 1;
	}
	if (alloc_use(num_vmrs, avail_info->use_vmr, avail_info->vmrs, NUM_VMRS, "vector memory register") != 0) {
		return 1;
	}
	avail_info->num_mrks_alloced = num_mrks;
	avail_info->num_vrs_alloced = num_vrs;
	init_regs(NUM_MRKS, avail_info->mrk_regs);
	init_regs(NUM_VRS, avail_info->vr_regs);

	return 0;
}
