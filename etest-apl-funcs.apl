APL_C_INCLUDE <gsi/preproc_defs.h>
APL_C_INCLUDE <gsi/libapl.h>

#include "ipl-apl-funcs.apl.h"
#include "etest-apl-funcs.apl.h"
//#include "gvrc_fused_ops.h"
#include "etest-utils.h"

//#define SM_REG0 SM_REG_0
//#define SM_0XFFFF SM_REG_4

#define END_RSP16_WA \
        ~SM_0XFFFF: RSP16 = RL;\
        RSP256 = RSP16;\
        RSP2K = RSP256;\
        RSP_START_RET;\
        RSP256 = RSP2K;\
        RSP16 = RSP256;\
        NOOP;\
        RSP_END;\
        ~SM_0XFFFF: RSP16 = RL;\
        RSP256 = RSP16;\
        RSP2K = RSP256;\
        RSP_START_RET;\
        RSP256 = RSP2K;\
        RSP16 = RSP256;\
        NOOP;\
        RSP_END;\


static void read_hbs_entries_from_rsp_fifo(u16 *buffer)
{
	u32 _2_ent_b0;
	u32 _2_ent_b1;
	u32 _2_ent_b2;
	u32 _2_ent_b3;
	//u16 ent = 0;

	/* taking from each half bank in the apc his rsp 2k val */
	apl_rsp_rd(0); // apc_id=0
	_2_ent_b0 = apl_rd_rsp2k_reg(0);
	_2_ent_b1 = apl_rd_rsp2k_reg(1);
	_2_ent_b2 = apl_rd_rsp2k_reg(2);
	_2_ent_b3 = apl_rd_rsp2k_reg(3);
	buffer[0] = (u16)(_2_ent_b0);
	buffer[1] = (u16)(_2_ent_b0 >> 16);
	buffer[2] = (u16)(_2_ent_b1);
	buffer[3] = (u16)(_2_ent_b1 >> 16);
	buffer[4] = (u16)(_2_ent_b2);
	buffer[5] = (u16)(_2_ent_b2 >> 16);
	buffer[6] = (u16)(_2_ent_b3);
	buffer[7] = (u16)(_2_ent_b3 >> 16);

	apl_rsp_rd(1); // apc_id=1
	_2_ent_b0 = apl_rd_rsp2k_reg(0);
	_2_ent_b1 = apl_rd_rsp2k_reg(1);
	_2_ent_b2 = apl_rd_rsp2k_reg(2);
	_2_ent_b3 = apl_rd_rsp2k_reg(3);
	buffer[8] = (u16)(_2_ent_b0);
	buffer[9] = (u16)(_2_ent_b0 >> 16);
	buffer[10] = (u16)(_2_ent_b1);
	buffer[11] = (u16)(_2_ent_b1 >> 16);
	buffer[12] = (u16)(_2_ent_b2);
	buffer[13] = (u16)(_2_ent_b2 >> 16);
	buffer[14] = (u16)(_2_ent_b3);
	buffer[15] = (u16)(_2_ent_b3 >> 16);
}

APL_FRAG rsp_2k_rl_out(VOID)
{
	SM_0XFFFF: RSP16 = RL;
	RSP256 = RSP16;
	RSP2K = RSP256;
	RSP32K = RSP2K;
	NOOP;
	NOOP;
	RSP_END;
};

APL_FRAG  set_rl_frag(SM_REG msk)
{
	msk: RL = 1;
	~msk: RL = 0;
};

APL_FRAG set_gl_frag(VOID)
{
	SM_0XFFFF: GL = RL;
};

APL_FRAG set_ggl_frag(VOID)
{
	SM_0XFFFF: GGL = RL;
};

APL_FRAG get_ggl_frag(VOID)
{
	SM_0XFFFF: RL =  GGL;
};

APL_FRAG get_gl_frag(VOID)
{
	SM_0XFFFF: RL =  GL;
};

void set_rl(u16 val)
{
	apl_set_sm_reg(SM_REG0, val);
	RUN_FRAG_ASYNC(set_rl_frag(msk = SM_REG0));
}

void set_gl(u16 val)
{
	apl_set_sm_reg(SM_REG0, val);
	RUN_FRAG_ASYNC(set_rl_frag(msk = SM_REG0));
	RUN_FRAG_ASYNC(set_gl_frag());
}

void set_ggl(u16 val)
{
	apl_set_sm_reg(SM_REG0, val);
	RUN_FRAG_ASYNC(set_rl_frag(msk = SM_REG0));
	RUN_FRAG_ASYNC(set_ggl_frag());
}

void get_rl(u16 *buffer)
{
	RUN_FRAG_ASYNC(rsp_2k_rl_out());
	read_hbs_entries_from_rsp_fifo(buffer);
}

void get_gl(u16 *buffer)
{
	RUN_FRAG_ASYNC(get_gl_frag());
	RUN_FRAG_ASYNC(rsp_2k_rl_out());
	read_hbs_entries_from_rsp_fifo(buffer);
}

void get_ggl(u16 *buffer)
{
	RUN_FRAG_ASYNC(get_ggl_frag());
	RUN_FRAG_ASYNC(rsp_2k_rl_out());
	read_hbs_entries_from_rsp_fifo(buffer);
}

void gvrc_2to2(enum gvrc_vr16 aa, enum gvrc_vr16 ab, enum gvrc_vr16 at1)
{
	apl_set_rn_reg(RN_REG_0, aa);
	apl_set_rn_reg(RN_REG_1, ab);
	apl_set_rn_reg(RN_REG_2, at1);

	RUN_IMM_FRAG_ASYNC(immf_2to2_imm(RN_REG a = RN_REG_0, RN_REG b = RN_REG_1, RN_REG t1 = RN_REG_2)
	{
		SM_0XFFFF:	RL = SB[a,b];
		//SM_0XFFFF:	RL &= SB[src2];
		{
			SM_0XFFFF:	SB[t1] = RL;
			SM_0XFFFF:	RL = SB[a];
		}
		SM_0XFFFF:	RL ^= SB[b];
		{
			SM_0XFFFF:	SB[a] = RL;
			SM_0XFFFF:	RL = SB[t1];
		}
		SM_0XFFFF:	SB[b] = RL;
	});

}

// input: a, b, c. output: a is the sum and c is the carry.
void gvrc_3to2(	enum gvrc_vr16 aa, enum gvrc_vr16 ab, enum gvrc_vr16 ac, enum gvrc_vr16 at1, enum gvrc_vr16 at2)
{
	apl_set_rn_reg(RN_REG_0, aa);
	apl_set_rn_reg(RN_REG_1, ab);
	apl_set_rn_reg(RN_REG_2, ac);
	apl_set_rn_reg(RN_REG_3, at1);
	apl_set_rn_reg(RN_REG_4, at2);

	RUN_IMM_FRAG_ASYNC(immf_3to2_imm(RN_REG a = RN_REG_0, RN_REG b = RN_REG_1, RN_REG c = RN_REG_2,
									RN_REG aANDb = RN_REG_3, RN_REG aXORb = RN_REG_4 )
	{
		SM_0XFFFF:	RL = SB[a,b];
		{
			SM_0XFFFF:	SB[aANDb] = RL;
			SM_0XFFFF:	RL = SB[a];
		}
		SM_0XFFFF:	RL ^= SB[b];
		{
			SM_0XFFFF:	SB[aXORb] = RL;
			SM_0XFFFF:	RL ^= SB[c];
		}
		{
			SM_0XFFFF:	SB[a] = RL;
			SM_0XFFFF:	RL = SB[aXORb,c];
		}
		SM_0XFFFF:	RL |= SB[aANDb];
		SM_0XFFFF:	SB[c] = RL;
	});

}

APL_FRAG cmn_cpy_16_msk(RN_REG dst, RN_REG src, SM_REG msk)
{
	msk:	RL = SB[src];
	msk:	SB[dst] = RL;
};


void gvrc_cpy_16_msk(enum gvrc_vr16 adst, enum gvrc_vr16 asrc, u16 amsk)
{
	apl_set_rn_reg(RN_REG_0, asrc);
	apl_set_rn_reg(RN_REG_1, adst);
	apl_set_sm_reg(SM_REG0, amsk);

	RUN_FRAG_ASYNC(cmn_cpy_16_msk(dst = RN_REG_1, src = RN_REG_0, msk = SM_REG0));
}

void gvrc_asl1(enum gvrc_vr16 adst, enum gvrc_vr16 asrc)
{
	apl_set_rn_reg(RN_REG_0, adst);
	apl_set_rn_reg(RN_REG_1, asrc);
	RUN_IMM_FRAG_ASYNC(immf_asl1(RN_REG dst = RN_REG_0, RN_REG src = RN_REG_1 )
	{
		SM_0XFFFF:	RL = SB[src];
		//SM_0XFFFF:	RL = NRL; // Wow! Looks like somebody put the lsb at the top/north
		SM_0XFFFF:	SB[dst] = NRL;
	});
}

void gvrc_asr1(enum gvrc_vr16 adst, enum gvrc_vr16 asrc)
{
	apl_set_rn_reg(RN_REG_0, adst);
	apl_set_rn_reg(RN_REG_1, asrc);
	RUN_IMM_FRAG_ASYNC(immf_asr1(RN_REG dst = RN_REG_0, RN_REG src = RN_REG_1 )
	{
		SM_0XFFFF:	RL = SB[src];
		//SM_0XFFFF:	RL = SRL; // Wow! Looks like somebody put the lsb at the top/north
		SM_0XFFFF:	SB[dst] = SRL;
	});
}

void gvrc_asl2(enum gvrc_vr16 adst, enum gvrc_vr16 asrc)
{
	apl_set_rn_reg(RN_REG_0, adst);
	apl_set_rn_reg(RN_REG_1, asrc);
	RUN_IMM_FRAG_ASYNC(immf_asl2(RN_REG dst = RN_REG_0, RN_REG src = RN_REG_1 )
	{
		SM_0XFFFF:	RL = SB[src];
		SM_0XFFFF:	RL = NRL;
		SM_0XFFFF:	SB[dst] = NRL;
	});
}

void gvrc_asr2(enum gvrc_vr16 adst, enum gvrc_vr16 asrc)
{
	apl_set_rn_reg(RN_REG_0, adst);
	apl_set_rn_reg(RN_REG_1, asrc);
	RUN_IMM_FRAG_ASYNC(immf_asr2(RN_REG dst = RN_REG_0, RN_REG src = RN_REG_1 )
	{
		SM_0XFFFF:	RL = SB[src];
		SM_0XFFFF:	RL = SRL;
		SM_0XFFFF:	SB[dst] = SRL;
	});
}

void gvrc_asl4(enum gvrc_vr16 adst, enum gvrc_vr16 asrc)
{
	apl_set_rn_reg(RN_REG_0, adst);
	apl_set_rn_reg(RN_REG_1, asrc);
	RUN_IMM_FRAG_ASYNC(immf_asl4(RN_REG dst = RN_REG_0, RN_REG src = RN_REG_1 )
	{
		SM_0XFFFF:	RL = SB[src];
		SM_0XFFFF:	RL = NRL;
		SM_0XFFFF:	RL = NRL;
		SM_0XFFFF:	RL = NRL;
		SM_0XFFFF:	SB[dst] = NRL;
	});
}

void gvrc_asr4(enum gvrc_vr16 adst, enum gvrc_vr16 asrc)
{
	apl_set_rn_reg(RN_REG_0, adst);
	apl_set_rn_reg(RN_REG_1, asrc);
	RUN_IMM_FRAG_ASYNC(immf_asr4(RN_REG dst = RN_REG_0, RN_REG src = RN_REG_1 )
	{
		SM_0XFFFF:	RL = SB[src];
		SM_0XFFFF:	RL = SRL;
		SM_0XFFFF:	RL = SRL;
		SM_0XFFFF:	RL = SRL;
		SM_0XFFFF:	SB[dst] = SRL;
	});
}

APL_FRAG cmn_iv_shift16_head_g2k(RN_REG mov)
{
		SM_0XFFFF:	RL = SB[mov];
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	RL = ERL;
		SM_0XFFFF:	SB[mov] = RL;
};

void gvrc_copy_hhb_16(enum gvrc_vr16 amov)
{
	apl_set_rn_reg(RN_REG_0, amov);
	for (int i = 0; i < 64; i++) {
		RUN_FRAG_ASYNC(cmn_iv_shift16_head_g2k(mov = RN_REG_0));
	}
}

void gvrc_spread4(	enum gvrc_vr16 adst, enum gvrc_vr16 as1, enum gvrc_vr16 as2, enum gvrc_vr16 as3,
					enum gvrc_vr16 as4, u16 asrc_mask)
{
	apl_set_rn_reg(RN_REG_0, adst);
	apl_set_rn_reg(RN_REG_1, as1);
	apl_set_rn_reg(RN_REG_2, as2);
	apl_set_rn_reg(RN_REG_3, as3);
	apl_set_rn_reg(RN_REG_4, as4);
	apl_set_sm_reg(SM_REG0, asrc_mask);
	RUN_IMM_FRAG_ASYNC(immf_spread4(RN_REG dst = RN_REG_0, RN_REG s1 = RN_REG_1, RN_REG s2 = RN_REG_2,
									RN_REG s3 = RN_REG_3, RN_REG s4 = RN_REG_4, SM_REG src_mask = SM_REG0)
	{
		{
		src_mask: RL = SB[s1];
		src_mask: GL = RL;
		}
		{
		SM_0X0001 << 0: SB[dst] = GL;
		src_mask: RL = SB[s2];
		src_mask: GL = RL;
		}
		{
		SM_0X0001 << 1: SB[dst] = GL;
		src_mask: RL = SB[s3];
		src_mask: GL = RL;
		}
		{
		SM_0X0001 << 2: SB[dst] = GL;
		src_mask: RL = SB[s4];
		src_mask: GL = RL;
		}
		SM_0X0001 << 3: SB[dst] = GL;
	});
}

void gvrc_spread5(	enum gvrc_vr16 adst, enum gvrc_vr16 as1, enum gvrc_vr16 as2, enum gvrc_vr16 as3,
					enum gvrc_vr16 as4, enum gvrc_vr16 as5, u16 asrc_mask)
{
	apl_set_rn_reg(RN_REG_0, adst);
	apl_set_rn_reg(RN_REG_1, as1);
	apl_set_rn_reg(RN_REG_2, as2);
	apl_set_rn_reg(RN_REG_3, as3);
	apl_set_rn_reg(RN_REG_4, as4);
	apl_set_rn_reg(RN_REG_5, as5);
	apl_set_sm_reg(SM_REG0, asrc_mask);
	RUN_IMM_FRAG_ASYNC(immf_spread5(RN_REG dst = RN_REG_0, RN_REG s1 = RN_REG_1, RN_REG s2 = RN_REG_2,
									RN_REG s3 = RN_REG_3, RN_REG s4 = RN_REG_4, RN_REG s5 = RN_REG_5,
									SM_REG src_mask = SM_REG0)
	{
		{
		src_mask: RL = SB[s1];
		src_mask: GL = RL;
		}
		{
		SM_0X0001 << 0: SB[dst] = GL;
		src_mask: RL = SB[s2];
		src_mask: GL = RL;
		}
		{
		SM_0X0001 << 1: SB[dst] = GL;
		src_mask: RL = SB[s3];
		src_mask: GL = RL;
		}
		{
		SM_0X0001 << 2: SB[dst] = GL;
		src_mask: RL = SB[s4];
		src_mask: GL = RL;
		}
		{
		SM_0X0001 << 3: SB[dst] = GL;
		src_mask: RL = SB[s5];
		src_mask: GL = RL;
		}
		SM_0X0001 << 4: SB[dst] = GL;
	});
}

APL_FRAG cmn_cpy_imm_16_mrk(RN_REG dst, RN_REG mrk_vreg, SM_REG mrk, SM_REG val)
{
	{
	mrk:	RL = SB[mrk_vreg];
	mrk:	GL = RL;
	}
	{
	val:	RL = ~SB[dst] & INV_GL;
	~val:	RL = SB[dst] & INV_GL;
	}
	{
	val:	SB[dst] = INV_RL;
	~val:	SB[dst] = RL;
	}
};

void gvrc_cpy_imm_16_mrk(enum gvml_mrks_n_flgs vreg, u16 val, u16 mrk)
{
	apl_set_sm_reg(RN_REG_0, vreg);
	apl_set_sm_reg(SM_REG_0, val);
	apl_set_sm_reg(SM_REG_1, mrk);
	RUN_FRAG_ASYNC(cmn_cpy_imm_16_mrk(dst = RN_REG_0, mrk_vreg = RN_REG_FLAGS, mrk = SM_REG_1, val = SM_REG_0));
}

APL_FRAG eq_imm_to_flag(RN_REG x, SM_REG imm_val, RN_REG vr_flags, SM_REG flag)
{
	SM_0XFFFF:	RL = SB[x];
	{
		~imm_val:	RL = INV_RL;
		SM_0XFFFF:	GL = RL;
	}
	flag:	SB[vr_flags] = GL;
};

void gvrc_eq_imm_16(enum gvml_mrks_n_flgs res_mrk, enum gvrc_vr16 ax, u16 aimm_val)
{
	apl_set_rn_reg(RN_REG_0, ax);
	apl_set_sm_reg(SM_REG_0, res_mrk);
	apl_set_sm_reg(SM_REG_1, aimm_val);

	RUN_FRAG_ASYNC(eq_imm_to_flag(x = RN_REG_0, imm_val = SM_REG_1, vr_flags = RN_REG_FLAGS, flag = SM_REG_0));
}


APL_FRAG io_frag_std_m4_idx_2_std_m1_idx(RN_REG dst, RN_REG src)
{
	{
		(SM_0XFFFF << 0):	RL = SB[src];
		(SM_0X0001 << 0):	GL = RL;
	}{
		(SM_0X0001 << 12):	RL = GL;
		~(SM_0XFFFF << 12):	RL = SRL;
		(SM_0X0001 << 0):	GL = RL;
	}{
		(SM_0XFFFF << 13):	SB[dst] = RL;
		(SM_0X0001 << 12):	SB[dst] = GL;
		~(SM_0XFFFF << 12):	SB[dst] = SRL;
	}
};


void gvrc_calc_index_m1_16(enum gvrc_vr16 dst)
{
	apl_set_rn_reg(RN_REG_0, dst);
	apl_set_rn_reg(RN_REG_1, GVRC_VR16_M1);
	//RUN_FRAG_ASYNC(io_frag_std_m4_idx_2_std_m1_idx(dst = RN_REG_0, src = RN_REG_1));
	RUN_FRAG_ASYNC(cmn_cpy_16_msk(dst = RN_REG_0, src = RN_REG_1, msk = SM_0XFFFF));
}

APL_FRAG cmn_reset_16_msk(RN_REG vreg, SM_REG msk)
{
	msk:	SB[vreg] = RSP16;
};


void gvrc_m_reset(enum gvml_mrks_n_flgs mrk_to_reset)
{
	apl_set_sm_reg(SM_REG_0, mrk_to_reset);

	RUN_FRAG_ASYNC(cmn_reset_16_msk(vreg = RN_REG_FLAGS, msk = SM_REG_0));
}

APL_FRAG cmn_cpy_bits_16(RN_REG dst, SM_REG dst_msk, RN_REG src, SM_REG src_msk)
{
	{
		src_msk:	RL = SB[src];
		src_msk:	GL = RL;
	}
	dst_msk:	SB[dst] = GL;
};


void gvrc_m_cpy(enum gvml_mrks_n_flgs dst_mrk, enum gvml_mrks_n_flgs src_mrk)
{
	apl_set_sm_reg(SM_REG_0, dst_mrk);
	apl_set_sm_reg(SM_REG_1, src_mrk);

	RUN_FRAG_ASYNC(cmn_cpy_bits_16(dst = RN_REG_FLAGS, dst_msk = SM_REG_0, src = RN_REG_FLAGS,  src_msk = SM_REG_1));
}

static void set_rl_by_mrk(enum gvrc_vr16 a_data_vr, enum gvml_mrks_n_flgs a_imrk)
{
	apl_set_rn_reg(RN_REG_0, a_data_vr);
	apl_set_sm_reg(SM_REG_0, a_imrk);

	RUN_IMM_FRAG_ASYNC(set_rl_by_mrk_imm(RN_REG mrk_reg = RN_REG_FLAGS, SM_REG mrk = SM_REG_0, RN_REG data = RN_REG_0)
	{
		{
			mrk:	RL = SB[mrk_reg];
			mrk:	GL = RL;
		}
		SM_0XFFFF:	RL = SB[data] & GL;
	});

}


void gvrc_iv_extract_u16_mrk_g32k(u16 *out_val, enum gvrc_vr16 data_vr, enum gvml_mrks_n_flgs imrk)
{
	u16 hb_data[16];
	set_rl_by_mrk(data_vr, imrk);
	RUN_FRAG_ASYNC(rsp_2k_rl_out());
	read_hbs_entries_from_rsp_fifo(hb_data);
	for (int hb = 0; hb < 16; hb++) {
		if (hb_data[hb] != 0) {
			*out_val = hb_data[hb];
			return;
		}
	}
	*out_val = 0;
	return;

}

void gvrc_iv_extract_u16_mrk_g2k(u16 *out_buf, enum gvrc_vr16 data_vr, enum gvml_mrks_n_flgs mrk_in)
{
	set_rl_by_mrk(data_vr, mrk_in);
	RUN_FRAG_ASYNC(rsp_2k_rl_out());
	read_hbs_entries_from_rsp_fifo(out_buf);
	return;
}

void gvrc_iv_test_for_one_true_g32k(u16 *pbfound, enum gvml_mrks_n_flgs amrk)
{

	apl_set_sm_reg(SM_REG_0, amrk);

	RUN_IMM_FRAG_ASYNC(immf_rsp_one_true(RN_REG vr_flags = RN_REG_FLAGS, SM_REG mrk = SM_REG_0)
	{
		mrk:	RL = SB[vr_flags];
		mrk: RSP16 = RL;
		RSP256 = RSP16;
		RSP2K = RSP256;
		RSP32K = RSP2K;
		NOOP;
		NOOP;
		RSP_END;
	});

	u32 _2_ent = 0;

	// taking from each half bank in the apc his rsp 2k val 
	// Code here is written assuming most likely no true
	// otherwise, it would be better to test after each read and abort if true
	*pbfound = 0;

//	for (apc_id = num_apcs - 1, msk = 0; apc_id >= 0; apc_id--) {
//		apuc_rsp_rd(apc_id);
//		msk = (u16)((msk << GSI_MMB_NUM_BANKS * GSI_APC_NUM_HALF_BANKS) | apuc_rd_rsp32k_reg());
//	}
	apl_rsp_rd(0); // apc_id=0
	unsigned char bmsk = apl_rd_rsp32k_reg();
	*pbfound = (u16)bmsk; 
	apl_rsp_rd(1); // apc_id=1
	bmsk = apl_rd_rsp32k_reg();
	//if (bmsk != 0) {
	//	*pbfound = 1;
	//}
	*pbfound |= ((u16)bmsk) << 8; 
	return;

	apl_rsp_rd(0); // apc_id=0
	_2_ent |= apl_rd_rsp2k_reg(0);
	_2_ent |= apl_rd_rsp2k_reg(1);
	_2_ent |= apl_rd_rsp2k_reg(2);
	_2_ent |= apl_rd_rsp2k_reg(3);

	apl_rsp_rd(1); // apc_id=1
	_2_ent |= apl_rd_rsp2k_reg(0);
	_2_ent |= apl_rd_rsp2k_reg(1);
	_2_ent |= apl_rd_rsp2k_reg(2);
	_2_ent |= apl_rd_rsp2k_reg(3);
	if (_2_ent != 0) {
		*pbfound = 1;
	}
	return;

}

APL_FRAG cmn_cpy_imm_16(RN_REG vreg, SM_REG val)
{
	{
	val:	SB[vreg] = INV_RSP16;
	~val:	SB[vreg] = RSP16;
	}
};


void __gvml_cpy_imm_16(enum gvrc_vr16 vreg, u16 val)
{
	apl_set_rn_reg(RN_REG_0, vreg);
	apl_set_sm_reg(SM_REG_0, val);

	RUN_FRAG_ASYNC(cmn_cpy_imm_16(vreg = RN_REG_0, val = SM_REG_0));

}

APL_FRAG create_rl_mrk_and_rst_dst(RN_REG dst)
{
	{
		SM_0XFFFF:	RL = 1;
		SM_0XFFFF:	SB[dst] = RSP16;
	}{
		SM_0XFFFF:	RL ^= WRL;
	}
};

APL_FRAG set_4idx_and_shift_rl_mrk(RN_REG dst, SM_REG idx)
{
	{
		(idx << 2):		SB[dst] ?= RL;
	}{
		SM_0XFFFF:		RL = WRL;
	}{
		(idx << 2):		SB[dst] ?= RL;
		(SM_0X0001 << 0):	SB[dst] ?= RL;
	}{
		SM_0XFFFF:		RL = WRL;
	}{
		(idx << 2):		SB[dst] ?= RL;
		(SM_0X0001 << 1):	SB[dst] ?= RL;
	}{
		SM_0XFFFF:		RL = WRL;
	}{
		(idx << 2):		SB[dst] ?= RL;
		(SM_0X0001 << 0):	SB[dst] ?= RL;
		(SM_0X0001 << 1):	SB[dst] ?= RL;
	}{
		SM_0XFFFF:		RL = WRL;
	}

};

void gvrc_create_index_m1_g2k_t0(enum gvrc_vr16 dst)
{
	u16 idx;

	apl_set_rn_reg(RN_REG_G0, dst);
	RUN_FRAG_ASYNC(create_rl_mrk_and_rst_dst(dst = RN_REG_G0));
	for (idx = 0; idx < 512; idx++) {
		apl_set_sm_reg(SM_REG0, idx);
		RUN_FRAG_ASYNC(set_4idx_and_shift_rl_mrk(dst = RN_REG_G0, idx = SM_REG0));
	}
}

APL_FRAG noop_apl(VOID)
{
	NOOP;
};

APL_FRAG cmn_set_16_msk(RN_REG vreg, SM_REG msk)
{
	msk:	SB[vreg] = INV_RSP16;
};

void gvrc_set_16_msk(enum gvrc_vr16 avreg, u16 amsk)
{
	apl_set_rn_reg(RN_REG_0, avreg);
	apl_set_sm_reg(SM_REG_0, amsk);

	RUN_FRAG_ASYNC(cmn_set_16_msk(vreg = RN_REG_0, msk = SM_REG_0));
}

APL_FRAG cmn_or_bits_16(RN_REG res_vr, SM_REG res_mrk,
                        RN_REG x_vr, SM_REG x_mrk,
                        RN_REG y_vr, SM_REG y_mrk)
{
	{
		y_mrk: RL = ~SB[y_vr] & INV_RSP16;
		x_mrk: RL = ~SB[x_vr] & INV_RSP16;
		x_mrk: GL = RL;
		y_mrk: GL = RL;
	}
	res_mrk: SB[res_vr] = INV_GL;
};


void gvrc_m_or(enum gvml_mrks_n_flgs ares, enum gvml_mrks_n_flgs ax, enum gvml_mrks_n_flgs ay)
{
	apl_set_sm_reg(SM_REG0, ares);
	apl_set_sm_reg(SM_REG1, ax);
	apl_set_sm_reg(SM_REG2, ay);

	RUN_FRAG_ASYNC(cmn_or_bits_16(res_vr = RN_REG_FLAGS, res_mrk = SM_REG0,
				      x_vr = RN_REG_FLAGS, x_mrk = SM_REG1,
				      y_vr = RN_REG_FLAGS, y_mrk = SM_REG2));
}

/* copy 16 bit vector register: dst = src */
void gvrc_cpy_16(enum gvrc_vr16 dst, enum gvrc_vr16 src)
{
	apl_set_rn_reg(RN_REG_G0, src);
	apl_set_rn_reg(RN_REG_G1, dst);

	RUN_FRAG_ASYNC(cmn_cpy_16_msk(dst = RN_REG_G1, src = RN_REG_G0, msk = SM_0XFFFF));
}

static void mnfc_set_flags_t0(u16 flags_msk)
{
	apl_set_sm_reg(SM_REG0, flags_msk);

	RUN_FRAG_ASYNC(cmn_set_16_msk(vreg = RN_REG_FLAGS, msk = SM_REG0));
}


static void mnfc_cpy_flags_t0(u16 dst_mrk, u16 src_mrk)
{
	apl_set_sm_reg(SM_REG0, dst_mrk);
	apl_set_sm_reg(SM_REG1, src_mrk);

	RUN_FRAG_ASYNC(cmn_cpy_bits_16(dst = RN_REG_FLAGS, dst_msk = SM_REG0, src = RN_REG_FLAGS,  src_msk = SM_REG1));
}

/*
 * converts nibble located on nibble_bit of nibble_vr into max nibble mask
 * e.g.  nibble 0x0 -> nibble_mask 0x0001, nibble 0x4 -> nibble_mask 0x001F, nibble 0xF -> nibble_mask 0xFFFF
 * the nibble_mask output is locatet on RL.
 * For entries with mrk16 == 0x0000 the nibble_mask is set to minumum 0x0001.
 */
APL_FRAG from_nibble_to_nibble_mask_max(RN_REG mrk16, RN_REG nibble_vr, SM_REG nibble_bit, RN_REG tmp, SM_REG sm0x0707, SM_REG sm0x5555)
{
	{	// Load nibble (b0..b3) & mrk16 to RL and set GL = b3
		SM_0XFFFF: RL = SB[nibble_vr, mrk16];
		nibble_bit << 3: GL = RL;
	}
	{	// Set tmp[1..15] = b3 and tmp[0] = 1
		SM_0XFFFF << 1: SB[tmp] = GL;
		SM_0X0001: SB[tmp] = INV_NRL; // tmp[0] = 1
		//set GL = b0
		nibble_bit: GL = RL;
	}
	{	// Set RL[b3] = b2 (Just to keep it there until we use it)
		nibble_bit << 3: RL = NRL;
		// Set RL[0X5555] = b0
		sm0x5555: RL = GL;
		//set GL = b1
		nibble_bit << 1: GL = RL;
	}
	{	// Set RL[SM_0X1111] = b1 | b0
		SM_0X1111: RL |= GL;
		// Set RL[SM_0X2222] = b1
		SM_0X1111 << 1: RL = GL;
		// Set RL[SM_0X4444] = b1 & b0
		SM_0X1111 << 2: RL &= GL;
		//set GL = b2 (which is kept on b3 location)
		nibble_bit << 3: GL = RL;
	}
	{	// Set RL[SM_0X0101] = b2 | (b1 | b0)
		// Set RL[SM_0X0202] = b2 | b1
		// Set RL[SM_0X0404] = b2 | (b1 & b0)
		sm0x0707: RL |= GL;
		// Set RL[SM_0X0808] = b2
		SM_0X0101 << 3: RL = GL;
		// Set RL[SM_0X1010] = b2 & (b1 | b0)
		// Set RL[SM_0X2020] = b2 & b1
		// Set RL[SM_0X4040] = b2 & (b1 & b0)
		sm0x0707 << 4: RL &= GL;
	}

	{	// Set RL[SM_0X0001] = 1
		// Set RL[SM_0X0002] = b3 | (b2 | (b1 | b0))
		// Set RL[SM_0X0004] = b3 | (b2 | b1)
		// Set RL[SM_0X0008] = b3 | (b2 | (b1 & b0))
		// Set RL[SM_0X0010] = b3 | b2
		// Set RL[SM_0X0020] = b3 | (b2 & (b1 | b0))
		// Set RL[SM_0X0040] = b3 | (b2 & b1)
		// Set RL[SM_0X0080] = b3 | (b2 & (b1 & b0))
		~(SM_0XFFFF << 8): RL = SB[tmp] | NRL;
		// Set RL[SM_0X0100] = b3
		SM_0XFFFF << 9: RL = SB[tmp] & NRL;
		// Set RL[SM_0X0200] = b3 & (b2 | (b1 | b0))
		// Set RL[SM_0X0400] = b3 & (b2 | b1))
		// Set RL[SM_0X0800] = b3 & (b2 | (b1 & b0))
		// Set RL[SM_0X1000] = b3 & b2
		// Set RL[SM_0X2000] = b3 & (b2 & (b1 | b0))
		// Set RL[SM_0X4000] = b3 & (b2 & b1)
		// Set RL[SM_0X8000] = b3 & (b2 & (b1 & b0))
		SM_0X0001 << 8: RL = SB[tmp];
	}
};

/*
 * converts nibble located on nibble_bit of nibble_vr into nibble mask while processing half-bank RSP.
 * Than check the RSP results with the nibble mask and update mrk16.
 * Input for RSP loacted on RL.
 * the nibble_mask output is locatet on RL for next iteration.
 */
APL_FRAG from_nibble_to_nibble_mask_max_with_hb_rsp(RN_REG mrk16, RN_REG nibble_vr, SM_REG nibble_bit, RN_REG tmp0, RN_REG tmp1, SM_REG sm0x0707, SM_REG sm0x5555)
{
	{
		SM_0XFFFF: RL = SB[nibble_vr];
		nibble_bit << 3: GL = RL;
		SM_0XFFFF: SB[tmp0] = RL;

		// Start RSP output - RSP16
		SM_0XFFFF: RSP16 = RL;
	}
	{
		SM_0XFFFF << 1: SB[tmp1] = GL;
		SM_0X0001: SB[tmp1] = INV_NRL; // SB[tmp1] = 1
		nibble_bit: GL = RL;

		// Continue RSP output - RSP256
		RSP256 = RSP16;
	}
	{
		nibble_bit << 3: RL = NRL;
		sm0x5555: RL = GL;
		nibble_bit << 1: GL = RL;

		// Continue RSP output - RSP2K
		RSP2K = RSP256;
	}
	{
		SM_0X1111: RL |= GL;
		SM_0X1111 << 1: RL = GL;
		SM_0X1111 << 2: RL &= GL;
		nibble_bit << 3: GL = RL;
	}

	{
		sm0x0707: RL |= GL;
		SM_0X0101 << 3: RL = GL;
		sm0x0707 << 4: RL &= GL;

		// Start RSP input
		RSP_START_RET;
	}

	{
		~(SM_0XFFFF << 8): RL = SB[tmp1] | NRL;
		SM_0XFFFF << 9: RL = SB[tmp1] & NRL;
		SM_0X0001 << 8: RL = SB[tmp1];

		// Continue RSP input - RSP256
		RSP256 = RSP2K;
	}
	{
		// keep the nibble mask
		SM_0XFFFF   : SB[tmp1] = RL;

		// Continue RSP input - RSP16
		RSP16 = RSP256;
	}

	{	// get nibble_masks from RSP16
		SM_0XFFFF: RL = SB[tmp0, mrk16] ^ INV_RSP16;
		SM_0XFFFF: GL = RL;
	}
	{
		RSP_END;
		SM_0XFFFF: SB[mrk16] = GL;
		SM_0X0001: RL = SB[tmp1];
		~SM_0X0001: RL = SB[tmp1] & GL;
	}
	END_RSP16_WA
	/*
	*/
};


APL_FRAG hb_rsp_and_update_mrk(RN_REG mrk16, RN_REG mrk_vr, SM_REG mrk)
{
	SM_0XFFFF: RSP16 = RL;
	RSP256 = RSP16;
	RSP2K = RSP256;
	RSP_START_RET;
	RSP256 = RSP2K;
	{
		SM_0XFFFF: RL &= SB[mrk16];
		RSP16 = RSP256;
	}
	{	// get nibble_masks from RSP16
		SM_0XFFFF        : RL ^= INV_RSP16;
		SM_0XFFFF: GL = RL;
	}
	{
		RSP_END;
		mrk: SB[mrk_vr] = GL;
	}

};



static void mark_minmax_u16_hb_t4(	enum gvrc_vr16 mrk_vr, u16 mrk, enum gvrc_vr16 data, int min, int num_bits,
									enum gvrc_vr16 t0, enum gvrc_vr16 t1, enum gvrc_vr16 t2, enum gvrc_vr16 t3)
{
	//GSI_DEBUG_ASSERT(num_bits > 0 && num_bits <= 16);
	int cur_bit = (num_bits - 1)  & ~3;
	u16 bits_mask = (u16)((1 << num_bits) - 1);
	u16 min_msk;

	if (min)
		min_msk = 0x0000;
	else
		min_msk = 0xFFFF;

	apl_set_sm_reg(SM_REG_0, min_msk);
	apl_set_sm_reg(SM_REG_1, mrk);
	apl_set_sm_reg(SM_REG_2, bits_mask);
	apl_set_rn_reg(RN_REG_0, data);
	apl_set_rn_reg(RN_REG_1, mrk_vr);
	apl_set_rn_reg(RN_REG_2, t0);
	apl_set_rn_reg(RN_REG_3, t1);
	apl_set_rn_reg(RN_REG_4, t2);
	apl_set_rn_reg(RN_REG_5, t3);

	/* Init 16b marker VR and the min_max_val VR */
	RUN_IMM_FRAG_ASYNC(mark_minmax_u16_hb_t4_init(RN_REG mrk16 = RN_REG_2, RN_REG mrk_vr = RN_REG_1,
							     SM_REG mrk = SM_REG_1, RN_REG tmp_data = RN_REG_5,
							     RN_REG data = RN_REG_0, SM_REG min_msk = SM_REG_0,
							     SM_REG msb_msk = SM_REG_2)
	{
		{
			msb_msk: RL = SB[data];
			~msb_msk: RL = 0;
		}
		{	// Init tmp_data
			min_msk: SB[tmp_data] = RL;
			~min_msk: SB[tmp_data] = INV_RL;

			// Init 16b marker vr
			mrk: RL = SB[mrk_vr];
			mrk: GL = RL;
		}
		SM_0XFFFF: SB[mrk16] = GL;
	});


	/* Calc nibble_mask of msb nibble */
	apl_set_sm_reg(SM_REG_0, 1 << cur_bit);
	apl_set_sm_reg(SM_REG_3, 0x0707);
	apl_set_sm_reg(SM_REG_11, 0x5555);
	RUN_FRAG_ASYNC(from_nibble_to_nibble_mask_max(	mrk16 = RN_REG_2, nibble_vr = RN_REG_5,
													nibble_bit = SM_REG_0, tmp = RN_REG_3,
													sm0x0707 = SM_REG_3, sm0x5555 = SM_REG_11));

	for (cur_bit -= 4; cur_bit >= 0; cur_bit -= 4) {
		/* Calc nibble_mask of next nibble while process half-bank RSP for the current nibble mask */
		apl_set_sm_reg(SM_REG_0, 1 << cur_bit);
		RUN_FRAG_ASYNC( from_nibble_to_nibble_mask_max_with_hb_rsp(	mrk16 = RN_REG_2, nibble_vr = RN_REG_5,
																	nibble_bit = SM_REG0, tmp0 = RN_REG_3,
																	tmp1 = RN_REG_4, sm0x0707 = SM_REG_3,
																	sm0x5555 = SM_REG_11));
	}

	/* Process half-bank RSP for the last nibble mask (lsb nibble) and update output marker */
	RUN_FRAG_ASYNC(hb_rsp_and_update_mrk(mrk16 = RN_REG_2, mrk_vr = RN_REG_G1, mrk = SM_REG_1));
}

void gvrc_iv_mark_min_u16_m1_g2k(enum gvml_mrks_n_flgs mrk_out, enum gvrc_vr16 vr_in,
								enum gvrc_vr16 t0, enum gvrc_vr16 t1, enum gvrc_vr16 t2, enum gvrc_vr16 t3)
{
	//u16 mrk_in = 1 << SM_BIT_12;

	mnfc_set_flags_t0(mrk_out);
	//mark_minmax_u16_t4(mrk_in, (enum gvrc_vr16)(vr_in), true);
	mark_minmax_u16_hb_t4(GVRC_VR16_FLAGS, mrk_out, vr_in, true, 16, t0, t1, t2, t3);
	//mnfc_cpy_flags_t0(mrk_out, mrk_in);
	//eq_imm_16_mrk_t0(mrk, data, minmax, mrk);
}

void gvrc_iv_brdcst_mrk_data_g2k(enum gvrc_vr16 vr_dst, enum gvrc_vr16 vr_src, enum gvml_mrks_n_flgs m_mrk)
{
	apl_set_rn_reg(RN_REG_0, vr_dst);
	apl_set_rn_reg(RN_REG_1, vr_src);
	apl_set_sm_reg(SM_REG_0, m_mrk);
	RUN_IMM_FRAG_ASYNC(brdcst_mrk_data_g2k_immf(RN_REG dst = RN_REG_0, RN_REG src = RN_REG_1,
							     				SM_REG mrk = SM_REG_0, RN_REG vr_flags = RN_REG_FLAGS)
	{
		{
			mrk:	RL = SB[vr_flags];
			mrk:	GL = RL;
		}
		SM_0XFFFF:	RL = SB[src] & GL;
		SM_0XFFFF: RSP16 = RL;
		RSP256 = RSP16;
		RSP2K = RSP256;
		RSP_START_RET;
		RSP256 = RSP2K;
		RSP16 = RSP256;
		NOOP;
		{
			RSP_END;
			SM_0XFFFF: SB[dst] = RSP16;
		}
	});
}

/*
 * l2_ready conventions
 * ====================
 *
 * 1) Mechanics
 *
 * When l2_ready=0 the MMB has exclusive access to L2, otherwise
 * when l2_ready=1 the L2DMA has exclusive access to L2.
 *
 * Explicit set/rst can be done via the L2DMA L2_READY_SR command, or
 * using L2_END to set L2_READY via the MMB to serialize I/O, e.g. after L1 -> L2 copy.
 *
 * Implicit set/rst can be done via the l2_ready_attr for RDL2 and WRL2, or
 * using L2_END following L1 <-> L2 I/O.
 *
 * 2) State changes
 *
 * l2_ready default state is 1.
 * Note: explicitly set on init.
 *
 * Copying data from system memory in to l2 and then to l1:
 * - l2dma from sys_mem to l2
 *   - can assume l2_ready=1 and start unconditionally
 *   - must reset l2_ready before copying from l2 to l1
 * - copying from l2 to l1
 *   - must set l2_ready=1 using l2_end() when done
 *
 * Copying data from l1 via l2 out to system memory:
 * - High level functions are responsible to reset l2_ready (e.g. using apuc_l2dma_l2_ready_rst_all)
 * - copying from l1 to l2
 *   - must set l2_ready=1 using l2_end() when done
 * - l2dma from l2 to sys_mem
 *   - l2dma blocks until l2_ready=1
 *   - keeps l2_ready set when done
 *
 * Low-level functions must be instructed by caller to perform implicit set/rst
 *
 * 3) Notes
 *
 * Assuming l2_ready default state is 1
 *
 * l2dma operations accessing l2, (MC)RDL2 and (MC)WRL2, wait until l2_ready=1, hence it is sw's responsibility
 * to make sure l2_ready is set either explicitly or implicitly post L1<->L2 I/O with L2_END before initiating the L2DMA command.
 * If a L1<->L2 transfer might still be in flight, explicit L2_END fragment should be executed to make sure l2_ready setting is
 * serialized with the background I/O.
 *
 * Conversely, L1<->L2 I/O, cannot block until l2_ready=0, so sw must make sure to initiate such I/O only after
 * ensuring l2_ready=0, either implicitly post RDL2/WRL2, explicitly using L2_READY_SR, and by waiting on l2dma_done/GSI_APUC_COND_L2_READY.
 */

APL_FRAG l2_end(VOID)
{
	L2_END;
};

APL_FRAG copy_l2_to_l1_byte(L1_REG dst, L1_REG parity_dst, L2_REG src)
{
		LGL = src + 0;
	{
		dst + 0 = LGL;
		LGL = src + 1;
	}
	{
		dst + 1 = LGL;
		LGL = src + 2;
	}
	{
		dst + 2 = LGL;
		LGL = src + 3;
	}
	{
		dst + 3 = LGL;
		LGL = src + 4;
	}
	{
		dst + 1,0 = LGL;
		LGL = src + 5;
	}
	{
		dst + 1,1 = LGL;
		LGL = src + 6;
	}
	{
		dst + 1,2 = LGL;
		LGL = src + 7;
	}
	{
		dst + 1,3 = LGL;
		LGL = src + 8;
	}
		parity_dst = LGL;
};


void gvrc_copy_N_bytes_l2_to_l1(u8 l1_bank_id, enum gvml_vm_reg vm_reg, uint l1_grp, int num_bytes, bool l2_ready_set, u8 l2_start_byte)
{
	int i;
	uint parity_set, row_in_set;
	uint l1_parity_grp, l1_parity_row;
	uint l1_grp_row = _vm_reg_to_set_ext(vm_reg, &parity_set, &row_in_set, &l1_parity_grp, &l1_parity_row);
	u8 l2_addr;

//	gsi_log("in gvrc_copy_N_bytes_l2_to_l1\n");
//	gsi_debug(1, "l1_bank_id=%u vm_reg=%u l1_grp=%u num_bytes=%u l2_ready_set=%u",
//	          l1_bank_id, vm_reg, l1_grp, num_bytes, l2_ready_set);
	for (i = 0; i < num_bytes; i++, l1_grp += 2, l1_parity_grp += 2) {
		if (l1_grp >= GSI_L1_NUM_GRPS) {
			l1_grp = 0;
			vm_reg++;
			l1_grp_row = _vm_reg_to_set_ext(vm_reg, &parity_set, &row_in_set, &l1_parity_grp, &l1_parity_row);
		}
		l2_addr = gsi_encode_l2_addr(l2_start_byte + i, 0);
//		gsi_debug(2, "l1_bank_id=%u vm_reg=%u l1_grp=%u l1_grp_row=%u l1_parity_grp=%u l1_parity_row=%u l2_addr=0x%02x",
//		          l1_bank_id, vm_reg, l1_grp, l1_grp_row, l1_parity_grp, l1_parity_row, l2_addr);
		apl_set_l1_reg_ext(L1_ADDR_REG0, l1_bank_id, l1_grp, l1_grp_row);
		apl_set_l1_reg_ext(L1_ADDR_REG1, l1_bank_id, l1_parity_grp, l1_parity_row);
		apl_set_l2_reg(L2_ADDR_REG0, l2_addr);
		RUN_FRAG_ASYNC(copy_l2_to_l1_byte(dst = L1_ADDR_REG0, parity_dst = L1_ADDR_REG1, src = L2_ADDR_REG0));
	}

	if (l2_ready_set)
		RUN_FRAG_ASYNC(l2_end());
}

APL_FRAG copy_l1_to_l2_byte(L2_REG dst, L1_REG src, L1_REG parity_src)
{
		LGL = src + 0;
	{
		dst + 0 = LGL;
		LGL = src + 1;
	}
	{
		dst + 1 = LGL;
		LGL = src + 2;
	}
	{
		dst + 2 = LGL;
		LGL = src + 3;
	}
	{
		dst + 3 = LGL;
		LGL = src + 1,0;
	}
	{
		dst + 4 = LGL;
		LGL = src + 1,1;
	}
	{
		dst + 5 = LGL;
		LGL = src + 1,2;
	}
	{
		dst + 6 = LGL;
		LGL = src + 1,3;
	}
	{
		dst + 7 = LGL;
		LGL = parity_src;
	}
		dst + 8 = LGL;
};

void gvrc_copy_N_bytes_l1_to_l2(u8 l1_bank_id, enum gvml_vm_reg vm_reg, uint l1_grp, int num_bytes, bool l2_ready_set, u8 l2_start_byte)
{
	int i;
	uint parity_set, row_in_set;
	uint l1_parity_grp, l1_parity_row;
	uint l1_grp_row = _vm_reg_to_set_ext(vm_reg, &parity_set, &row_in_set, &l1_parity_grp, &l1_parity_row);
	u8 l2_addr;

//	gsi_debug(1, "l1_bank_id=%u vm_reg=%u l1_grp=%u num_bytes=%u l2_ready_set=%u",
//	          l1_bank_id, vm_reg, l1_grp, num_bytes, l2_ready_set);
	for (i = 0; i < num_bytes; i++, l1_grp += 2, l1_parity_grp += 2) {
		if (l1_grp >= GSI_L1_NUM_GRPS) {
			l1_grp = 0;
			vm_reg++;
			l1_grp_row = _vm_reg_to_set_ext(vm_reg, &parity_set, &row_in_set, &l1_parity_grp, &l1_parity_row);
		}
		l2_addr = gsi_encode_l2_addr(l2_start_byte + i, 0);
//		gsi_debug(2, "l1_bank_id=%u vm_reg=%u l1_grp=%u l1_grp_row=%u l1_parity_grp=%u l1_parity_row=%u l2_addr=0x%02x",
//		          l1_bank_id, vm_reg, l1_grp, l1_grp_row, l1_parity_grp, l1_parity_row, l2_addr);
		apl_set_l1_reg_ext(L1_ADDR_REG0, l1_bank_id, l1_grp, l1_grp_row);
		apl_set_l1_reg_ext(L1_ADDR_REG1, l1_bank_id, l1_parity_grp, l1_parity_row);
		apl_set_l2_reg(L2_ADDR_REG0, l2_addr);
		RUN_FRAG_ASYNC(copy_l1_to_l2_byte(dst = L2_ADDR_REG0, src = L1_ADDR_REG0, parity_src = L1_ADDR_REG1));
	}

	if (l2_ready_set)
		RUN_FRAG_ASYNC(l2_end());
}


static u16 load_16_parity_mask(uint parity_grp)
{
	//GSI_DEBUG_ASSERT(parity_grp <= 1);
	return !parity_grp ? 0x0808 : 0x1010;	/* 0x0808 << parity_grp */
}

/*
 * Load 16 bits from L1 @src into @dst and verify parity stored in L1 @parity_src
 * Return verified parity in SB[@flags]:@parity_flag (0=OK, 1=Error)
 */
APL_FRAG load_16(RN_REG dst, L1_REG src, L1_REG parity_src, SM_REG parity_mask2, RN_REG flags, SM_REG parity_flag)
{
	{
		parity_flag:		RL = SB[flags];
		parity_flag:		GL = RL;
		GGL = src + 0;
	}
	{
		(SM_0X1111 << 0):	SB[dst] = GGL;
		(SM_0X3333 << 3):	RL = GGL;
		(SM_0X0001 << 1):	RL = INV_GL;	// ~old_parity
		GGL = src + 1;
	}
	{
		(SM_0X1111 << 1):	SB[dst] = GGL;
		(SM_0X3333 << 3):	RL ^= GGL;
		GGL = src + 2;
	}
	{
		(SM_0X1111 << 2):	SB[dst] = GGL;
		(SM_0X3333 << 3):	RL ^= GGL;
		GGL = src + 3;
	}
	{
		(SM_0X1111 << 3):	SB[dst] = GGL;
		(SM_0X3333 << 3):	RL ^= GGL;
		GGL = parity_src;
	}
	{
		parity_mask2:		RL ^= GGL;
	}
	{
		/* use GL to compute new_parity = ~(~p0 & ~p1 & ~old_parity) */
		(SM_0X0101 << 3):	RL ^= INV_SRL;
		(SM_0X0101 << 4):	RL ^= INV_NRL;
		parity_mask2:		GL = RL;
		(SM_0X0001 << 1):	GL = RL;
	}
	parity_flag:		SB[flags] = INV_GL;
};

void gvrc_io_load_16(enum gvrc_vr16 dst, enum gvml_vm_reg vm_reg)
{
	uint parity_set, row_in_set, l1_parity_grp, l1_parity_row;
//	u16 l1_addr = gal_vm_reg_to_set_ext(vm_reg, &parity_set, &row_in_set, &l1_parity_grp, &l1_parity_row);
	u16 l1_addr = _vm_reg_to_set_ext(vm_reg, &parity_set, &row_in_set, &l1_parity_grp, &l1_parity_row);
	u16 parity_mask = load_16_parity_mask(l1_parity_grp);

//	gsi_debug(2, "dst=%u vm_reg=%u: l1_addr=0x%x parity_set=%u row_in_set=%u l1_parity_grp=%u parity_mask=0x%04x l1_parity_row=%u",
//	          dst, vm_reg, l1_addr, parity_set, row_in_set, l1_parity_grp, parity_mask, l1_parity_row);

	apl_set_rn_reg(RN_REG_G0, dst);
	apl_set_l1_reg(L1_ADDR_REG0, l1_addr);
	apl_set_l1_reg(L1_ADDR_REG1, l1_parity_row);
	apl_set_sm_reg(SM_REG0, parity_mask);
	apl_set_sm_reg(SM_REG1, (u16)(1 << PE_FLAG));

	RUN_FRAG_ASYNC(load_16(dst = RN_REG_G0, src = L1_ADDR_REG0, parity_src = L1_ADDR_REG1, parity_mask2 = SM_REG0, flags = RN_REG_FLAGS, parity_flag = SM_REG1));
// 	_check_parity_error(APUC_SYS_CHECK_PARITY_AFTER_LOAD, false);
}

static u16 store_16_parity_mask(uint parity_grp)
{
	//GSI_DEBUG_ASSERT(parity_grp <= 1);
	return !parity_grp ? 0x0001 : 0x0010;
}

/*
 * Store 16 bits from @src into L1 @dst and generate parity stored in L1 @parity_dst
 */
APL_FRAG store_16(L1_REG dst, L1_REG parity_dst, SM_REG parity_mask, RN_REG src)
{
	{
		SM_0XFFFF:		RL = SB[src];
		(SM_0X1111 << 0):	GGL = RL;
	}
	{
		dst + 0 = GGL;
		(SM_0X1111 << 1):	GGL = RL;
	}
	{
		dst + 1 = GGL;
		(SM_0X1111 << 1):	RL ^= NRL;
		(SM_0X1111 << 2):	GGL = RL;
	}
	{
		dst + 2 = GGL;
		(SM_0X1111 << 2):	RL ^= NRL;
		(SM_0X1111 << 3):	GGL = RL;
	}
	{
		dst + 3 = GGL;
		(SM_0X1111 << 3):	RL ^= NRL;
		(SM_0X0001 << 11):	GL = RL;
	}
	{
		(SM_0X0001 << 15):	RL ^= GL;
		(SM_0X0001 << 3):	GL = RL;
		GGL = parity_dst;
	}
	{
		(SM_0X0001 << 7):	RL ^= GL;
		(SM_0X0001 << 7):	GL = RL;
		(SM_0X1111 << 0):	RL = GGL;
	}
	{
		(parity_mask << 0):	RL = GL;
		(SM_0X0001 << 15):	GL = RL;
	}
	{
		(parity_mask << 8):	RL = GL;
		(SM_0X1111 << 0):	GGL = RL;
	}
	{
		parity_dst = GGL;
	}
};


void gvrc_io_store_16(enum gvml_vm_reg vm_reg, enum gvrc_vr16 src)
{
	uint parity_set, row_in_set, l1_parity_grp, l1_parity_row;
	u16 l1_addr = gal_vm_reg_to_set_ext(vm_reg, &parity_set, &row_in_set, &l1_parity_grp, &l1_parity_row);
	u16 parity_mask = store_16_parity_mask(l1_parity_grp);

//	gsi_debug(2, "vm_reg=%u src=%u: l1_addr=0x%x parity_set=%u row_in_set=%u l1_parity_grp=%u parity_mask=0x%04x l1_parity_row=%u",
//	          vm_reg, src, l1_addr, parity_set, row_in_set, l1_parity_grp, parity_mask, l1_parity_row);

// 	_check_parity_error(APUC_SYS_CHECK_PARITY_BEFORE_STORE, true);

	apl_set_rn_reg(RN_REG_G0, src);
	apl_set_l1_reg(L1_ADDR_REG0, l1_addr);
	apl_set_l1_reg(L1_ADDR_REG1, l1_parity_row);
	apl_set_sm_reg(SM_REG0, parity_mask);

	RUN_FRAG_ASYNC(store_16(dst = L1_ADDR_REG0, parity_dst = L1_ADDR_REG1, parity_mask = SM_REG0, src = RN_REG_G0));
}

void gvrc_reset_16(enum gvrc_vr16 vreg)
{
	apl_set_rn_reg(RN_REG_G0, vreg);

	RUN_FRAG_ASYNC(cmn_reset_16_msk(vreg = RN_REG_G0, msk = SM_0XFFFF));
}

APL_FRAG cmn_not_bits_16(RN_REG res_vr, SM_REG res_mrk,
                         RN_REG x_vr, SM_REG x_mrk)
{
	{
		x_mrk: RL = ~SB[x_vr] & INV_RSP16;
		x_mrk: GL = RL;
	}
	res_mrk: SB[res_vr] = GL;
};

void gvrc_m_not(enum gvml_mrks_n_flgs res, enum gvml_mrks_n_flgs x)
{
	apl_set_sm_reg(SM_REG0, res);
	apl_set_sm_reg(SM_REG1, x);

	RUN_FRAG_ASYNC(cmn_not_bits_16(res_vr = RN_REG_FLAGS, res_mrk = SM_REG0,
				      x_vr = RN_REG_FLAGS, x_mrk = SM_REG1));
}

void gvrc_cpy_imm_16(enum gvrc_vr16 vreg, u16 val)
{
	apl_set_rn_reg(RN_REG_G0, vreg);
	apl_set_sm_reg(SM_REG0, val);

	RUN_FRAG_ASYNC(cmn_cpy_imm_16(vreg = RN_REG_G0, val = SM_REG0));
}

void gvrc_cpy_from_mrk_16_msk(enum gvrc_vr16 avr_dst, u16 asrc_mrk, u16 adst_msk)
{
	apl_set_rn_reg(RN_REG_0, avr_dst);
	apl_set_sm_reg(SM_REG0, asrc_mrk);
	apl_set_sm_reg(SM_REG1, adst_msk);

	RUN_IMM_FRAG_ASYNC(immf_gvrc_cpy_from_mrk_16_msk(	RN_REG vr_flags = RN_REG_FLAGS, RN_REG vr_dst = RN_REG_0, 
														SM_REG src_mrk = SM_REG0, SM_REG dst_msk = SM_REG1)
	{
		{
			src_mrk:	RL = SB[vr_flags];
			src_mrk:	GL = RL;
		}
		dst_msk:	SB[vr_dst] = GL;
	});

}

void gvrc_cpy_to_mrk_16_msk(u16 adst_msk, enum gvrc_vr16 avr_src, u16 asrc_mrk)
{
	apl_set_rn_reg(RN_REG_0, avr_src);
	apl_set_sm_reg(SM_REG0, asrc_mrk);
	apl_set_sm_reg(SM_REG1, adst_msk);

	RUN_IMM_FRAG_ASYNC(immf_gvrc_cpy_to_mrk_16_msk(	RN_REG vr_flags = RN_REG_FLAGS, RN_REG vr_src = RN_REG_0, 
														SM_REG src_mrk = SM_REG0, SM_REG dst_msk = SM_REG1)
	{
		{
			src_mrk:	RL = SB[vr_src];
			src_mrk:	GL = RL;
		}
		dst_msk:	SB[vr_flags] = GL;
	});

}


void set_bank_en_mask(u8 bank_en)
{
	apl_set_bank_en_reg(bank_en);
	RUN_FRAG_ASYNC(noop_apl());
}

static inline u8 BANK_EN_MASK(uint apc_id, uint bank_id)
{
	//GSI_ASSERT(apc_id < apuc_num_apcs());
	//GSI_ASSERT(bank_id < apc_num_banks());
	return (u8)(1 << (apc_id * GSI_MMB_NUM_BANKS + bank_id));
}

void enable_bank(uint apc_id, uint bank_id)
{
	//GSI_ASSERT(apc_id < apuc_num_apcs());
	//GSI_ASSERT(bank_id < apc_num_banks());
	set_bank_en_mask(BANK_EN_MASK(apc_id, bank_id));
}

void enable_bank_reset(void)
{
	set_bank_en_mask((u8)~0);
}

void etest_test_apl_init(void)
{
	apl_set_sm_reg(SM_0X0001, 0x0001);
	apl_set_sm_reg(SM_0XFFFF, 0xffff);
	apl_set_rn_reg(RN_REG_FLAGS, GVRC_VR16_FLAGS);

	apl_set_sm_reg(SM_0XFFFF, 0xffff);
	apl_set_sm_reg(SM_0X0001, 0x0001);
	apl_set_sm_reg(SM_0X1111, 0x1111);
	apl_set_sm_reg(SM_0X0101, 0x0101);
	apl_set_sm_reg(SM_0X000F, 0x000f);
	apl_set_sm_reg(SM_0X0F0F, 0x0f0f);
	apl_set_sm_reg(SM_0X0707, 0x0707);
	apl_set_sm_reg(SM_0X5555, 0x5555);
	apl_set_sm_reg(SM_0X3333, 0x3333);
	apl_set_sm_reg(SM_0X00FF, 0x00ff);
	apl_set_sm_reg(SM_0X001F, 0x001f);
	apl_set_sm_reg(SM_0X003F, 0x0003);

	//apl_set_rn_reg(RN_REG_T0, VR16_T0);
	//apl_set_rn_reg(RN_REG_T1, VR16_T1);
	//apl_set_rn_reg(RN_REG_T2, VR16_T2);
	//apl_set_rn_reg(RN_REG_T3, VR16_T3);
	//apl_set_rn_reg(RN_REG_T4, VR16_T4);
	//apl_set_rn_reg(RN_REG_T5, VR16_T5);
	//apl_set_rn_reg(RN_REG_T6, VR16_T6);
	//apl_set_rn_reg(RN_REG_FLAGS, VR16_FLAGS);

/*
	apl_set_l1_reg(L1_ADDR_REG_RESERVED, 0);
	apl_set_l1_reg(L1_ADDR_REG_INDEX, gal_vm_reg_to_addr(GVML_VM_INDEX));
	gvrc_create_index_m1_g2k_t0(GVML_M4_IDX);
	for (u8 bank_id = 0; bank_id < 4; bank_id++) {
		for (uint apc_id = 0; apc_id < 2; apc_id++) {
			enable_bank(apc_id, bank_id);
			gvrc_set_16_msk(GVML_M4_IDX, (u16)((bank_id << 12) | (apc_id << 14)));
		}
	}
*/
	enable_bank_reset();
}