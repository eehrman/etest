#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <wordexp.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <string.h>

typedef uint16_t u16;

#define bool	_Bool
#include "seqmatch.h"

#define min(a,b) (((a)<(b))?(a):(b))

static char *read_to_buf(char *fnt, int *psize)
{
	wordexp_t exp_result;
	wordexp(fnt, &exp_result, 0);
	printf("%s\n", exp_result.we_wordv[0]);
	struct stat st;
	stat(exp_result.we_wordv[0], &st);
	int size = (int)st.st_size;
	char *word_buf = (char *)malloc(size);
	FILE *fh_words = fopen(exp_result.we_wordv[0], "rb");
	size_t read_len = fread(word_buf, sizeof(char), size, fh_words);
	printf("read_to_buf. read len %d.\n", (int)read_len);
	if ((int)read_len != size) {
		printf("Error reading file %s.\n", fnt);
		*psize = -1;
		return NULL;
	}
	*psize = size;
	fclose(fh_words);
	return word_buf;

}


#define NUMQ_BUF_LEN 16
#define NUMW_BUF_LEN 6
#define START_POS_BUF_LEN 6
#define NUM_U16_ROWS 4

//free(iquery);free(hd_thresh);free(copy_move);free(wstarts);free(isample);

static void rearrange(char c, u16 *a) {
	memset(a, 0, NUM_U16_ROWS * sizeof(u16));
	for (int ic = 0; ic < 4; ic++) {
		u16 rc = (u16)(((unsigned int)c & (3 << (ic*2))) >> (ic * 2));
		u16 repc = 0;
		for (int irepeat = 0; irepeat < 4 /*8 * (int)sizeof(u16) / NUM_U16_ROWS*/; irepeat++) {
			repc = (u16)(repc | (rc << (2 * irepeat)));
			//a[ic % NUM_U16_ROWS] |= rc << (irepeat * NUM_U16_ROWS);
		}
//			if (iw < 100) {
//				printf("iw: %d, ic: %d, c: %d, rc: 0x%hx, repc: 0x%hx\n", iw, ic, (int)c, rc, repc);
//			}
		a[ic] = repc;
	}

}

int get_sample_data(u16 **piquery, u16 **phd_thresh, u16 **pcopy_move, u16 **pwstarts, u16 **pisample, u16 **piqlens,
                    int *pnum_rows, int *pvec_size, int *pnumw, int *pqlen_sum, int *pnumq)
{
	printf("get_sample_data called\n");
	char fnt_words[] = "~/eclipse-workspace/dq_words_db.txt";
	char fnt_starts[] = "~/eclipse-workspace/dq_words_starts.txt";
	char fnt_queries[] = "~/eclipse-workspace/dq_words_queries.txt";
	int wbuf_len, sbuf_len, qbuf_len;
	char *word_buf = read_to_buf(fnt_words, &wbuf_len);
	char *starts_buf = read_to_buf(fnt_starts, &sbuf_len);
	char *q_buf = read_to_buf(fnt_queries, &qbuf_len);
	if (wbuf_len < 0 || sbuf_len < 0 || qbuf_len < 0) {
		return (1);
	}

	// db
	u16 *isample = (u16 *)malloc(wbuf_len * sizeof(u16) * NUM_U16_ROWS);
	for (int iw = 0; iw < wbuf_len; iw++) {
		char c = word_buf[iw];
		u16 a[NUM_U16_ROWS];
		rearrange(c, a);
		for (int ir = 0; ir < NUM_U16_ROWS; ir++) {
			isample[(ir * wbuf_len) + iw] = a[ir];
//			isample[(ir * wbuf_len) + iw] = (u16)word_buf[iw];
		}
	}
	int vec_size = wbuf_len;

	// starts
	char *pstarts_buf = starts_buf;
	char s_num_starts[NUMW_BUF_LEN + 1];
	memcpy(s_num_starts, pstarts_buf, NUMW_BUF_LEN);
	pstarts_buf += NUMW_BUF_LEN;
	s_num_starts[NUMW_BUF_LEN] = '\0';
	int numw = atoi(s_num_starts);
	u16 *wstarts = (u16 *)malloc(numw * sizeof(u16));
	for (int iw = 0; iw < numw; iw++) {
		char buf[START_POS_BUF_LEN + 1];
		memcpy(buf, pstarts_buf, START_POS_BUF_LEN);
		pstarts_buf += START_POS_BUF_LEN;
		buf[START_POS_BUF_LEN] = '\0';
		wstarts[iw] = (u16)atoi(buf);
//		if (iw > 1360 && iw < 1380) {
//			printf("get_sample_data: iw: %d, val: %hu.\n", iw, wstarts[iw]);
//		}
	}

	/*
	for (int ir = 0; ir < NUM_U16_ROWS; ir++) {
		u16 *p = &(isample[ir * wbuf_len]);
		printf("get_sample_data. ir = %d.\n", ir);
		u16 next_start_idx = 0;
		for (int i = 0; i < 32; i++) {
			if (i == wstarts[next_start_idx]) {
				printf(" | ");
				next_start_idx++;
			}
			printf("%hu,", p[i]);
		}
		printf("etc. \n");
	}
	*/

	// queries
	char s_num_qs[NUMQ_BUF_LEN + 1];
	char *pq_buf = q_buf;
	memcpy(s_num_qs, pq_buf, NUMQ_BUF_LEN);
	pq_buf += NUMQ_BUF_LEN;
	s_num_qs[NUMQ_BUF_LEN] = '\0';
	int num_qs = atoi(s_num_qs);
	u16 *iqlens = (u16 *)malloc(num_qs * sizeof(u16));
	int q_data_len = (qbuf_len - NUMQ_BUF_LEN);
	// q_data_len is not really the right param here. The sum of qlens values is, but it is not accessible, so we overallocate to make sure
	u16 *iquery = (u16 *)malloc(q_data_len * sizeof(u16) * NUM_U16_ROWS);
	printf("q_data_len = %d. num_qs = %d.\n", q_data_len, num_qs);
	u16 *hd_thresh = (u16 *)malloc(q_data_len * sizeof(u16));
	u16 *copy_move = (u16 *)malloc(q_data_len * sizeof(u16));
	int qpos = 0;
	int qlen_sum = 0;
	for (int iq = 0; iq < num_qs; iq++) {
		char s_qlen[3];
		memcpy(s_qlen, pq_buf, 2);
		s_qlen[2] = '\0';
		pq_buf += 2;
		int qlen = atoi(s_qlen);
		iqlens[iq] = (u16)qlen;
		char qword[qlen + 1], s_thresh[qlen * 2], s_copy_move[qlen];
		u16 qthresh[qlen], move_copy_buf[qlen];
		memcpy(qword, pq_buf, qlen);
		qword[qlen] = '\0';
		pq_buf += qlen;
		memcpy(s_thresh, pq_buf, qlen * 2);
		pq_buf += qlen * 2;
		memcpy(s_copy_move, pq_buf, qlen);
		pq_buf += qlen;
		for (int iw = 0; iw < qlen; iw++) {
			char buf[3];
			memcpy(buf, &s_thresh[iw * 2], 2);
			buf[2] = '\0';
			qthresh[iw] = (u16)atoi(buf);
			buf[0] = s_copy_move[iw];
			buf[1] = '\0';
			move_copy_buf[iw] = (u16)atoi(buf);
		}
		for (int iqc = 0; iqc < qlen; iqc++) {
			char c = qword[iqc];
			u16 a[NUM_U16_ROWS];
			rearrange(c, a);
			for (int ir = 0; ir < NUM_U16_ROWS; ir++) {
				iquery[(qpos * NUM_U16_ROWS) + (ir * qlen) + iqc] = a[ir];
			}
			//memcpy(&(iquery[(ir * q_data_len) + qpos]), qword, qlen);
//			for (int iqc = 0; iqc < qlen; iqc++) {
//				iquery[(qpos * NUM_U16_ROWS) + (ir * qlen) + iqc] = (u16)qword[iqc];
//			}
		}
		memcpy(&(hd_thresh[qpos]), qthresh, qlen * sizeof(u16)); // Should be correct now. wrong! qpos is moving up by *4 and hd_thresh needs only one u16
		memcpy(&(copy_move[qpos]), move_copy_buf, qlen * sizeof(u16)); // same as above. Wrong through lack of use

		qpos += qlen;
		qlen_sum += qlen;
	}
	/*
	qpos = 0;
	printf("get_sample_data. query data.\n");
	for (int iq = 0; iq < min(32, num_qs); iq++) {
		printf("{%d: ", iq);
		for (int iel = 0; iel < iqlens[iq]; iel++) {
			printf("(");
			for (int ir = 0; ir < NUM_U16_ROWS; ir++) {
				u16 el = (iquery[qpos + (ir * iqlens[iq]) + iel]);
				printf("%hu,", el);
			}
			printf("),");
		}
		printf("}\n,");
		qpos += iqlens[iq] * NUM_U16_ROWS;
	}
	*/
	*piquery = iquery;
	*phd_thresh = hd_thresh;
	*pcopy_move = copy_move;
	*pwstarts = wstarts;
	*pisample = isample;
	*piqlens = iqlens;
	free(q_buf);
	free(starts_buf);
	free(word_buf);
	*pnum_rows = NUM_U16_ROWS;
	*pvec_size = (u16)vec_size;
	*pnumw = (u16)numw;
	//*pqlen_sum = (u16)q_data_len;
	*pnumq = (u16)num_qs;
	*pqlen_sum = (u16)qlen_sum;
	printf("get_sample_data done.\n");
	return 0;
}
